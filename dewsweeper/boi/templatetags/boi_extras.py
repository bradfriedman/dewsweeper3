from django import template
from django.template.defaultfilters import stringfilter


register = template.Library()

@register.filter()
def dict_get(d, key):
    return d.get(key, 0)


@register.filter
def index(List, i):
    return List[int(i)]


@register.filter
def percentage(value, precision):
    return format(value, '.{precision}%'.format(precision=precision))
