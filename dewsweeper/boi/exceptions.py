class BoiException(Exception):
  pass


class BoiDraftException(Exception):
  pass


class BoiMonkeyListException(Exception):
  pass


class BoiMonkeyLineupSubmitterException(Exception):
  pass


class BoiNoDayOneScoreException(Exception):
  pass


class BoiSchedulingException(Exception):
  pass


class BoiScoringException(Exception):
  pass


class BoiScraperException(Exception):
  pass


class BoiWaiversException(Exception):
  pass
