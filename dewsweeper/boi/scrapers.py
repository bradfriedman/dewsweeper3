from abc import abstractmethod, ABCMeta
import re
import urllib
import urllib2
from urlparse import parse_qs, urlparse

from bs4 import BeautifulSoup
import dateutil.parser as dateutil_parser

from .boi_config import SUMOREF_BANZUKE_URL
from .boi_config import SUMOREF_RESULTS_URL
from .boi_config import SUMOREF_RIKISHI_URL

from .exceptions import BoiScraperException

from .models import Kimarite
from .models import OzumoBanzukeEntry

from .utils import calculate_score
from .utils import convert_basho_id_to_sumoref_id
from .utils import convert_rank_string_to_rank
from .utils import convert_rank_to_full_string


class BoiScraper(object):
  __metaclass__ = ABCMeta

  @abstractmethod
  def scrape(self):
    pass

  def download_data(self, url, payload=None):
    '''
    Returns response object from url retrieval.

    Args:
        url: The url (without querystring params) to fetch
        payload: A dict with the querystring params

    Returns:
        The response object
    '''
    qs_params = urllib.urlencode(payload) if payload else None
    final_url = '{0}?{1}'.format(url, qs_params) if qs_params else url
    return urllib2.urlopen(final_url)

  def get_response_status_code(self, response):
    return response.getcode()

  def get_response_content(self, response):
    return response.read()

  def get_response_url(self, response):
    return response.geturl()


RIKISHI_CELL_CLASSES = ['shikona', 'debut', 'retired']


class BanzukeScraper(BoiScraper):
  '''
  Returns a list of tuples describing rikishi in a banzuke. Each tuple in the
  list consists of (shikona, rank in integer code, rank string, sumoref_id)
  '''

  def __init__(self, basho_id, include_juryo=True):
    self.basho_id = basho_id
    self.include_juryo = include_juryo

  def scrape(self):
    raw_banzuke = self._download_raw_banzuke()
    return self._process_raw_banzuke(raw_banzuke)

  def _get_rank_from_shikona_cell(self, cell):
    return convert_rank_string_to_rank(
      cell.find_parent('tr').find('td', class_='short_rank').get_text()
    )

  def _get_sumoref_rikishi_id_from_shikona_cell(self, cell):
    return parse_qs(urlparse(cell.find('a').get('href', '')).query).get(
      'r', [''])[0]

  def _get_rikishi_banzuke_tuple_from_shikona_cell(self, cell):
    rank_short_str = cell.find_parent('tr').find(
      'td', class_='short_rank').get_text()
    rank_int = convert_rank_string_to_rank(rank_short_str)
    rank_full_str = convert_rank_to_full_string(rank_int)

    sumoref_id = self._get_sumoref_rikishi_id_from_shikona_cell(cell)

    return (cell.get_text(), rank_int, rank_full_str, sumoref_id)

  def _download_raw_banzuke(self):
    sumoref_id = convert_basho_id_to_sumoref_id(self.basho_id)
    payload = {'b': sumoref_id}
    raw_data_result = self.download_data(SUMOREF_BANZUKE_URL, payload)
    if self.get_response_status_code(raw_data_result) != 200:
      raise Exception('Failed to pull banzuke '
                      '(url {0}, status code {1})').format(
        self.get_response_url(raw_data_result),
        self.get_response_status_code(raw_data_result))

    return self.get_response_content(raw_data_result)

  def _process_raw_banzuke(self, raw_banzuke):
    soup = BeautifulSoup(raw_banzuke, 'lxml')
    makuuchi_table = soup.find('table', class_='banzuke')
    rikishi_cells = makuuchi_table.find_all(
      'td', class_=lambda c: c in RIKISHI_CELL_CLASSES)

    if self.include_juryo:
      juryo_table = makuuchi_table.find_next_sibling(
        'table', class_='banzuke')
      rikishi_cells += juryo_table.find_all(
        'td', class_=lambda c: c in RIKISHI_CELL_CLASSES)

    return [self._get_rikishi_banzuke_tuple_from_shikona_cell(rc)
            for rc in rikishi_cells]


class OzumoResultsScraper(BoiScraper):

  def __init__(self, basho_id, day):
    self.basho_id = basho_id
    self.day = day
    self.bout_rows = []

  def scrape(self):
    raw_results = self._download_raw_results()
    return self._process_raw_results(raw_results)

  def _normalize_kimarite(self, kimarite):
    # 'fusensho', 'fusenhai', 'default', 'no contest' ---> 'fusen'
    if ('fusen' in kimarite or kimarite == 'default' or
        kimarite == 'no contest'):
      kimarite = 'fusen'

    return kimarite

  def _extract_kimarite_from_cell(self, cell):
    kimarite_text = cell.get_text().lower()
    re_match = re.search(r'([a-z]+)[0-9]*', kimarite_text)
    if not re_match:
      raise BoiScraperException('Could not extract kimarite from '
                                '{0}'.format(kimarite_text))
    return self._normalize_kimarite(re_match.group(1))

  def _process_bout(self, data_row):
    east_kekka = data_row.find('td', class_='tk_kekka')
    east_rikishi = data_row.find('td', class_='tk_east')
    kimarite_cell = data_row.find('td', class_='tk_kim')
    west_rikishi = data_row.find('td', class_='tk_west')

    # Check to see if we didn't find any of those cells
    if (not east_kekka or not east_rikishi or
        not kimarite_cell or not west_rikishi):
      raise BoiScraperException('Could not find a results cell: '
                                '{0}'.format(data_row))

    # Did east win?
    east_kekka_img = east_kekka.find('img')
    east_won = ('shiro' in east_kekka_img['src'] or
                'fusensho' in east_kekka_img['src'])

    # Get OzumoBanzukeEntry object for east
    east_shikona = east_rikishi.find('a').get_text()
    east_banzuke_entry_obj = OzumoBanzukeEntry.objects.get(
      shikona=east_shikona,
      basho_id=self.basho_id,
    )

    # Get OzumoBanzukeEntry object for west
    west_shikona = west_rikishi.find('a').get_text()
    west_banzuke_entry_obj = OzumoBanzukeEntry.objects.get(
      shikona=west_shikona,
      basho_id=self.basho_id,
    )

    # Get kimarite object
    kimarite_name = self._extract_kimarite_from_cell(kimarite_cell)
    kimarite_obj, _ = Kimarite.objects.get_or_create(
      name=kimarite_name,
    )

    # Get Rikishi objects
    east_rikishi_obj = east_banzuke_entry_obj.rikishi
    west_rikishi_obj = west_banzuke_entry_obj.rikishi

    # Assign winners and losers
    if east_won:
      winning_banzuke_entry_obj = east_banzuke_entry_obj
      losing_banzuke_entry_obj = west_banzuke_entry_obj
      winning_rikishi_obj = east_rikishi_obj
      losing_rikishi_obj = west_rikishi_obj
    else:
      winning_banzuke_entry_obj = west_banzuke_entry_obj
      losing_banzuke_entry_obj = east_banzuke_entry_obj
      winning_rikishi_obj = west_rikishi_obj
      losing_rikishi_obj = east_rikishi_obj

    return {
      'winning_rikishi': winning_rikishi_obj,
      'winning_rikishi_rank': winning_banzuke_entry_obj.rank,
      'losing_rikishi': losing_rikishi_obj,
      'losing_rikishi_rank': losing_banzuke_entry_obj.rank,
      'kimarite': kimarite_obj,
      'value': calculate_score(winning_banzuke_entry_obj.rank,
                               losing_banzuke_entry_obj.rank,
                               kimarite=kimarite_name),
      'basho_id': self.basho_id,
      'day': self.day,
    }

  def get_bout_results_by_rikishi(self):
    '''Returns a dict of rikishi shikona to a tuple of (value, kimarite).'''
    results = {}
    for b in self.bout_rows:
      results[b['winning_rikishi'].shikona_for_basho(self.basho_id)] = (
        b['value'], b['kimarite'].name
      )
      results[b['losing_rikishi'].shikona_for_basho(self.basho_id)] = (
        0, b['kimarite'].name
      )
    return results

  def _download_raw_results(self):
    sumoref_id = convert_basho_id_to_sumoref_id(self.basho_id)
    payload = {'b': sumoref_id, 'd': self.day}
    raw_data_result = self.download_data(SUMOREF_RESULTS_URL,
                                         payload=payload)
    if self.get_response_status_code(raw_data_result) != 200:
      raise Exception('Failed to pull ozumo results '
                      '(url {0}, status code {1})').format(
        self.get_response_url(raw_data_result),
        self.get_response_status_code(raw_data_result))

    return self.get_response_content(raw_data_result)

  def _process_raw_results(self, raw_results):
    soup = BeautifulSoup(raw_results, 'lxml')
    data_table = soup.find('table', class_='tk_table')
    data_rows = data_table.find_all('tr')
    self.bout_rows = [self._process_bout(dr) for dr in data_rows[1:]]
    return self.bout_rows


class OfflineOzumoResultsScraper(OzumoResultsScraper):

  def __init__(self, basho_id, day, rank_dict):
    super(OfflineOzumoResultsScraper, self).__init__(basho_id, day)

    self.rank_dict = rank_dict
    if not self.rank_dict:
      raise BoiScraperException(
        'OfflineOzumoResultsScraper must be provided with rank_dict')

  def _process_bout(self, data_row):
    east_kekka = data_row.find('td', class_='tk_kekka')
    east_rikishi = data_row.find('td', class_='tk_east')
    kimarite_cell = data_row.find('td', class_='tk_kim')
    west_rikishi = data_row.find('td', class_='tk_west')

    # Check to see if we didn't find any of those cells
    if (not east_kekka or not east_rikishi or
        not kimarite_cell or not west_rikishi):
      raise BoiScraperException('Could not find a results cell: '
                                '{0}'.format(data_row))

    # Did east win?
    east_kekka_img = east_kekka.find('img')
    east_won = 'shiro' in east_kekka_img['src']

    # Get shikona for east and west
    east_shikona = east_rikishi.find('a').get_text()
    west_shikona = west_rikishi.find('a').get_text()

    # Get kimarite
    kimarite_name = self._extract_kimarite_from_cell(kimarite_cell)

    # Compile results
    if east_won:
      winning_rikishi = east_shikona
      losing_rikishi = west_shikona
    else:
      winning_rikishi = west_shikona
      losing_rikishi = east_shikona

    return {
      'winning_rikishi': winning_rikishi,
      'winning_rikishi_rank': self.rank_dict[winning_rikishi],
      'losing_rikishi': losing_rikishi,
      'losing_rikishi_rank': self.rank_dict[losing_rikishi],
      'value': calculate_score(self.rank_dict[winning_rikishi],
                               self.rank_dict[losing_rikishi]),
      'kimarite': kimarite_name,
      'basho_id': self.basho_id,
      'day': self.day,
    }

  def get_bout_results_by_rikishi(self):
    raise BoiScraperException(
      'Cannot get bout results by rikishi when offline')


class RikishiProfileScraper(BoiScraper):

  def __init__(self, web_id):
    self.web_id = web_id

  def scrape(self):
    raw_profile = self._download_raw_profile()
    return self._process_raw_profile(raw_profile)

  def _get_rikishi_data_key_value_from_cell(self, cell):
    key = cell.get_text()
    if not key:
      return (None, None)
    try:
      value = cell.find_next_sibling('td', class_='val').get_text()
    except AttributeError:
      return (None, None)
    if 'real name' in key.lower():
      # Reverse the names
      tokens = value.split()
      if len(tokens) >= 2:
        reversed_name = u'{0} {1}'.format(tokens[1], tokens[0])
      else:
        reversed_name = value
      value = reversed_name.title()
    if 'birth' in key.lower():
      date_value = re.sub(r'\(.*\)', '', value).strip()
      value = dateutil_parser.parse(date_value).date()
    return (key, value)

  def _download_raw_profile(self):
    payload = {'r': self.web_id}
    raw_data_result = self.download_data(SUMOREF_RIKISHI_URL,
                                         payload=payload)
    if self.get_response_status_code(raw_data_result) != 200:
      raise Exception('Failed to pull rikishi profile '
                      '(url {0}, status code {1})').format(
        self.get_response_url(raw_data_result),
        self.get_response_status_code(raw_data_result))

    return self.get_response_content(raw_data_result)

  def _process_raw_profile(self, raw_profile):
    soup = BeautifulSoup(raw_profile, 'lxml')
    data_table = soup.find('table', class_='rikishidata')
    data_cells = data_table.find_all('td', class_='cat')
    return {k: v for k, v in
            [self._get_rikishi_data_key_value_from_cell(c)
             for c in data_cells] if k is not None and v is not None}
