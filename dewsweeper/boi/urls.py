from django.conf.urls import url

from . import views

app_name = 'boi'
urlpatterns = [
  url(r'^$', views.IndexView.as_view(), name='index'),
  url(r'^banzuke/?$', views.BanzukeView.as_view(), name='banzuke'),
  url(r'^banzuke/(?P<pk>[0-9]+)/$',
      views.ProfileView.as_view(),
      name='profile'),
  url(r'^dewsweeper/$', views.dewsweeper_view, name='dewsweeper'),
  url(r'^dewsweeper/auto/$', views.dewsweeper_automated_view,
      name='dewsweeper_automated'),
  url(r'^draftregistration/$', views.draft_registration_view,
      name='draft_registration'),
  url(r'^manage/?$', views.SquadronManagementIndexView.as_view(),
      name='mgmt_index'),
  url(r'^manage/(?P<oyakata_id>[0-9]+)/$', views.squadron_management_view,
      name='mgmt'),
  url(r'^matchups/$', views.MatchupsView.as_view(), name='matchups'),
  url(r'^monkeylist/$', views.monkey_list_view, name='monkey_list'),
  url(r'^monkeysubmit/auto/$', views.monkey_automated_management_view,
      name='monkeysubmit_automated'),
  url(r'^recordbanzuke/$', views.record_banzuke_view, name='record_banzuke'),
  url(r'^registerplayers/$', views.register_players_view,
      name='register_players'),
  url(r'^resultsindex/$', views.ResultsIndexView.as_view(),
      name='results_index'),
  url(r'^results/$', views.ResultsView.as_view(), name='results'),
  url(r'^scheduleregistration/$', views.schedule_registration_view,
      name='schedule_registration'),
  url(r'^standings/$', views.StandingsView.as_view(), name='standings'),
  url(r'^suicide/$', views.SuicideStandingsView.as_view(), name='suicide'),
  url(r'^utils/$', views.UtilsView.as_view(), name='utils'),
  url(r'^waivers/$', views.waivers_view, name='waivers'),
  url(r'^winlossmatrix/$', views.WinLossMatrixView.as_view(),
      name='winlossmatrix'),
]
