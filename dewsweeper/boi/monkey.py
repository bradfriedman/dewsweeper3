import collections
import io
import operator
import os
import random
import re

from django.conf import settings
from django.db import transaction

from .boi_config import DRAFT_DIVISION_SIZE
from .boi_config import DRAFT_DIVISIONS
from .boi_config import LINEUP_SIZE
from .boi_config import MONKEY_SHIKONA
from .boi_config import SQUADRON_SIZE

from .emailers import LineupEmailer

from .exceptions import BoiMonkeyListException
from .exceptions import BoiMonkeyLineupSubmitterException

from .models import BashoSquadron
from .models import LineupEntry
from .models import OzumoBanzukeEntry
from .models import SquadronMember
from .models import SuicidePoolEntry

from .utils import get_basho_full_name


class MonkeyLineupSubmitter(object):

  def __init__(self, basho_id, day, include_suicide, send_email):
    self.basho_id = basho_id
    self.day = day
    self.include_suicide = include_suicide
    self.send_email = send_email
    self.trash_talk_generator = None

  @transaction.atomic
  def submit_lineup(self, shikona=MONKEY_SHIKONA):
    basho_squadron = BashoSquadron.objects.filter(
      basho_id=self.basho_id,
      shikona=shikona,
    ).first()

    if not basho_squadron:
      raise BoiMonkeyLineupSubmitterException(
        'No basho squadron for {0} in {1}'.format(
          shikona, get_basho_full_name(self.basho_id)))

    squadron_members = SquadronMember.objects.filter(
      basho_id=self.basho_id,
      oyakata=basho_squadron.oyakata,
      is_active=True,
    ).order_by('selection_order')

    if len(squadron_members) != SQUADRON_SIZE:
      raise BoiMonkeyLineupSubmitterException(
        '{0} squadron members for {1} in {2}'.format(
          len(squadron_members), shikona,
          get_basho_full_name(self.basho_id)))

    # Select four squadron members randomly
    # TODO: Intelligently select four squadron members based on matchups
    selected_squadron_members = sorted(
      random.sample(squadron_members, LINEUP_SIZE),
      key=lambda sm: sm.selection_order)

    # Delete any pre-existing lineup entries
    LineupEntry.objects.filter(
      oyakata=basho_squadron.oyakata,
      basho_id=self.basho_id,
      day=self.day,
    ).delete()

    # Then, add new lineup entries
    new_lineup_entries = []
    for n, selected_squadron_member in enumerate(selected_squadron_members):
      new_lineup_entries.append(LineupEntry.objects.create(
        oyakata=basho_squadron.oyakata,
        rikishi=selected_squadron_member.rikishi,
        basho_id=self.basho_id,
        day=self.day,
        order=n + 1,
      ))

    # Do a suicide pool pick, if necessary
    suicide_pick = None
    if self.include_suicide:
      prev_suicide_picks = SuicidePoolEntry.objects.filter(
        basho_id=self.basho_id,
        oyakata=basho_squadron.oyakata,
        day__lt=self.day,
      ).values('rikishi_id')

      available_suicide_picks = OzumoBanzukeEntry.objects.filter(
        basho_id=self.basho_id,
      ).exclude(
        rikishi_id__in=prev_suicide_picks
      )

      suicide_pick = random.choice(available_suicide_picks)

      # First, map the OzumoBanzukeEntry to a Rikishi
      suicide_rikishi = suicide_pick.rikishi

      # Then, remove old suicide pick, if any
      SuicidePoolEntry.objects.filter(
        oyakata=basho_squadron.oyakata,
        basho_id=self.basho_id,
        day=self.day,
      ).delete()

      # Finally, create new SuicidePoolEntry
      SuicidePoolEntry.objects.create(
        oyakata=basho_squadron.oyakata,
        rikishi=suicide_rikishi,
        basho_id=self.basho_id,
        day=self.day,
      )

    # Email the Monkey's lineup out!
    if self.send_email:
      lineup_emailer = LineupEmailer(
        new_lineup_entries,
        suicide_pick,
        self._generate_monkey_taunt(),
        self._generate_monkey_taunt(),
        basho_squadron,
        self.day,
      )
      lineup_emailer.send_email()

    return new_lineup_entries

  def _generate_monkey_taunt(self, length=250):
    self.trash_talk_generator = (self.trash_talk_generator or
                                 TrashTalkGenerator())
    return self.trash_talk_generator.generate_trash_talk(length)


class TrashTalkGenerator(object):

  def __init__(self, corpus_path=None, model_order=10):
    self.corpus_path = corpus_path or 'boi/static/boi/data/monkeyspeech.dat'
    self.model_order = model_order
    self.corpus_data = None
    self.model = collections.defaultdict(list)
    self.starting_point_indexes = []

  def _read_corpus(self):
    if not self.corpus_data:
      path = os.path.join(settings.BASE_DIR, self.corpus_path)
      with io.open(path, 'r', encoding='utf8') as f:
        self.corpus_data = f.read()

  def _create_markov_model(self):
    if not self.model:
      for index in xrange(len(self.corpus_data) - self.model_order - 1):
        prefix = self.corpus_data[index:index + self.model_order]
        suffix = self.corpus_data[index + self.model_order]
        self.model[prefix].append(suffix)

  def _create_starting_points(self):
    if not self.starting_point_indexes:
      self.starting_point_indexes = [
        m.start() + 2 for m in re.finditer('\n\n', self.corpus_data)
        if m.start() + 2 + self.model_order < len(self.corpus_data)]

  def generate_trash_talk(self, n):
    self._read_corpus()
    self._create_markov_model()
    self._create_starting_points()

    starting_point = random.choice(self.starting_point_indexes)
    result = self.corpus_data[
             starting_point:starting_point + self.model_order]
    while len(result) < n or result[-1] not in ['.', '!', '?', '\n']:
      prefix = result[-self.model_order:]
      result += random.choice(self.model[prefix])

    return result


class MonkeyListGenerator(object):

  @staticmethod
  def generate_list(draft):
    rikishi_dict = collections.defaultdict(int)
    max_value = DRAFT_DIVISION_SIZE * SQUADRON_SIZE
    lines = [line for line in draft.split('\n')
             if line and not line.isspace()]
    for division in range(DRAFT_DIVISIONS - 1):
      cur_value = max_value
      oyakata_line_num = division * (SQUADRON_SIZE + 1)
      oyakata_line = lines[oyakata_line_num]
      oyakata_tokens = [o.strip() for o in oyakata_line.split('\t')]
      if len(oyakata_tokens) != DRAFT_DIVISION_SIZE:
        raise BoiMonkeyListException(
          'Incorrect number of oyakata in line {line}.'.format(
            line=oyakata_line,
          ))

      # Read each line of selections
      for rikishi_row_num in range(SQUADRON_SIZE):
        rikishi_line_num = oyakata_line_num + rikishi_row_num + 1
        rikishi_line = lines[rikishi_line_num]
        rikishi_tokens = [
          r.strip() for r in rikishi_line.strip().split('\t')]
        if len(rikishi_tokens) != DRAFT_DIVISION_SIZE:
          raise BoiMonkeyListException(
            'Incorrect number of rikishi in line {line}.'.format(
              line=rikishi_line,
            ))

        # If this is an odd-numbered row, values are reversed because
        # draft order is snaked
        if rikishi_row_num % 2 == 1:
          rikishi_tokens.reverse()

        for r in rikishi_tokens:
          rikishi_dict[r] += cur_value
          cur_value -= 1

    return sorted(
      rikishi_dict.items(), key=operator.itemgetter(1, 0), reverse=True)
