# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import codecs
import collections
import datetime
import logging
import mock
import os
import random

from django.core import mail
from django.test import SimpleTestCase, TestCase, TransactionTestCase
from django.urls import reverse
from django.utils.crypto import get_random_string

from .boi_config import LINEUP_SIZE
from .boi_config import MONKEY_SHIKONA

from .draft import DraftRegistrar

from .emailers import LineupEmailer
from .emailers import DewsweeperEmailer
from .emailers import WaiversResultsEmailer

from .exceptions import BoiNoDayOneScoreException
from .exceptions import BoiScoringException

from .forms import RegisterPlayersForm

from .models import Bout
from .models import Kimarite
from .models import BashoSquadron
from .models import Oyakata
from .models import Rikishi
from .models import OzumoBanzukeEntry
from .models import LineupEntry
from .models import OzumoBout
from .models import SquadronMember
from .models import SuicidePoolEntry
from .models import _get_lineup

from .monkey import MonkeyLineupSubmitter
from .monkey import MonkeyListGenerator

from .scheduling import ScheduleRegistrar

from .scoring import DewsweeperProcessor
from .scoring import score_lineup
from .scoring import SquadronSorter

from .scrapers import BanzukeScraper
from .scrapers import OzumoResultsScraper
from .scrapers import RikishiProfileScraper

from .utils import calculate_score
from .utils import convert_rank_to_full_string
from .utils import convert_rank_to_short_string
from .utils import get_current_basho_id
from .utils import get_basho_name, get_basho_year, get_basho_full_name
from .utils import get_basho_ids_in_year

from .view_utils import create_divisions
from .view_utils import get_current_day

from .waivers import WaiversProcessor
from .waivers import WaiversResult

random.seed()

TESTDATA_BASEDIR = os.path.join(os.path.dirname(__file__),
                                'testdata')


def assertEqualHTML(string1, string2, file1='', file2=''):
  '''
  Compare two unicode strings containing HTML.
  A human friendly diff goes to logging.error() if there
  are not equal, and an exception gets raised.
  '''
  from bs4 import BeautifulSoup as bs
  import difflib

  p = []

  for mystr in [string1, string2]:
    soup = bs(mystr, 'lxml')
    pretty = soup.prettify()
    p.append(pretty)
  if p[0] != p[1]:
    for line in difflib.unified_diff(p[0].splitlines(),
                                     p[1].splitlines(),
                                     fromfile=file1,
                                     tofile=file2):
      logging.error(line)
    raise Exception('Not equal %s %s' % (file1, file2))


def random_dob():
  year = random.randint(1950, 2000)
  month = random.randint(1, 12)
  day = random.randint(1, 28)
  return datetime.date(year=year, month=month, day=day)


def create_rikishi(real_name=None, dob=None, base_basho_id=None, rank=None,
                   num_shikona=1):
  real_name = real_name or 'Rik{0}'.format(random.randint(1, 100000))
  dob = dob or random_dob()
  r = Rikishi.objects.create(real_name=real_name, dob=dob)
  base_basho_id = base_basho_id or get_current_basho_id()
  rank = rank or 0

  for i in range(num_shikona):
    cur_shikona = '{0}_Shikona{1}'.format(real_name, i + 1)
    OzumoBanzukeEntry.objects.create(rikishi=r, shikona=cur_shikona,
                                     rank=rank, basho_id=base_basho_id + i)

  return r


def get_or_create_kimarite(kimarite):
  kimarite = kimarite or get_random_string(length=8)
  existing = Kimarite.objects.filter(name=kimarite).first()
  return existing if existing else Kimarite.objects.create(name=kimarite)


def create_ozumo_bout(r1, r2, r1_rank, r2_rank, basho_id=None, day=None,
                      kimarite=None):
  basho_id = basho_id or get_current_basho_id()
  day = day or 1
  kimarite = get_or_create_kimarite(
    kimarite or random.choice(['yorikiri', 'uwatenage', 'shitatenage']))
  value = calculate_score(r1_rank, r2_rank)
  return OzumoBout.objects.create(
    winning_rikishi=r1, winning_rikishi_rank=r1_rank,
    losing_rikishi=r2, losing_rikishi_rank=r2_rank,
    basho_id=basho_id, day=day, kimarite=kimarite, value=value
  )


def create_oyakata():
  first_name = get_random_string(length=5)
  last_name = get_random_string(length=5)
  short_name = first_name
  rank = random.randint(-3, 10)
  style = get_random_string(length=10)
  quote = get_random_string(length=100)
  email = '%s@gmail.com' % get_random_string(length=8)
  image_path = 'boi/images/%s.gif' % short_name

  o = Oyakata.objects.create(
    first_name=first_name, last_name=last_name, short_name=short_name,
    rank=rank, style=style, quote=quote, email=email, image_path=image_path
  )

  return o


def create_basho_squadron(oyakata, shikona, basho_id, rank, place=None):
  return BashoSquadron.objects.create(
    oyakata=oyakata, shikona=shikona, basho_id=basho_id, rank=rank,
    place=place,
  )


def assign_rikishi_to_squadron(oyakata, basho_id, rikishi_list):
  if isinstance(rikishi_list, Rikishi):
    rikishi_list = [rikishi_list]

  for n, r in enumerate(rikishi_list):
    SquadronMember.objects.create(
      oyakata=oyakata,
      rikishi=r,
      basho_id=basho_id,
      selection_order=n + 1,
      is_active=True,
    )


def init_oyakata_with_shikona(shikona=None, basho_id=None, rank=None):
  if basho_id is None:
    basho_id = 584
  o = create_oyakata()
  shikona = shikona or get_random_string(length=8)
  rank = rank or 0
  create_basho_squadron(o, shikona, basho_id, rank)
  return o


def create_bout_result(o1, o1_score, o1_chanko, o1_bp, o2, o2_score, o2_chanko,
                       o2_bp, basho_id, day, order):
  return Bout.objects.create(
    oyakata1=o1, oyakata1_score=o1_score, oyakata1_chanko=o1_chanko,
    oyakata1_bp=o1_bp, oyakata2=o2, oyakata2_score=o2_score,
    oyakata2_chanko=o2_chanko, oyakata2_bp=o2_bp, basho_id=basho_id,
    day=day, order=order
  )


def init_bout_result(o1, o2, o1_wins=True, draw=False, basho_id=None, day=None,
                     order=None, o1_score=None, o1_chanko=None, o2_score=None,
                     o2_chanko=None):
  basho_id = basho_id or get_current_basho_id()
  day = day or 1
  order = order or 1
  if draw:
    if o1_score != o2_score:
      raise Exception('Attempted to create a drawn bout, but with '
                      'different scores for the oyakata.')
    if o1_score is None:
      o1_score = o2_score = 4
  elif o1_wins:
    if (o1_score is not None and o2_score is not None
        and o1_score <= o2_score):
      raise Exception('Attempted to create a bout where o1 wins, but o1 '
                      'did not have a higher score than o2.')
    o1_score = o1_score if o1_score is not None else 10
    o2_score = o2_score if o2_score is not None else 6
  else:
    if (o1_score is not None and o2_score is not None
        and o2_score <= o1_score):
      raise Exception('Attempted to create a bout where o2 wins, but o2 '
                      'did not have a higher score than o1.')
    o1_score = o1_score if o1_score is not None else 6
    o2_score = o2_score if o2_score is not None else 10
  o1_chanko = o1_chanko if o1_chanko is not None else 2
  o1_bp = o1_score + 2
  o2_chanko = o2_chanko if o2_chanko is not None else 2
  o2_bp = o2_score + 2
  return create_bout_result(o1, o1_score, o1_chanko, o1_bp,
                            o2, o2_score, o2_chanko, o2_bp,
                            basho_id, day, order)


def set_lineup(oyakata, rikishi_list, basho_id, day):
  result_list = []
  for n, rikishi in enumerate(rikishi_list):
    le = LineupEntry.objects.create(
      oyakata=oyakata, rikishi=rikishi, basho_id=basho_id, day=day,
      order=n + 1)
    result_list.append(le)

  return le


def get_oyakata_obj(shikona, basho_id):
  return BashoSquadron.objects.get(
    shikona=shikona,
    basho_id=basho_id,
  ).oyakata


def get_rikishi_obj(shikona, basho_id):
  return OzumoBanzukeEntry.objects.get(
    shikona=shikona,
    basho_id=basho_id,
  ).rikishi


def get_squadron_member_obj(oyakata_shikona, rikishi_shikona,
                            basho_id):
  return SquadronMember.objects.get(
    oyakata=get_oyakata_obj(oyakata_shikona, basho_id),
    rikishi=get_rikishi_obj(rikishi_shikona, basho_id),
    basho_id=basho_id,
  )


def _setUpTestData(cls):
  cls.basho_id = get_current_basho_id()
  cls.day = 1
  cls.o1 = init_oyakata_with_shikona(basho_id=cls.basho_id)
  cls.o2 = init_oyakata_with_shikona(basho_id=cls.basho_id)
  cls.o3 = init_oyakata_with_shikona(basho_id=cls.basho_id)
  cls.o4 = init_oyakata_with_shikona(basho_id=cls.basho_id)
  cls.br = init_bout_result(cls.o1, cls.o2, day=cls.day)
  cls.br_draw = init_bout_result(cls.o1, cls.o2, day=cls.day + 1,
                                 draw=True)
  cls.r1 = create_rikishi()
  cls.r2 = create_rikishi()

  cls.rikishi_master_list = [create_rikishi(real_name='Rikishi%s' % str(n + 1))
                             for n in xrange(50)]

  cls.rikishi_list1 = cls.rikishi_master_list[0:6]
  cls.rikishi_list2 = cls.rikishi_master_list[6:12]

  assign_rikishi_to_squadron(cls.o1, cls.basho_id, cls.rikishi_list1)
  assign_rikishi_to_squadron(cls.o2, cls.basho_id, cls.rikishi_list2)

  cls.kimarite_oshidashi = Kimarite.objects.create(
    name='oshidashi',
  )

  cls.ozumo_bouts = [create_ozumo_bout(
    cls.rikishi_master_list[i],
    cls.rikishi_master_list[i + 1],
    0,
    0,
    basho_id=cls.basho_id,
    day=1,
  ) for i in xrange(0, len(cls.rikishi_master_list) / 2 - 1, 2)]

  for o in [cls.o1, cls.o2]:
    for day in [1, 2]:
      SuicidePoolEntry.objects.create(
        oyakata=o,
        rikishi=cls.rikishi_master_list[0],
        basho_id=cls.basho_id,
        day=day,
      )

  cls.o1_basho_squadron = cls.o1.basho_squadron_set.get(
    basho_id=cls.basho_id)
  cls.o2_basho_squadron = cls.o2.basho_squadron_set.get(
    basho_id=cls.basho_id)


class BoiTestCase(TestCase):

  @classmethod
  def setUpTestData(cls):
    _setUpTestData(cls)


class BoiTransactionTestCase(TransactionTestCase):

  def setUp(self):
    _setUpTestData(self)


class RegisterPlayersViewTests(BoiTestCase):

  def test_initial_page(self):
    response = self.client.get(reverse('boi:register_players'),
                               {'b': self.basho_id + 1})
    self.assertEqual(response.status_code, 200)
    self.assertContains(response, self.o1_basho_squadron.shikona)
    self.assertContains(response, self.o2_basho_squadron.shikona)
    self.assertContains(response, self.o1.real_name)
    self.assertContains(response, self.o2.real_name)
    self.assertContains(response, str(self.basho_id + 1))

  def test_register_players(self):
    shikona1 = 'shikona1'
    shikona2 = 'shikona2'
    shikona3 = 'shikona3'
    shikona4 = 'shikona4'
    future_basho_id = 1000
    payload = {
      'player0': shikona1,
      'player1': shikona2,
      'player2': shikona3,
      'player3': shikona4,
    }
    response = self.client.post(
      '{0}?b={1}'.format(reverse('boi:register_players'),
                         future_basho_id),
      payload)
    self.assertRedirects(response, reverse('boi:utils'))


class SquadronManagementViewTests(BoiTestCase):

  def test_initial_page(self):
    response = self.client.get(reverse('boi:mgmt', args=[self.o1.id]))
    self.assertEqual(response.status_code, 200)

    # Ensure all rikishi are available to be picked
    for r in self.rikishi_list1:
      self.assertContains(response, r.shikona_for_basho(self.basho_id))

  def test_submit_valid_lineup(self):
    pre_trash_talk = 'pre trash talk'
    post_trash_talk = 'post trash talk'
    suicide_rikishi = self.rikishi_master_list[-1]
    suicide_pick = OzumoBanzukeEntry.objects.get(
      rikishi=suicide_rikishi,
      basho_id=self.basho_id,
    ).id
    day = 3
    form_data = {
      'pre_trash_talk': pre_trash_talk,
      'starting_rikishi': [
        r.id for r in self.rikishi_list1[:LINEUP_SIZE]
      ],
      'post_trash_talk': post_trash_talk,
      'suicide_pick': suicide_pick,
      'active_day': day,
    }

    response = self.client.post(reverse('boi:mgmt', args=[self.o1.id]),
                                form_data)
    self.assertEqual(response.status_code, 302)
    self.assertRedirects(
      response,
      reverse('boi:matchups') + '?b={0}&d={1}'.format(self.basho_id, day))

    lineup_entries = LineupEntry.objects.filter(
      basho_id=self.basho_id,
      oyakata=self.o1,
      day=day,
    ).order_by(
      'order'
    )

    self.assertEqual(lineup_entries.count(), LINEUP_SIZE)

    # Make sure suicide entry is recorded
    suicide_entries = SuicidePoolEntry.objects.filter(
      oyakata=self.o1,
      basho_id=self.basho_id,
      day=day,
    )
    self.assertEqual(suicide_entries.count(), 1)
    suicide_entry = suicide_entries.first()
    self.assertEqual(suicide_entry.rikishi, suicide_rikishi)

  def test_submit_lineup_too_few_rikishi(self):
    pre_trash_talk = 'pre trash talk'
    post_trash_talk = 'post trash talk'
    suicide_pick = OzumoBanzukeEntry.objects.get(
      rikishi=self.rikishi_master_list[-1],
      basho_id=self.basho_id,
    ).id
    day = 3
    form_data = {
      'pre_trash_talk': pre_trash_talk,
      'starting_rikishi': [
        r.id for r in self.rikishi_list1[:LINEUP_SIZE - 1]
      ],
      'post_trash_talk': post_trash_talk,
      'suicide_pick': suicide_pick,
      'active_day': day,
    }

    response = self.client.post(reverse('boi:mgmt', args=[self.o1.id]),
                                form_data)
    self.assertEqual(response.status_code, 200)
    self.assertContains(response, 'Submit')

    # Make sure no lineup entries are added
    lineup_entries = LineupEntry.objects.filter(
      oyakata=self.o1,
      basho_id=self.basho_id,
      day=day,
    )
    self.assertEqual(lineup_entries.count(), 0)

    # Make sure no suicide entries are recorded
    suicide_entry = SuicidePoolEntry.objects.filter(
      oyakata=self.o1,
      basho_id=self.basho_id,
      day=day,
    )
    self.assertEqual(suicide_entry.count(), 0)

  def test_submit_lineup_too_many_rikishi(self):
    pre_trash_talk = 'pre trash talk'
    post_trash_talk = 'post trash talk'
    suicide_pick = OzumoBanzukeEntry.objects.get(
      rikishi=self.rikishi_master_list[-1],
      basho_id=self.basho_id,
    ).id
    day = 3
    form_data = {
      'pre_trash_talk': pre_trash_talk,
      'starting_rikishi': [
        r.id for r in self.rikishi_list1[:LINEUP_SIZE + 1]
      ],
      'post_trash_talk': post_trash_talk,
      'suicide_pick': suicide_pick,
      'active_day': day,
    }

    response = self.client.post(reverse('boi:mgmt', args=[self.o1.id]),
                                form_data)
    self.assertEqual(response.status_code, 200)
    self.assertContains(response, 'Submit')

    # Make sure no lineup entries are added
    lineup_entries = LineupEntry.objects.filter(
      oyakata=self.o1,
      basho_id=self.basho_id,
      day=day,
    )
    self.assertEqual(lineup_entries.count(), 0)

    # Make sure no suicide entries are recorded
    suicide_entry = SuicidePoolEntry.objects.filter(
      oyakata=self.o1,
      basho_id=self.basho_id,
      day=day,
    )
    self.assertEqual(suicide_entry.count(), 0)


class BashoSquadronTests(BoiTestCase):

  def test_basho_record(self):
    # Check that o1 is 1-0-1
    self.assertEqual(1, self.o1_basho_squadron.wins)
    self.assertEqual(0, self.o1_basho_squadron.losses)
    self.assertEqual(1, self.o1_basho_squadron.draws)

    # Check that o2 is 0-1-1
    self.assertEqual(0, self.o2_basho_squadron.wins)
    self.assertEqual(1, self.o2_basho_squadron.losses)
    self.assertEqual(1, self.o2_basho_squadron.draws)

  def test_suicide_picks(self):
    oyakata = init_oyakata_with_shikona(basho_id=self.basho_id)
    suicide_pool_entry = SuicidePoolEntry.objects.create(
      oyakata=oyakata,
      rikishi=self.rikishi_master_list[0],
      basho_id=self.basho_id,
      day=1,
    )
    basho_squadron = oyakata.basho_squadron_set.get(
      basho_id=self.basho_id)
    expected = {
      1: suicide_pool_entry
    }
    self.assertEqual(expected, basho_squadron.suicide_picks)

  def test_num_suicide_strikes(self):
    self.assertEqual(0, self.o1_basho_squadron.num_suicide_strikes)

  def test_num_suicide_entries(self):
    oyakata = init_oyakata_with_shikona(basho_id=self.basho_id)
    SuicidePoolEntry.objects.create(
      oyakata=oyakata,
      rikishi=self.rikishi_master_list[0],
      basho_id=self.basho_id,
      day=1,
    )
    basho_squadron = oyakata.basho_squadron_set.get(
      basho_id=self.basho_id)
    self.assertEqual(1, basho_squadron.num_suicide_entries)

  def test_suicide_still_alive(self):
    self.assertEqual(True, self.o1_basho_squadron.suicide_still_alive)

  def test_total_points(self):
    self.assertEqual(14, self.o1_basho_squadron.total_points)

  def test_total_chanko(self):
    self.assertEqual(4, self.o1_basho_squadron.total_chanko)

  def test_points_against(self):
    self.assertEqual(10, self.o1_basho_squadron.points_against)

  def test_dominance_factor(self):
    self.assertEqual(14. / 10., self.o1_basho_squadron.dominance_factor)

  def test_points_possible(self):
    self.assertEqual(18, self.o1_basho_squadron.points_possible)

  def test_coach_rating(self):
    self.assertEqual(14. / 18., self.o1_basho_squadron.coach_rating)

  def test_standings_points(self):
    self.assertEqual(3, self.o1_basho_squadron.standings_points)

  def test_num_squadron_selections(self):
    self.assertEqual(6, self.o1_basho_squadron.num_squadron_selections)


class BoutModelTests(BoiTestCase):

  def test_result_string_o1_wins(self):
    expected = '{0} no kachi desu! ({1}-{2})'.format(
      self.o1.most_recent_shikona,
      self.br.oyakata1_score,
      self.br.oyakata2_score)
    self.assertEqual(expected, self.br.result_string)

  def test_get_oyakata1_shikona(self):
    self.assertEqual(self.o1.most_recent_shikona,
                     self.br.oyakata1_shikona)

  def test_get_rikishi_value(self):
    r1_rank = -2
    r2_rank = -3
    create_ozumo_bout(self.r1, self.r2, r1_rank, r2_rank,
                      basho_id=self.basho_id, day=self.day)
    result = self.br.get_rikishi_value(self.r1)
    self.assertEqual(3, result)

    result = self.br.get_rikishi_value(self.r2)
    self.assertEqual(0, result)

  def test_get_oyakata1_day_one_score(self):
    basho_id = 678
    day_one = 12
    oyakata1_day_one_score = 5
    # Create day one score
    Bout.objects.create(
      basho_id=basho_id,
      day=day_one,
      order=1,
      oyakata1=self.o1,
      oyakata1_score=oyakata1_day_one_score,
      oyakata2=self.o2,
      oyakata2_score=0,
    )
    day_two_bout = Bout.objects.create(
      basho_id=basho_id,
      day=day_one + 1,
      order=1,
      oyakata1=self.o1,
      oyakata2=self.o2,
    )
    self.assertEqual(oyakata1_day_one_score,
                     day_two_bout.oyakata1_day_one_score)

  def test_get_oyakata2_day_one_score(self):
    basho_id = 678
    day_one = 12
    oyakata2_day_one_score = 5
    # Create day one score
    Bout.objects.create(
      basho_id=basho_id,
      day=day_one,
      order=1,
      oyakata1=self.o1,
      oyakata1_score=0,
      oyakata2=self.o2,
      oyakata2_score=oyakata2_day_one_score,
    )
    day_two_bout = Bout.objects.create(
      basho_id=basho_id,
      day=day_one + 1,
      order=1,
      oyakata1=self.o1,
      oyakata2=self.o2,
    )
    self.assertEqual(oyakata2_day_one_score,
                     day_two_bout.oyakata2_day_one_score)

  def test_get_oyakata1_day_one_score_no_score_raises_exception(self):
    basho_id = 678
    day_one = 12
    day_two_bout = Bout.objects.create(
      basho_id=basho_id,
      day=day_one + 1,
      order=1,
      oyakata1=self.o1,
      oyakata2=self.o2,
    )
    with self.assertRaises(BoiNoDayOneScoreException):
      day_two_bout.oyakata1_day_one_score

  def test_get_oyakata2_day_one_score_no_score_raises_exception(self):
    basho_id = 678
    day_one = 12
    day_two_bout = Bout.objects.create(
      basho_id=basho_id,
      day=day_one + 1,
      order=1,
      oyakata1=self.o1,
      oyakata2=self.o2,
    )
    with self.assertRaises(BoiNoDayOneScoreException):
      day_two_bout.oyakata2_day_one_score


class LineupEntryModelTests(BoiTestCase):
  pass


class OyakataModelTests(BoiTestCase):

  def test_shikona_for_basho(self):
    oyakata = create_oyakata()
    shikona = 'My Shikona'
    create_basho_squadron(oyakata, shikona, self.basho_id, 0)
    result = oyakata.shikona_for_basho(self.basho_id)
    self.assertEqual(result, shikona)

  def test_rank_for_basho(self):
    oyakata = create_oyakata()
    shikona = 'My Shikona'
    rank = -3
    create_basho_squadron(oyakata, shikona, self.basho_id, rank)
    result = oyakata.rank_for_basho(self.basho_id)
    self.assertEqual(result, rank)


class RikishiModelTests(BoiTestCase):

  def test_most_recent_shikona(self):
    """
    most_recent_shikona returns the most recent shikona.
    """
    num_shikona = 3
    rikishi = create_rikishi(num_shikona=num_shikona)
    self.assertEqual(rikishi.most_recent_shikona,
                     '{0}_Shikona{1}'.format(rikishi.real_name,
                                             num_shikona))

  def test_rank_for_basho(self):
    basho_id = 1203
    rank = 5
    rikishi = create_rikishi(rank=rank, base_basho_id=basho_id)
    self.assertEqual(rikishi.rank_for_basho(basho_id), rank)


class UtilsTests(BoiTestCase):

  def test_calculate_score_same_rank(self):
    result = calculate_score(-3, -3)
    self.assertEqual(2, result)

  def test_calculate_score_ozeki_beats_yokozuna(self):
    result = calculate_score(-2, -3)
    self.assertEqual(3, result)

  def test_calculate_score_m1_beats_yokozuna(self):
    result = calculate_score(1, -3)
    self.assertEqual(6, result)

  def test_calculate_score_m15_beats_yokozuna(self):
    result = calculate_score(15, -3)
    self.assertEqual(9, result)

  def test_calculate_score_m15_beats_m12(self):
    result = calculate_score(15, 12)
    self.assertEqual(3, result)

  def test_calculate_score_m13_beats_m12(self):
    result = calculate_score(13, 12)
    self.assertEqual(3, result)

  def test_calculate_score_yokozuna_beats_m15(self):
    result = calculate_score(-3, 15)
    self.assertEqual(2, result)

  def test_get_lineup(self):
    set_lineup(self.o1, self.rikishi_list1, self.basho_id, self.day)

    expected = [l.shikona_for_basho(self.basho_id)
                for l in self.rikishi_list1]
    result = _get_lineup(self.o1, self.basho_id, self.day)
    self.assertEqual(expected, result)

  def test_get_lineup_forced_previous_day_lineup(self):
    set_lineup(self.o1, self.rikishi_list1, self.basho_id, self.day)

    # Now, try to get lineup for day 2. Should fall back to day 1's lineup
    day = self.day + 1
    expected = [l.shikona_for_basho(self.basho_id)
                for l in self.rikishi_list1]
    result = _get_lineup(self.o1, self.basho_id, day, force=True)
    self.assertEqual(expected, result)

  def test_get_lineup_unforced_previous_day_lineup(self):
    set_lineup(self.o1, self.rikishi_list1, self.basho_id, self.day)

    # Now, try to get lineup for day 2. Should return [] since not forced
    day = self.day + 1
    result = _get_lineup(self.o1, self.basho_id, day, force=False)
    self.assertEqual([], list(result))

  def test_get_basho_year(self):
    basho_id = 545
    self.assertEqual(2011, get_basho_year(basho_id))

  def test_get_basho_year_after_skipped_basho_2011(self):
    basho_id = 550
    self.assertEqual(2012, get_basho_year(basho_id))

  def test_get_basho_year_after_skipped_basho_2020(self):
    basho_id = 603
    self.assertEqual(2021, get_basho_year(basho_id))

  def test_get_basho_name(self):
    basho_id = 545
    self.assertEqual('Hatsu', get_basho_name(basho_id))

  def test_get_basho_name_after_skipped_basho_2011(self):
    basho_id = 550
    self.assertEqual('Hatsu', get_basho_name(basho_id))

  def test_get_basho_name_after_skipped_basho_2020(self):
    basho_id = 603
    self.assertEqual('Hatsu', get_basho_name(basho_id))

  def test_get_basho_full_name(self):
    basho_id = 545
    self.assertEqual('Hatsu 2011', get_basho_full_name(basho_id))

  def test_get_basho_ids_in_year(self):
    self.assertEqual([539, 540, 541, 542, 543, 544],
                     get_basho_ids_in_year(2010))
    self.assertEqual([545, 546, 547, 548, 549],
                     get_basho_ids_in_year(2011))
    self.assertEqual([592, 593, 594, 595, 596, 597],
                     get_basho_ids_in_year(2019))
    self.assertEqual([598, 599, 600, 601, 602],
                     get_basho_ids_in_year(2020))
    self.assertEqual([603, 604, 605, 606, 607, 608],
                     get_basho_ids_in_year(2021))

  def test_convert_rank_to_full_string(self):
    self.assertEqual('Yokozuna', convert_rank_to_full_string(-3))
    self.assertEqual('Ozeki', convert_rank_to_full_string(-2))
    self.assertEqual('Sekiwake', convert_rank_to_full_string(-1))
    self.assertEqual('Komusubi', convert_rank_to_full_string(0))
    self.assertEqual('Maegashira-1', convert_rank_to_full_string(1))
    self.assertEqual('Juryo-5', convert_rank_to_full_string(1005))
    self.assertEqual('Makushita-11', convert_rank_to_full_string(2011))
    self.assertEqual('Sandanme-56', convert_rank_to_full_string(3056))
    self.assertEqual('Jonidan-31', convert_rank_to_full_string(4031))
    self.assertEqual('Jonokuchi-20', convert_rank_to_full_string(5020))

  def test_convert_rank_to_short_string(self):
    self.assertEqual('Y', convert_rank_to_short_string(-3))
    self.assertEqual('O', convert_rank_to_short_string(-2))
    self.assertEqual('S', convert_rank_to_short_string(-1))
    self.assertEqual('K', convert_rank_to_short_string(0))
    self.assertEqual('M1', convert_rank_to_short_string(1))
    self.assertEqual('J5', convert_rank_to_short_string(1005))
    self.assertEqual('Ms11', convert_rank_to_short_string(2011))
    self.assertEqual('Sd56', convert_rank_to_short_string(3056))
    self.assertEqual('Jd31', convert_rank_to_short_string(4031))
    self.assertEqual('Jk20', convert_rank_to_short_string(5020))


class TiebreakerTests(TestCase):

  @classmethod
  def setUpTestData(cls):
    cls.basho_id = 600
    cls.day = 1
    cls.num_oyakata = 12
    cls.oyakata_list = [
      init_oyakata_with_shikona(basho_id=cls.basho_id,
                                shikona='Player%s' % str(n + 1))
      for n in xrange(cls.num_oyakata)]
    cls.basho_squadron_list = [o.basho_squadron_set.get(
      basho_id=cls.basho_id) for o in cls.oyakata_list]

    cls.rikishi_master_list = [
      create_rikishi(real_name='Rikishi%s' % str(n + 1))
      for n in xrange(cls.num_oyakata * 6 + 6)]

    cls.rikishi_lists = [cls.rikishi_master_list[n * 6:(n * 6) + 6]
                         for n in xrange(cls.num_oyakata)]

    for (n, o) in enumerate(cls.oyakata_list):
      assign_rikishi_to_squadron(o, cls.basho_id, cls.rikishi_lists[n])

  def test_break_ties(self):
    # Create a three-way tie on points/chanko
    # All squadrons have two wins
    # #2 has best head-to-head record, followed by #1 and #0
    # Expected order: o2, o1, o0
    Bout.objects.create(
      oyakata1=self.oyakata_list[0],
      oyakata1_score=6,
      oyakata1_chanko=0,
      oyakata1_bp=6,
      oyakata2=self.oyakata_list[3],
      oyakata2_score=4,
      oyakata2_chanko=0,
      oyakata2_bp=4,
      basho_id=self.basho_id,
      day=1,
      order=1,
    )
    Bout.objects.create(
      oyakata1=self.oyakata_list[1],
      oyakata1_score=6,
      oyakata1_chanko=0,
      oyakata1_bp=6,
      oyakata2=self.oyakata_list[0],
      oyakata2_score=0,
      oyakata2_chanko=0,
      oyakata2_bp=0,
      basho_id=self.basho_id,
      day=2,
      order=1,
    )
    Bout.objects.create(
      oyakata1=self.oyakata_list[2],
      oyakata1_score=6,
      oyakata1_chanko=0,
      oyakata1_bp=6,
      oyakata2=self.oyakata_list[0],
      oyakata2_score=0,
      oyakata2_chanko=0,
      oyakata2_bp=0,
      basho_id=self.basho_id,
      day=3,
      order=1,
    )
    Bout.objects.create(
      oyakata1=self.oyakata_list[2],
      oyakata1_score=6,
      oyakata1_chanko=0,
      oyakata1_bp=6,
      oyakata2=self.oyakata_list[1],
      oyakata2_score=0,
      oyakata2_chanko=0,
      oyakata2_bp=0,
      basho_id=self.basho_id,
      day=4,
      order=1,
    )
    Bout.objects.create(
      oyakata1=self.oyakata_list[0],
      oyakata1_score=6,
      oyakata1_chanko=0,
      oyakata1_bp=6,
      oyakata2=self.oyakata_list[4],
      oyakata2_score=0,
      oyakata2_chanko=0,
      oyakata2_bp=0,
      basho_id=self.basho_id,
      day=5,
      order=1,
    )
    Bout.objects.create(
      oyakata1=self.oyakata_list[1],
      oyakata1_score=6,
      oyakata1_chanko=0,
      oyakata1_bp=6,
      oyakata2=self.oyakata_list[5],
      oyakata2_score=0,
      oyakata2_chanko=0,
      oyakata2_bp=0,
      basho_id=self.basho_id,
      day=5,
      order=1,
    )

    squadron_sorter = SquadronSorter()
    tiebroken_squadrons = squadron_sorter._break_ties(
      self.basho_squadron_list[:3])
    self.assertEqual(self.basho_squadron_list[2::-1], tiebroken_squadrons)

    sorted_squadrons = squadron_sorter.sort(
      self.basho_squadron_list[:3])
    self.assertEqual(self.basho_squadron_list[2::-1], sorted_squadrons)


class ViewUtilsTests(BoiTestCase):

  def test_create_divisions(self):
    basho_id = 601
    num_oyakata = 12
    oyakata_list = [init_oyakata_with_shikona(basho_id=basho_id, rank=n - 3)
                    for n in range(num_oyakata)]

    # Create the divisions
    division_members = create_divisions(basho_id)
    self.assertEqual(num_oyakata, len(division_members))

    # Test each division
    for cur_division in range(3):
      for cur_oyakata in range(cur_division, num_oyakata, 3):
        self.assertEqual(cur_division + 1,
                         division_members[cur_oyakata].division)
        self.assertEqual(oyakata_list[cur_oyakata],
                         division_members[cur_oyakata].oyakata)
        self.assertEqual(basho_id,
                         division_members[cur_oyakata].basho_id)

  def test_get_current_day(self):
    basho_id = 600
    num_days_to_populate = 6

    for day in xrange(num_days_to_populate):
      init_bout_result(self.o1, self.o2, basho_id=basho_id, day=day + 1)

    result = get_current_day(basho_id=basho_id)
    self.assertEqual(7, result)

  def test_get_current_day_no_results(self):
    basho_id = 600
    result = get_current_day(basho_id=basho_id)
    self.assertEqual(0, result)


class RegisterPlayersFormTests(BoiTestCase):

  def test_register_players_form(self):
    old_basho_squadrons = BashoSquadron.objects.filter(
      basho_id=self.basho_id,
    )
    form = RegisterPlayersForm(old_basho_squadrons=old_basho_squadrons)
    visible_fields = form.visible_fields()
    self.assertEqual(4, len(visible_fields))

    # Check o1
    o1_field = visible_fields[0]
    self.assertEqual(o1_field.label, self.o1.real_name)
    self.assertEqual(o1_field.initial, self.o1_basho_squadron.shikona)

    # Check o2
    o2_field = visible_fields[1]
    self.assertEqual(o2_field.label, self.o2.real_name)
    self.assertEqual(o2_field.initial, self.o2_basho_squadron.shikona)


class ScraperTests(TestCase):

  def test_banzuke_scraper(self):
    basho_id = 586
    scraper = BanzukeScraper(basho_id)

    # Read in raw banzuke data
    with open(os.path.join(TESTDATA_BASEDIR, 'banzuke',
                           'banzuke.html')) as f:
      raw_banzuke = f.read()

    rikishi_list = scraper._process_raw_banzuke(raw_banzuke)

    with open(os.path.join(TESTDATA_BASEDIR, 'banzuke',
                           'expected.dat')) as f:
      expected_rikishi_list = f.readlines()
    for n, expected_rikishi in enumerate(expected_rikishi_list):
      rikishi = rikishi_list[n]
      tokens = expected_rikishi.strip().split(',')
      self.assertEqual(rikishi[0], tokens[0])
      self.assertEqual(rikishi[1], int(tokens[1]))
      self.assertEqual(rikishi[2], tokens[2])
      self.assertEqual(rikishi[3], tokens[3])

  def test_rikishi_profile_scraper(self):
    scraper = RikishiProfileScraper('')

    # Read in raw profile data
    with open(os.path.join(TESTDATA_BASEDIR, 'rikishiprofile',
                           'rikishiprofile.html')) as f:
      raw_profile = f.read()

    profile = scraper._process_raw_profile(raw_profile)

    self.assertEqual(profile['Real Name'], u'Davaajargal Mönkhbat')
    self.assertEqual(profile['Birth Date'],
                     datetime.date(year=1985, month=3, day=11))
    self.assertEqual(profile['Heya'], 'Miyagino')

  def test_ozumo_results_scraper(self):
    basho_id = 586
    day = 1
    scraper = OzumoResultsScraper(basho_id, day)

    # Create Rikishi and OzumoBanzukeEntries for all of the relevant rikishi
    with open(os.path.join(TESTDATA_BASEDIR, 'ozumoresults',
                           'ozumoresults.dat')) as f:
      rikishi_list_raw = f.read()
    for rikishi in rikishi_list_raw.split('\n'):
      tokens = rikishi.split(',')
      if len(tokens) != 2:
        continue
      shikona = tokens[0]
      rank = int(tokens[1])
      rikishi_obj = Rikishi.objects.create(
        real_name=shikona,
        dob=random_dob(),
        web_id=shikona,
      )
      OzumoBanzukeEntry.objects.create(
        rikishi=rikishi_obj,
        shikona=shikona,
        basho_id=basho_id,
        rank=rank,
      )

    # Read in test results file
    with open(os.path.join(TESTDATA_BASEDIR, 'ozumoresults',
                           'ozumoresults.html')) as f:
      raw_results = f.read()
    bouts = scraper._process_raw_results(raw_results)

    self.assertIsNotNone(bouts)

    # Read in expected results
    with open(os.path.join(TESTDATA_BASEDIR, 'ozumoresults',
                           'expected.dat')) as f:
      expected_results = f.readlines()
    for n, r in enumerate(expected_results):
      bout = bouts[n]
      tokens = r.strip().split(',')
      self.assertEqual(
        bout['winning_rikishi'].shikona_for_basho(basho_id), tokens[0])
      self.assertEqual(bout['winning_rikishi_rank'],
                       int(tokens[1]))
      self.assertEqual(
        bout['losing_rikishi'].shikona_for_basho(basho_id), tokens[2])
      self.assertEqual(bout['losing_rikishi_rank'],
                       int(tokens[3]))
      self.assertEqual(bout['kimarite'].name, tokens[4])
      self.assertEqual(bout['value'], int(tokens[5]))
      self.assertEqual(bout['basho_id'], basho_id)
      self.assertEqual(bout['day'], day)


class DewsweeperEmailerTests(TestCase):

  @classmethod
  def setUpTestData(cls):
    cls.basho_id = get_current_basho_id()
    cls.day = 1

    # Initialize oyakata for all 12 players
    cls.oyakata_list = [init_oyakata_with_shikona(shikona='shikona%s' % n,
                                                  basho_id=cls.basho_id)
                        for n in xrange(12)]
    cls.basho_squadron_list = [
      oyakata.basho_squadron_set.get(basho_id=cls.basho_id)
      for oyakata in cls.oyakata_list
    ]

    # Create some rikishi to work with
    cls.rikishi_master_list = [
      create_rikishi(real_name='Rikishi%s' % str(n + 1),
                     base_basho_id=cls.basho_id) for n in xrange(12 * 6)
    ]

    # Assign the rikishi to the squadrons, then
    # create SuicidePoolEntries and LineupEntries.
    for n, oyakata in enumerate(cls.oyakata_list):
      assign_rikishi_to_squadron(
        oyakata, cls.basho_id, cls.rikishi_master_list[n * 6:(n * 6) + 6])
      SuicidePoolEntry.objects.create(
        oyakata=oyakata,
        rikishi=cls.rikishi_master_list[n],
        basho_id=cls.basho_id,
        day=cls.day,
      )
      basho_squadron = cls.basho_squadron_list[n]
      squadron_members = basho_squadron.squadron_members
      for i in xrange(4):
        LineupEntry.objects.create(
          oyakata=oyakata,
          rikishi=squadron_members[i].rikishi,
          basho_id=cls.basho_id,
          day=cls.day,
          order=i + 1,
        )

    # Create ozumo bouts
    cls.results_by_rikishi = {}
    for n in xrange((12 * 6) / 2):
      if n % 2 == 0:
        r1 = cls.rikishi_master_list[n]
        r2 = cls.rikishi_master_list[n + ((12 * 6) / 2)]
      else:
        r1 = cls.rikishi_master_list[n + ((12 * 6) / 2)]
        r2 = cls.rikishi_master_list[n]
      create_ozumo_bout(
        r1, r2, 0, 0, basho_id=cls.basho_id, day=cls.day
      )
      cls.results_by_rikishi[r1.shikona_for_basho(cls.basho_id)] = (
        2, 'oshidashi'
      )
      cls.results_by_rikishi[r2.shikona_for_basho(cls.basho_id)] = (
        0, 'oshidashi'
      )

    # Create BoI Bouts (the schedule)
    cls.bouts = []
    for i in xrange(6):
      b = Bout.objects.create(
        oyakata1=cls.oyakata_list[i * 2],
        oyakata2=cls.oyakata_list[(i * 2) + 1],
        basho_id=cls.basho_id,
        day=cls.day,
        order=i + 1,
      )
      b.oyakata1_score, b.oyakata1_chanko, b.oyakata1_bp = (
        score_lineup(b.oyakata1_lineup, b.oyakata1_squadron,
                     cls.results_by_rikishi, cls.basho_id)
      )
      b.oyakata2_score, b.oyakata2_chanko, b.oyakata2_bp = (
        score_lineup(b.oyakata2_lineup, b.oyakata2_squadron,
                     cls.results_by_rikishi, cls.basho_id)
      )
      b.save()
      cls.bouts.append(b)

  def test_dewsweeper_emailer(self):
    dewsweeper_emailer = DewsweeperEmailer(
      self.bouts, self.results_by_rikishi, self.basho_squadron_list,
      self.basho_id, self.day, recipient_list=['brad.friedman@gmail.com']
    )

    email_message = dewsweeper_emailer._build_email_message()

    # Read in expected message
    with open(os.path.join(TESTDATA_BASEDIR, 'emailers', 'dewsweeper',
                           'expected_day_1.dat')) as f:
      expected_email_message = f.read()

    assertEqualHTML(expected_email_message.strip(),
                    email_message.strip())

    # Note: This won't actually send an email.
    #       Django unit tests send all test email to mail.outbox
    dewsweeper_emailer.send_email()
    self.assertEqual(1, len(mail.outbox))


class DewsweeperProcessorTests(TestCase):

  @classmethod
  def setUpTestData(cls):
    cls.basho_id = 610
    cls.day = 1
    cls.last_day_of_reg_season = 11
    cls.semis_day_one = 12
    cls.semis_day_two = 13

    # Initialize oyakata for all 12 players
    cls.oyakata_list = [init_oyakata_with_shikona(shikona='shikona%s' % n,
                                                  basho_id=cls.basho_id)
                        for n in xrange(12)]
    cls.basho_squadron_list = [
      oyakata.basho_squadron_set.get(basho_id=cls.basho_id)
      for oyakata in cls.oyakata_list
    ]

    # Create some rikishi to work with
    cls.rikishi_master_list = [
      create_rikishi(real_name='Rikishi%s' % str(n + 1),
                     base_basho_id=cls.basho_id) for n in xrange(12 * 6)
    ]

    # Assign the rikishi to the squadrons, then
    # create SuicidePoolEntries and LineupEntries.
    for n, oyakata in enumerate(cls.oyakata_list):
      assign_rikishi_to_squadron(
        oyakata, cls.basho_id, cls.rikishi_master_list[n * 6:(n * 6) + 6])
      basho_squadron = cls.basho_squadron_list[n]
      squadron_members = basho_squadron.squadron_members
      for day in range(15):
        for i in xrange(4):
          LineupEntry.objects.create(
            oyakata=oyakata,
            rikishi=squadron_members[i].rikishi,
            basho_id=cls.basho_id,
            day=day + 1,
            order=i + 1,
          )

    # Create a Kimarite object
    cls.kimarite_oshidashi = Kimarite.objects.create(
      name='oshidashi',
    )

    # These two dicts map from a day (int) to the relevant results
    cls.day_results = collections.defaultdict(list)
    cls.day_results_by_rikishi = collections.defaultdict(dict)

    # Create ozumo bouts for a regular day and for the last day of
    # a regular season
    for n in xrange((12 * 6) / 2):
      if n % 2 == 0:
        r1 = cls.rikishi_master_list[n]
        r2 = cls.rikishi_master_list[n + ((12 * 6) / 2)]
      else:
        r1 = cls.rikishi_master_list[n + ((12 * 6) / 2)]
        r2 = cls.rikishi_master_list[n]
      winning_rikishi_rank = losing_rikishi_rank = 0
      create_ozumo_bout(
        r1, r2, 0, 0, basho_id=cls.basho_id, day=cls.day
      )
      for cur_day in [cls.day, cls.last_day_of_reg_season,
                      cls.semis_day_one, cls.semis_day_two]:
        cls.day_results[cur_day].append({
          'winning_rikishi': r1,
          'winning_rikishi_rank': winning_rikishi_rank,
          'losing_rikishi': r2,
          'losing_rikishi_rank': losing_rikishi_rank,
          'kimarite': cls.kimarite_oshidashi,
          'value': 2,
          'basho_id': cls.basho_id,
          'day': cur_day,
        })
        cls.day_results_by_rikishi[
          cur_day][r1.shikona_for_basho(cls.basho_id)] = (2, 'oshidashi')
        cls.day_results_by_rikishi[
          cur_day][r2.shikona_for_basho(cls.basho_id)] = (0, 'oshidashi')

    for cur_day in [cls.day, cls.last_day_of_reg_season,
                    cls.semis_day_one, cls.semis_day_two]:
      # Create BoI Bouts (the schedule)
      for i in xrange(6):
        b = Bout.objects.create(
          oyakata1=cls.oyakata_list[i * 2],
          oyakata2=cls.oyakata_list[(i * 2) + 1],
          basho_id=cls.basho_id,
          day=cur_day,
          order=i + 1,
        )

  def test_score_lineup(self):
    my_day = 1
    oyakata = self.oyakata_list[0]
    my_results_by_rikishi = {}

    # Get SquadronMember objects for an oyakata
    squadron_members = SquadronMember.objects.filter(
      oyakata=oyakata,
      basho_id=self.basho_id,
    ).order_by('selection_order')

    for n, val in enumerate([4, 0, 0, 0, 2, 2]):
      shikona = squadron_members[n].rikishi.shikona_for_basho(
        self.basho_id)
      my_results_by_rikishi[shikona] = (val, 'oshidashi')

    # Get Bout object
    bout = Bout.objects.get(
      basho_id=self.basho_id,
      day=my_day,
      oyakata1=oyakata,
    )

    results = score_lineup(bout.oyakata1_lineup, bout.oyakata1_squadron,
                           my_results_by_rikishi, self.basho_id)

    self.assertEqual((4, 2, 8), results)

  def test_dewsweeper_processor(self):
    dewsweeper_processor = DewsweeperProcessor(
      self.basho_id, self.day, True)
    dewsweeper_processor._process_ozumo_results(
      self.day_results[self.day], self.day_results_by_rikishi[self.day]
    )

    # Ensure the results email went out
    self.assertEqual(1, len(mail.outbox))

    # Read in expected message
    with open(os.path.join(TESTDATA_BASEDIR, 'emailers', 'dewsweeper',
                           'expected_day_1.dat')) as f:
      expected_email_message = f.read()

    assertEqualHTML(expected_email_message.strip(),
                    mail.outbox[0].body)

  def test_dewsweeper_processor_last_day_of_reg_season(self):
    # Remove the playoff bouts created in setUpTestData
    Bout.objects.filter(
      basho_id=self.basho_id,
      day__gt=self.last_day_of_reg_season,
    ).delete()

    dewsweeper_processor = DewsweeperProcessor(
      self.basho_id, self.last_day_of_reg_season, True)
    dewsweeper_processor._process_ozumo_results(
      self.day_results[self.last_day_of_reg_season],
      self.day_results_by_rikishi[self.last_day_of_reg_season],
    )

    # Ensure the results email went out
    self.assertEqual(1, len(mail.outbox))

    # Read in expected message
    with open(os.path.join(TESTDATA_BASEDIR, 'emailers', 'dewsweeper',
                           'expected_day_11.dat')) as f:
      expected_email_message = f.read()

    assertEqualHTML(expected_email_message.strip(),
                    mail.outbox[0].body)

    # Ensure that the playoffs are scheduled
    day_one_bouts = Bout.objects.filter(
      basho_id=self.basho_id,
      day=self.last_day_of_reg_season + 1,
    )
    self.assertEqual(6, len(day_one_bouts))
    day_two_bouts = Bout.objects.filter(
      basho_id=self.basho_id,
      day=self.last_day_of_reg_season + 2,
    )
    self.assertEqual(6, len(day_two_bouts))

  def test_dewsweeper_processor_day_one_of_semis(self):
    dewsweeper_processor = DewsweeperProcessor(
      self.basho_id, self.semis_day_one, True)
    dewsweeper_processor._process_ozumo_results(
      self.day_results[self.semis_day_one],
      self.day_results_by_rikishi[self.semis_day_one],
    )

    # Ensure the results email went out
    self.assertEqual(1, len(mail.outbox))

    # Read in expected message
    with open(os.path.join(TESTDATA_BASEDIR, 'emailers', 'dewsweeper',
                           'expected_day_12.dat')) as f:
      expected_email_message = f.read()

    assertEqualHTML(expected_email_message.strip(),
                    mail.outbox[0].body)

  def test_dewsweeper_processor_day_two_of_semis(self):
    # Create some day one results
    for b in Bout.objects.filter(
        basho_id=self.basho_id,
        day=self.semis_day_one,
    ):
      b.oyakata1_score = b.oyakata2_score = 3
      b.save()

    dewsweeper_processor = DewsweeperProcessor(
      self.basho_id, self.semis_day_two, True)
    dewsweeper_processor._process_ozumo_results(
      self.day_results[self.semis_day_two],
      self.day_results_by_rikishi[self.semis_day_two],
    )

    # Ensure the results email went out
    self.assertEqual(1, len(mail.outbox))

    # Read in expected message
    with open(os.path.join(TESTDATA_BASEDIR, 'emailers', 'dewsweeper',
                           'expected_day_13.dat')) as f:
      expected_email_message = f.read()

    assertEqualHTML(expected_email_message.strip(),
                    mail.outbox[0].body)

    # Ensure that the finals are scheduled
    day_one_bouts = Bout.objects.filter(
      basho_id=self.basho_id,
      day=self.semis_day_two + 1,
    )
    self.assertEqual(6, len(day_one_bouts))
    day_two_bouts = Bout.objects.filter(
      basho_id=self.basho_id,
      day=self.semis_day_two + 2,
    )
    self.assertEqual(6, len(day_two_bouts))

  def test_dewsweeper_processor_no_mail(self):
    dewsweeper_processor = DewsweeperProcessor(
      self.basho_id, self.day, False)
    dewsweeper_processor._process_ozumo_results(
      self.day_results[self.day], self.day_results_by_rikishi[self.day]
    )

    # Ensure the results email DID NOT go out
    self.assertEqual(0, len(mail.outbox))

  def test_calculate_new_rank(self):
    dewsweeper_processor = DewsweeperProcessor(
      1, 1, False)

    # M5 gets 1st place, goes up 4 ranks to M1
    self.assertEqual(1, dewsweeper_processor._calculate_new_rank(5, -4))

    # M1 gets 12th place, goes down 4 ranks to M5
    self.assertEqual(5, dewsweeper_processor._calculate_new_rank(1, 4))

    # M1 gets 1st place, only goes up to sekiwake
    self.assertEqual(-1, dewsweeper_processor._calculate_new_rank(1, -4))

    # M7 gets 12th place, only goes down to M10, the rank floor
    self.assertEqual(10, dewsweeper_processor._calculate_new_rank(7, 4))

  def test_update_basho_squadrons_with_final_basho_results(self):
    basho_id = 670
    # Initialize oyakata for all 12 players
    oyakata_list = [init_oyakata_with_shikona(shikona='shikona%s' % n,
                                              basho_id=basho_id)
                    for n in xrange(12)]

    # Create Bout objects for Day 14 and 15, which oyakata2 will win
    bouts = []
    for day in [14, 15]:
      for cur_bout in range(6):
        oyakata1 = oyakata_list[cur_bout * 2]
        oyakata2 = oyakata_list[(cur_bout * 2) + 1]
        bouts.append(Bout.objects.create(
          oyakata1=oyakata1,
          oyakata1_score=2,
          oyakata1_chanko=0,
          oyakata1_bp=2,
          oyakata2=oyakata2,
          oyakata2_score=4,
          oyakata2_chanko=0,
          oyakata2_bp=4,
          basho_id=basho_id,
          day=day,
          order=cur_bout + 1,
        ))

    dewsweeper_processor = DewsweeperProcessor(basho_id, 15, False)
    dewsweeper_processor._update_basho_squadrons_with_final_basho_results()

    basho_squadron_list = [
      oyakata.basho_squadron_set.get(basho_id=basho_id)
      for oyakata in oyakata_list
    ]

    expected_places = [2, 1, 4, 3, 6, 5, 8, 7, 10, 9, 12, 11]
    actual_places = [bs.place for bs in basho_squadron_list]
    self.assertEqual(expected_places, actual_places)

  def test_update_oyakata_rank_and_flags_yokozuna_stays_yokozuna(self):
    oyakata = create_oyakata()
    oyakata.rank = -3

    dewsweeper_processor = DewsweeperProcessor(580, 15, False)
    dewsweeper_processor._update_oyakata_rank_and_flags(oyakata, 12)

    # Even from last place, the yokozuna should remain yokozuna
    self.assertEqual(-3, oyakata.rank)

  def test_update_oyakata_rank_and_flags_promote_to_yokozuna(self):
    oyakata = create_oyakata()
    oyakata.rank = -2
    oyakata.is_tsunatori = True

    dewsweeper_processor = DewsweeperProcessor(580, 15, False)
    dewsweeper_processor._update_oyakata_rank_and_flags(oyakata, 1)

    # Ensure the new yokozuna has the right rank and all flags cleared
    self.assertEqual(-3, oyakata.rank)
    self.assertFalse(oyakata.is_tsunatori)
    self.assertFalse(oyakata.is_kadoban)
    self.assertFalse(oyakata.is_repromotion_eligible)

  def test_update_oyakata_rank_and_flags_promote_to_yokozuna_jun_jun_yusho(
      self):
    # With jun-yusho, promotion should happen if:
    #   * The previous two basho were also at ozeki and were yusho, jun
    #     (in either order).
    #   * The previous basho was a kachi, and the basho before was a yusho,
    #     all at ozeki
    oyakata = create_oyakata()
    prev_bs = create_basho_squadron(oyakata, 'foo', 579,
                                    -2, place=2)
    prev_prev_bs = create_basho_squadron(oyakata, 'foo', 578,
                                         -2, place=2)
    dewsweeper_processor = DewsweeperProcessor(580, 15, False)
    for place in [1, 2]:
      oyakata.rank = -2

      dewsweeper_processor._update_oyakata_rank_and_flags(oyakata, place)

      # Ensure the new yokozuna has the right rank and all flags cleared
      # if current place is 1, then promote!
      # if current place is 2, then no promote (yusho, kachi, jun).
      self.assertEqual(-3 if place == 1 else -2, oyakata.rank)
      self.assertFalse(oyakata.is_tsunatori)
      self.assertFalse(oyakata.is_kadoban)
      self.assertFalse(oyakata.is_repromotion_eligible)

  def test_update_oyakata_rank_and_flags_promote_to_yokozuna_yusho_kachi_yusho(
      self):
    oyakata = create_oyakata()
    prev_bs = create_basho_squadron(oyakata, 'foo', 579,
                                    -2, place=4)
    prev_prev_bs = create_basho_squadron(oyakata, 'foo', 578,
                                         -2, place=1)
    dewsweeper_processor = DewsweeperProcessor(580, 15, False)
    for place in [1, 2]:
      oyakata.rank = -2

      dewsweeper_processor._update_oyakata_rank_and_flags(oyakata, place)

      # Ensure the new yokozuna has the right rank and all flags cleared
      # if current place is 1, then promote!
      # if current place is 2, then no promote (yusho, kachi, jun).
      self.assertEqual(-3 if place == 1 else -2, oyakata.rank)
      self.assertFalse(oyakata.is_tsunatori)
      self.assertFalse(oyakata.is_kadoban)
      self.assertFalse(oyakata.is_repromotion_eligible)

  def test_update_oyakata_rank_and_flags_give_ozeki_tsunatori(self):
    oyakata = create_oyakata()
    oyakata.rank = -2

    dewsweeper_processor = DewsweeperProcessor(580, 15, False)
    dewsweeper_processor._update_oyakata_rank_and_flags(oyakata, 1)

    # Ensure the ozeki is still ozeki and has tsunatori
    self.assertEqual(-2, oyakata.rank)
    self.assertTrue(oyakata.is_tsunatori)
    self.assertFalse(oyakata.is_kadoban)
    self.assertFalse(oyakata.is_repromotion_eligible)

  def test_update_oyakata_rank_and_flags_give_ozeki_jun_tsunatori(self):
    oyakata = create_oyakata()
    oyakata.rank = -2
    basho_id = 623

    for prev_rank in [-1, -2]:
      prev_bs = create_basho_squadron(oyakata, 'foo', basho_id - 1,
                                      prev_rank)

      dewsweeper_processor = DewsweeperProcessor(basho_id, 15, False)

      for prev_place in [1, 2]:
        for current_place in [1, 2]:
          # Reset oyakata object
          oyakata.rank = -2
          oyakata.is_tsunatori = False

          # Edit the BashoSquadron for the oyakata in the
          # previous basho in which he got 1st or 2nd place
          prev_bs.place = prev_place
          prev_bs.save()

          dewsweeper_processor._update_oyakata_rank_and_flags(
            oyakata, current_place)

          # Ensure the ozeki is still ozeki
          # May also have tsunatori if oyakata recently got a yusho
          self.assertEqual(-2, oyakata.rank)
          self.assertFalse(oyakata.is_kadoban)
          self.assertFalse(oyakata.is_repromotion_eligible)
          if current_place == 1:
            self.assertTrue(oyakata.is_tsunatori)
          else:
            self.assertFalse(oyakata.is_tsunatori)

      prev_bs.delete()

  def test_update_oyakata_rank_and_flags_give_ozeki_kadoban(self):
    # An ozeki should get kadoban status if getting 8th place or worse
    dewsweeper_processor = DewsweeperProcessor(580, 15, False)

    for place in [8, 9, 10, 11, 12]:
      oyakata = create_oyakata()
      oyakata.rank = -2

      dewsweeper_processor._update_oyakata_rank_and_flags(oyakata, place)

      # Ensure the ozeki is still ozeki and has kadoban
      self.assertEqual(-2, oyakata.rank)
      self.assertFalse(oyakata.is_tsunatori)
      self.assertTrue(oyakata.is_kadoban)
      self.assertFalse(oyakata.is_repromotion_eligible)

  def test_update_oyakata_rank_and_flags_ozeki_escapes_kadoban(self):
    # An ozeki should escape kadoban status with 5th or better
    dewsweeper_processor = DewsweeperProcessor(580, 15, False)

    for place in [1, 2, 3, 4, 5]:
      oyakata = create_oyakata()
      oyakata.rank = -2
      oyakata.is_kadoban = True

      dewsweeper_processor._update_oyakata_rank_and_flags(oyakata, place)

      # Ensure the ozeki is still ozeki and is no longer kadoban
      # Ozeki may also be tsunatori if he got a yusho
      self.assertEqual(-2, oyakata.rank)
      if place == 1:
        self.assertTrue(oyakata.is_tsunatori)
      else:
        self.assertFalse(oyakata.is_tsunatori)
      self.assertFalse(oyakata.is_kadoban)
      self.assertFalse(oyakata.is_repromotion_eligible)

  def test_update_oyakata_rank_and_flags_ozeki_stays_kadoban(self):
    # An ozeki should keep kadoban status without demotion with 6th or 7th
    dewsweeper_processor = DewsweeperProcessor(580, 15, False)

    for place in [6, 7]:
      oyakata = create_oyakata()
      oyakata.rank = -2
      oyakata.is_kadoban = True

      dewsweeper_processor._update_oyakata_rank_and_flags(oyakata, place)

      # Ensure the ozeki is still ozeki and still has kadoban
      self.assertEqual(-2, oyakata.rank)
      self.assertFalse(oyakata.is_tsunatori)
      self.assertTrue(oyakata.is_kadoban)
      self.assertFalse(oyakata.is_repromotion_eligible)

  def test_update_oyakata_rank_and_flags_demote_from_ozeki(self):
    # An ozeki should be demoted if on kadoban and with an 8th or worse
    dewsweeper_processor = DewsweeperProcessor(580, 15, False)

    for place in [8, 9, 10, 11, 12]:
      oyakata = create_oyakata()
      oyakata.rank = -2
      oyakata.is_kadoban = True

      dewsweeper_processor._update_oyakata_rank_and_flags(oyakata, place)

      # Ensure the ozeki has been demoted and is repromotion eligible
      self.assertEqual(-1, oyakata.rank)
      self.assertFalse(oyakata.is_tsunatori)
      self.assertFalse(oyakata.is_kadoban)
      self.assertTrue(oyakata.is_repromotion_eligible)

  def test_update_oyakata_rank_and_flags_promote_to_ozeki(self):
    # Sekiwake should promote to ozeki with a 2nd or 1st place finish
    dewsweeper_processor = DewsweeperProcessor(580, 15, False)

    for place in [1, 2]:
      oyakata = create_oyakata()
      oyakata.rank = -1

      dewsweeper_processor._update_oyakata_rank_and_flags(oyakata, place)

      # Ensure the new ozeki has the right rank and all flags cleared
      self.assertEqual(-2, oyakata.rank)
      self.assertFalse(oyakata.is_tsunatori)
      self.assertFalse(oyakata.is_kadoban)
      self.assertFalse(oyakata.is_repromotion_eligible)

  def test_update_oyakata_rank_and_flags_repromote_to_ozeki(self):
    # Sekiwake should repromote to ozeki with a 1st-3rd place finish
    dewsweeper_processor = DewsweeperProcessor(580, 15, False)

    for place in [1, 2, 3]:
      oyakata = create_oyakata()
      oyakata.rank = -1
      oyakata.is_repromotion_eligible = True

      dewsweeper_processor._update_oyakata_rank_and_flags(oyakata, place)

      # Ensure the new ozeki has the right rank and all flags cleared
      self.assertEqual(-2, oyakata.rank)
      self.assertFalse(oyakata.is_tsunatori)
      self.assertFalse(oyakata.is_kadoban)
      self.assertFalse(oyakata.is_repromotion_eligible)

  def test_update_banzuke(self):
    basho_id = 671
    # Initialize oyakata for all 12 players
    oyakata_list = [init_oyakata_with_shikona(shikona='shikona%s' % n,
                                              basho_id=basho_id)
                    for n in xrange(12)]
    basho_squadron_list = [
      oyakata.basho_squadron_set.get(basho_id=basho_id)
      for oyakata in oyakata_list
    ]

    # Set all ranks from M10 to sekiwake, places from 1 to 12
    for n, bs in enumerate(basho_squadron_list):
      bs.rank = 10 - n
      bs.oyakata.rank = bs.rank
      bs.place = n + 1
      bs.save()
      bs.oyakata.save()

    dewsweeper_processor = DewsweeperProcessor(basho_id, 15, False)
    dewsweeper_processor._update_banzuke()

    # Make sure all flags are set to False, as none of them apply here
    for oyakata in oyakata_list:
      oyakata.refresh_from_db()
      self.assertFalse(oyakata.is_tsunatori)
      self.assertFalse(oyakata.is_kadoban)
      self.assertFalse(oyakata.is_repromotion_eligible)

    # orig. ranks    10  9  8  7  6  5  4  3  2  1  0 -1
    # rank change    -4 -3 -3 -2 -1  0  0  1  2  3  3  4
    expected_ranks = [6, 6, 5, 5, 5, 5, 4, 4, 4, 4, 3, 3]
    actual_ranks = [o.rank for o in oyakata_list]
    self.assertEqual(expected_ranks, actual_ranks)

  def test_update_banzuke_raises_with_null_place(self):
    dewsweeper_processor = DewsweeperProcessor(self.basho_id, 15, False)
    with self.assertRaises(BoiScoringException):
      dewsweeper_processor._update_banzuke()


class LineupEmailerTests(TestCase):

  @classmethod
  def setUpTestData(cls):
    cls.basho_id = get_current_basho_id()
    cls.day = 1
    cls.submission_day = cls.day + 1

    # Initialize oyakata for all 12 players
    cls.oyakata_list = [init_oyakata_with_shikona(shikona='shikona%s' % n,
                                                  basho_id=cls.basho_id)
                        for n in xrange(12)]
    cls.basho_squadron_list = [
      oyakata.basho_squadron_set.get(basho_id=cls.basho_id)
      for oyakata in cls.oyakata_list
    ]

    # Create some rikishi to work with
    cls.rikishi_master_list = [
      create_rikishi(real_name='Rikishi%s' % str(n + 1),
                     base_basho_id=cls.basho_id) for n in xrange(12 * 6)
    ]

    # Assign the rikishi to the squadrons and create SuicidePoolEntries
    for n, oyakata in enumerate(cls.oyakata_list):
      assign_rikishi_to_squadron(
        oyakata, cls.basho_id, cls.rikishi_master_list[n * 6:(n * 6) + 6])
      SuicidePoolEntry.objects.create(
        oyakata=oyakata,
        rikishi=cls.rikishi_master_list[n],
        basho_id=cls.basho_id,
        day=cls.day,
      )

    # Create ozumo bouts
    for n in xrange((12 * 6) / 2):
      if n % 2 == 0:
        r1 = cls.rikishi_master_list[n]
        r2 = cls.rikishi_master_list[n + ((12 * 6) / 2)]
      else:
        r1 = cls.rikishi_master_list[n + ((12 * 6) / 2)]
        r2 = cls.rikishi_master_list[n]
      create_ozumo_bout(
        r1, r2, 0, 0, basho_id=cls.basho_id, day=cls.day
      )

    cls.oyakata = cls.oyakata_list[0]
    cls.basho_squadron = cls.basho_squadron_list[0]
    cls.squadron_members = cls.basho_squadron.squadron_members

    # Create new LineupEntries
    cls.lineup_entries = []
    for i in xrange(4):
      cls.lineup_entries.append(LineupEntry.objects.create(
        oyakata=cls.oyakata,
        rikishi=cls.squadron_members[i].rikishi,
        basho_id=cls.basho_id,
        day=cls.submission_day,
        order=i + 1,
      ))

  def test_lineup_emailer(self):
    suicide_pick = OzumoBanzukeEntry.objects.get(
      basho_id=self.basho_id,
      rikishi=self.rikishi_master_list[0],
    )

    pre_trash_talk = 'pre_trash_talk'
    post_trash_talk = 'post_trash_talk'

    lineup_emailer = LineupEmailer(
      self.lineup_entries, suicide_pick, pre_trash_talk, post_trash_talk,
      self.basho_squadron, self.submission_day)

    email_message = lineup_emailer._build_email_message()

    # Read in expected message
    with open(os.path.join(TESTDATA_BASEDIR, 'emailers', 'lineup',
                           'expected.dat')) as f:
      expected_email_message = f.read()

    assertEqualHTML(expected_email_message.strip(),
                    email_message.strip())

  def test_lineup_emailer_no_suicide(self):
    suicide_pick = None

    pre_trash_talk = 'pre_trash_talk'
    post_trash_talk = 'post_trash_talk'

    lineup_emailer = LineupEmailer(
      self.lineup_entries, suicide_pick, pre_trash_talk, post_trash_talk,
      self.basho_squadron, self.submission_day)

    email_message = lineup_emailer._build_email_message()

    # Read in expected message
    with open(os.path.join(TESTDATA_BASEDIR, 'emailers', 'lineup',
                           'expected_no_suicide.dat')) as f:
      expected_email_message = f.read()

    assertEqualHTML(expected_email_message.strip(),
                    email_message.strip())


class MonkeyLineupSubmitterTests(TestCase):

  @classmethod
  def setUpTestData(cls):
    cls.basho_id = 682
    cls.day = 1
    cls.monkey_oyakata = init_oyakata_with_shikona(
      shikona=MONKEY_SHIKONA, basho_id=cls.basho_id)
    cls.monkey_basho_squadron = cls.monkey_oyakata.basho_squadron_set.get(
      basho_id=cls.basho_id)

    # Create some rikishi to work with
    cls.rikishi_master_list = [
      create_rikishi(real_name='Rikishi%s' % str(n + 1),
                     base_basho_id=cls.basho_id) for n in xrange(6)
    ]

    # Assign all rikishi to the Monkey
    assign_rikishi_to_squadron(
      cls.monkey_oyakata, cls.basho_id, cls.rikishi_master_list)

  @mock.patch('boi.monkey.TrashTalkGenerator.generate_trash_talk',
              return_value='[monkey trashtalk]')
  def test_monkey_lineup_submitter(self, generate_trash_talk_patched_func):
    monkey_lineup_submitter = MonkeyLineupSubmitter(
      self.basho_id, self.day, False, True
    )
    new_lineup_entries = monkey_lineup_submitter.submit_lineup()
    self.assertEqual(4, len(new_lineup_entries))

    # Assert no duplicates
    seen_rikishi = set()
    for entry in new_lineup_entries:
      self.assertNotIn(entry.rikishi, seen_rikishi)
      seen_rikishi.add(entry.rikishi)

    # Check mail was sent
    self.assertEqual(1, len(mail.outbox))

    # Read in expected message template
    with open(os.path.join(TESTDATA_BASEDIR, 'emailers', 'lineup',
                           'expected_monkey.dat')) as f:
      expected_email_message_template = f.read()

    # Replace template with actual rikishi selections
    expected_email_message = expected_email_message_template.replace(
      '{r1}',
      new_lineup_entries[0].rikishi.shikona_for_basho(self.basho_id)
    ).replace(
      '{r2}',
      new_lineup_entries[1].rikishi.shikona_for_basho(self.basho_id)
    ).replace(
      '{r3}',
      new_lineup_entries[2].rikishi.shikona_for_basho(self.basho_id)
    ).replace(
      '{r4}',
      new_lineup_entries[3].rikishi.shikona_for_basho(self.basho_id)
    )

    self.assertEqual(expected_email_message, mail.outbox[0].body)

    # Check that new LineupEntries are created
    new_lineup_entries = LineupEntry.objects.filter(
      oyakata=self.monkey_oyakata,
      basho_id=self.basho_id,
      day=self.day,
    )
    self.assertEqual(4, len(new_lineup_entries))

    # Assert that LineupEntries are in ascending selection order
    last_selection_order = -1
    for new_lineup_entry in new_lineup_entries:
      cur_squadron_member = SquadronMember.objects.get(
        oyakata=self.monkey_oyakata,
        basho_id=self.basho_id,
        rikishi=new_lineup_entry.rikishi,
      )
      self.assertGreater(cur_squadron_member.selection_order,
                         last_selection_order)
      last_selection_order = cur_squadron_member.selection_order


class MonkeyListGeneratorTests(SimpleTestCase):

  def test_monkey_list_generator(self):
    with open(os.path.join(TESTDATA_BASEDIR, 'monkeylist', 'input.dat')
              ) as f:
      draft_input = f.read().strip()

    monkey_list_generator = MonkeyListGenerator()
    results = monkey_list_generator.generate_list(draft_input)

    expected = [
      ('Hakuho', 48), ('Kotoshogiku', 43), ('Kakuryu', 43),
      ('Kisenosato', 42), ('Terunofuji', 37), ('Takakeisho', 37),
      ('Okinoumi', 37), ('Shohozan', 33), ('Tochiozan', 30),
      ('Onosho', 21), ('Takayasu', 20), ('Aminishiki', 18), ('Abi', 18),
      ('Mitakeumi', 17), ('Yoshikaze', 15), ('Hokutofuji', 15),
      ('Endo', 15), ('Sokokurai', 13), ('Daieisho', 12), ('Shodai', 11),
      ('Ichinojo', 11), ('Tamawashi', 10), ('Nishikigi', 10),
      ('Chiyomaru', 9), ('Goeido', 8), ('Takekaze', 7), ('Daishomaru', 6),
      ('Kagayaki', 4), ('Asanoyama', 4), ('Ryuden', 3), ('Daiamami', 2),
      ('Ikioi', 1),
    ]

    self.assertEqual(results, expected)


class WaiversTests(TestCase):

  @classmethod
  def setUpTestData(cls):
    cls.basho_id = 940
    cls.short_name_to_basho_squadron = {}

    with codecs.open(os.path.join(TESTDATA_BASEDIR, 'draft',
                                  'oyakata.dat'), encoding='utf-8') as f:
      oyakata_lines = [o.strip() for o in f.readlines()
                       if o and not o.isspace()]

    for oyakata_line in oyakata_lines:
      short_name, shikona = oyakata_line.split(',')
      oyakata_obj = Oyakata.objects.create(
        first_name=short_name,
        last_name=short_name,
        short_name=short_name,
        rank=0,
        style='',
        quote='',
        image_path='.',
      )
      cls.short_name_to_basho_squadron[short_name] = (
        BashoSquadron.objects.create(
          oyakata=oyakata_obj,
          basho_id=cls.basho_id,
          shikona=shikona,
          rank=0,
        )
      )

    # Create Rikishi objects
    with open(os.path.join(TESTDATA_BASEDIR, 'draft', 'rikishi.dat')) as f:
      rikishi_lines = [r.strip() for r in f.readlines()
                       if r and not r.isspace()]

    for rikishi_line in rikishi_lines:
      rikishi_obj = Rikishi.objects.create(
        real_name=rikishi_line,
        dob=random_dob(),
        web_id=rikishi_line,
      )
      OzumoBanzukeEntry.objects.create(
        rikishi=rikishi_obj,
        shikona=rikishi_line,
        basho_id=cls.basho_id,
        rank=0,
      )

    # Add Hakuho to Monkey's squadron
    assign_rikishi_to_squadron(
      get_oyakata_obj('Makino the Magic Monkey', cls.basho_id),
      cls.basho_id,
      [get_rikishi_obj('Hakuho', cls.basho_id)],
    )

    # Add Hakuho to Cindi's squadron
    assign_rikishi_to_squadron(
      get_oyakata_obj('Peerless', cls.basho_id),
      cls.basho_id,
      [get_rikishi_obj('Hakuho', cls.basho_id)],
    )

  def test_waivers_processor(self):
    # Read in input
    with open(os.path.join(TESTDATA_BASEDIR, 'waivers', 'input.dat')) as f:
      waivers_input = f.read()

    waivers_processor = WaiversProcessor(self.basho_id, False)
    processed_waivers = waivers_processor.process_waivers(waivers_input)

    # Check processed_waivers output
    # 2 total moves, plus another for the NoneType divisional separator
    self.assertEqual(3, len(processed_waivers))
    self.assertIsNone(processed_waivers[1])

    # Check that Hakuho is inactive on Monkey's squadron
    monkey_hakuho_squadron_member = get_squadron_member_obj(
      'Makino the Magic Monkey', 'Hakuho', self.basho_id)
    self.assertFalse(monkey_hakuho_squadron_member.is_active)

  def test_waivers_results_emailer(self):
    waivers_results = []

    # Add Daieisho to Monkey's squadron
    assign_rikishi_to_squadron(
      get_oyakata_obj('Makino the Magic Monkey', self.basho_id),
      self.basho_id,
      [get_rikishi_obj('Daieisho', self.basho_id)],
    )

    # Add Tochinoshin to Cindi's squadron
    assign_rikishi_to_squadron(
      get_oyakata_obj('Peerless', self.basho_id),
      self.basho_id,
      [get_rikishi_obj('Tochinoshin', self.basho_id)],
    )

    waivers_results.append(WaiversResult(
      self.short_name_to_basho_squadron['The Monkey'],
      get_squadron_member_obj('Makino the Magic Monkey',
                              'Hakuho',
                              self.basho_id),
      get_squadron_member_obj('Makino the Magic Monkey',
                              'Daieisho',
                              self.basho_id),
    ))

    # End of division
    waivers_results.append(None)

    waivers_results.append(WaiversResult(
      self.short_name_to_basho_squadron['Textor'],
      get_squadron_member_obj('Peerless',
                              'Hakuho',
                              self.basho_id),
      get_squadron_member_obj('Peerless',
                              'Tochinoshin',
                              self.basho_id),
    ))

    waivers_results_emailer = WaiversResultsEmailer(
      waivers_results, self.basho_id)
    waivers_results_emailer.send_email()

    self.assertEqual(1, len(mail.outbox))

    # Read in expected message
    with open(os.path.join(TESTDATA_BASEDIR, 'emailers', 'waivers',
                           'expected.dat')) as f:
      expected_email_message = f.read()

    self.assertEqual(expected_email_message, mail.outbox[0].body)


class DraftRegistrarTests(TestCase):

  @classmethod
  def setUpTestData(cls):
    cls.basho_id = 950

    with codecs.open(os.path.join(TESTDATA_BASEDIR, 'draft',
                                  'oyakata.dat'), encoding='utf-8') as f:
      oyakata_lines = [o.strip() for o in f.readlines()
                       if o and not o.isspace()]

    for oyakata_line in oyakata_lines:
      short_name, shikona = oyakata_line.split(',')
      oyakata_obj = Oyakata.objects.create(
        first_name=short_name,
        last_name=short_name,
        short_name=short_name,
        rank=0,
        style='',
        quote='',
        image_path='.',
      )
      BashoSquadron.objects.create(
        oyakata=oyakata_obj,
        basho_id=cls.basho_id,
        shikona=shikona,
        rank=0,
      )

    # Create Rikishi objects
    with open(os.path.join(TESTDATA_BASEDIR, 'draft', 'rikishi.dat')) as f:
      rikishi_lines = [r.strip() for r in f.readlines()
                       if r and not r.isspace()]

    for rikishi_line in rikishi_lines:
      rikishi_obj = Rikishi.objects.create(
        real_name=rikishi_line,
        dob=random_dob(),
        web_id=rikishi_line,
      )
      OzumoBanzukeEntry.objects.create(
        rikishi=rikishi_obj,
        shikona=rikishi_line,
        basho_id=cls.basho_id,
        rank=0,
      )

  def test_draft_registrar(self):
    with open(os.path.join(TESTDATA_BASEDIR, 'draft', 'input.dat')) as f:
      draft_input = f.read().strip()

    draft_registrar = DraftRegistrar(self.basho_id)
    draft_registrar.register_draft(draft_input)

    # Ensure the right number of SquadromMembers were created
    registered_squadron_members = SquadronMember.objects.filter(
      basho_id=self.basho_id,
    )
    self.assertEqual(72, registered_squadron_members.count())

    # Spot-check some SquadronMembers
    oyakata = Oyakata.objects.get(
      short_name='Brad',
    )
    squadron_member = SquadronMember.objects.get(
      oyakata=oyakata,
      basho_id=self.basho_id,
      selection_order=1,
    )
    self.assertEqual(
      'Kotoshogiku',
      squadron_member.rikishi.shikona_for_basho(self.basho_id))


class ScheduleRegistrarTests(TestCase):

  def test_schedule_registrar(self):
    basho_id = 909
    shikona_to_oyakata_dict = {}

    # Read in all oyakata and make Oyakata and BashoSquadron objects
    with codecs.open(os.path.join(TESTDATA_BASEDIR, 'scheduling',
                                  'oyakata.dat'), encoding='utf-8') as f:
      oyakata_lines = f.readlines()
    for n, oyakata_line in enumerate(oyakata_lines):
      oyakata_line = oyakata_line.strip()
      if not oyakata_line:
        continue

      shikona_to_oyakata_dict[oyakata_line] = Oyakata.objects.create(
        first_name='FirstName%s' % n,
        last_name='LastName%s' % n,
        short_name='ShortName%s' % n,
        rank=0,
        style='Style%s' % n,
        quote='Quote',
        image_path='.',
      )
      basho_squadron_obj = BashoSquadron.objects.create(
        oyakata=shikona_to_oyakata_dict[oyakata_line],
        basho_id=basho_id,
        shikona=oyakata_line,
        rank=0,
      )

    # Read in input
    with open(os.path.join(TESTDATA_BASEDIR, 'scheduling',
                           'input.dat')) as f:
      schedule_input = f.read()

    schedule_registrar = ScheduleRegistrar(basho_id)
    schedule_registrar.register_schedule(schedule_input)

    # Check to see the right number of bouts were created
    for day in range(1, 12):
      bouts = Bout.objects.filter(
        basho_id=basho_id,
        day=day,
      )
      self.assertEqual(6, bouts.count())

    # Spot check some bouts
    bout = Bout.objects.filter(
      basho_id=basho_id,
      day=1,
      oyakata1=shikona_to_oyakata_dict['Uruku Sake'],
      oyakata2=shikona_to_oyakata_dict['Free Ama'],
    ).first()
    self.assertIsNotNone(bout)
    self.assertEqual(1, bout.order)

    bout = Bout.objects.filter(
      basho_id=basho_id,
      day=11,
      oyakata1=shikona_to_oyakata_dict["Freakin' Plant"],
      oyakata2=shikona_to_oyakata_dict['Free Ama'],
    ).first()
    self.assertIsNotNone(bout)
    self.assertEqual(6, bout.order)
