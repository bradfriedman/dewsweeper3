from abc import abstractmethod, ABCMeta

import os

from django.core.mail import EmailMessage

from .boi_config import DAY_ONES, DAY_TWOS
from .boi_config import EMAIL_FROM_ADDRESS, EMAIL_TO_ADDRESS

from .utils import get_basho_full_name


class BoiEmailer(object):
  __metaclass__ = ABCMeta

  def __init__(self, **kwargs):
    self.subject = None
    self.message = None
    self.from_email = kwargs.get('from_email', EMAIL_FROM_ADDRESS)
    self.recipient_list = kwargs.get('recipient_list', [EMAIL_TO_ADDRESS])
    self.html_message = None
    self.html_type = False

  @abstractmethod
  def _build_email_subject(self):
    pass

  @abstractmethod
  def _build_email_message(self):
    pass

  def set_html_type(self, is_html=True):
    self.html_type = is_html

  def send_email(self):
    self.subject = self._build_email_subject()
    self.message = self._build_email_message()
    return self._send()

  def _get_email_message(self):
    if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
      from google.appengine.api import mail
      msg = mail.EmailMessage(
        subject=self.subject, sender=self.from_email,
        to=self.recipient_list)
      if self.html_type:
        msg.html = self.message
      else:
        msg.body = self.message
      msg.check_initialized()
    else:
      msg = EmailMessage(self.subject, self.message, self.from_email,
                         self.recipient_list)
      if self.html_type:
        msg.content_subtype = 'html'

    return msg

  def _send(self):
    msg = self._get_email_message()
    return msg.send()


class DewsweeperEmailer(BoiEmailer):

  def __init__(self, bouts, results_by_rikishi, basho_squadrons, basho_id,
               day, **kwargs):
    self.bouts = bouts
    self.results_by_rikishi = results_by_rikishi
    self.basho_squadrons = basho_squadrons
    self.basho_id = basho_id
    self.day = day
    self.is_day_one = self.day in DAY_ONES
    self.is_day_two = self.day in DAY_TWOS

    super(DewsweeperEmailer, self).__init__(**kwargs)

    self.set_html_type()

  def _get_win_draw_verb(self, s1, s2):
    if s1 == s2:
      return 'def. (TIEBREAKER)' if self.is_day_two else 'HIKIWAKE'
    return 'def.'

  def _get_day_one_verb(self, s1, s2):
    return 'is beating' if s1 != s2 else 'is tied with'

  def _get_verb(self, s1, s2):
    return (self._get_day_one_verb(s1, s2) if self.is_day_one else
            self._get_win_draw_verb(s1, s2))

  def _get_scored_points_vs_potential(self, result):
    shikona = result['oyakata'].shikona_for_basho(self.basho_id)
    if result['bp'] <= 0:
      return (u'{shikona} could not possibly have '
              'scored any points :(').format(shikona=shikona)
    if result['bp'] > result['score']:
      return u'{shikona} could have scored {bp} points.'.format(
        shikona=shikona, bp=result['bp']
      )
    else:
      return u'{shikona} scored all {bp} potential points!'.format(
        shikona=shikona, bp=result['bp']
      )

  def _get_rikishi_score(self, shikona):
    if shikona in self.results_by_rikishi:
      return self.results_by_rikishi[shikona][0]
    else:
      return 0

  def _build_result_details(self, lineup1, lineup2):
    result_details = ''
    for n, lineup_entry1 in enumerate(lineup1):
      lineup_entry2 = lineup2[n]
      r1_shikona = lineup_entry1.rikishi.shikona_for_basho(self.basho_id)
      r2_shikona = lineup_entry2.rikishi.shikona_for_basho(self.basho_id)
      result_details += u'''
                <tr>
                    <td>{r1_shikona} - {r1_score}</td>
                    <td></td>
                    <td>{r2_shikona} - {r2_score}</td>
                </tr>
            '''.format(
        r1_shikona=r1_shikona,
        r1_score=self._get_rikishi_score(r1_shikona),
        r2_shikona=r2_shikona,
        r2_score=self._get_rikishi_score(r2_shikona),
      )

    return result_details

  def _build_day_one_score_row(self, bout):
    return '''
            <tr>
                <td>DAY ONE SCORE: {winning_day_one_score}</td>
                <td></td>
                <td>DAY ONE SCORE: {losing_day_one_score}</td>
            </tr>
        '''.format(
      winning_day_one_score=bout.winning_oyakata_day_one_score,
      losing_day_one_score=bout.losing_oyakata_day_one_score,
    )

  def _build_results_table(self):
    table = '<table width="600">\n\n'
    for b in self.bouts:
      wr = b.winning_result
      lr = b.losing_result
      winning_total_score = wr['score'] + wr['day_one_score']
      losing_total_score = lr['score'] + lr['day_one_score']
      table += u'''
                <tr>
                  <td style="padding-top: 30px; padding-bottom:10px"><b>{w_shikona} {w_score} ({w_chanko})</b></td>
                  <td style="padding-top: 30px; padding-bottom:10px"><b>{verb}</b></td>
                  <td style="padding-top: 30px; padding-bottom:10px"><b>{l_shikona} {l_score} ({l_chanko})</b></td>
                </tr>
            '''.format(
        w_shikona=wr['oyakata'].shikona_for_basho(self.basho_id),
        w_score=winning_total_score,
        w_chanko=wr['chanko'],
        verb=self._get_verb(winning_total_score, losing_total_score),
        l_shikona=lr['oyakata'].shikona_for_basho(self.basho_id),
        l_score=losing_total_score,
        l_chanko=lr['chanko'],
      )
      table += u'''
                <tr><td colspan="3">{w_scored_vs_potential}</td></tr>
                <tr><td colspan="3" style="padding-bottom: 10px">{l_scored_vs_potential}</td></tr>
            '''.format(
        w_scored_vs_potential=self._get_scored_points_vs_potential(wr),
        l_scored_vs_potential=self._get_scored_points_vs_potential(lr),
      )

      table += self._build_result_details(b.winning_lineup,
                                          b.losing_lineup)

      if self.is_day_two:
        table += self._build_day_one_score_row(b)

    table += '\n</table><br><br>\n\n'
    return table

  def _build_standings_table(self):
    table = '<table width="500">\n'
    table += '''
            <tr>
              <th>Oyakata</th><th>Record</th><th>Points</th><th>Chanko</th>
            </tr>
        '''

    for s in self.basho_squadrons:
      table += u'''
                <tr>
                  <td align="center">{shikona}</td>
                  <td align="center">{record}</td>
                  <td align="center">{points}</td>
                  <td align="center">{chanko}</td>
                </tr>
            '''.format(
        shikona=s.shikona,
        record=s.record_string,
        points=s.total_points,
        chanko=s.total_chanko,
      )

    table += '\n</table>\n\n'
    return table

  def _build_email_subject(self):
    basho_full_name = get_basho_full_name(self.basho_id)
    return '{basho_full_name} Day {day} Results'.format(
      basho_full_name=basho_full_name, day=self.day)

  def _build_email_message(self):
    html_message = ('<font size="+1">Dewsweeper reporting with Day {day} '
                    'results</font><br/>\n'.format(day=self.day))

    # Build table
    html_message += self._build_results_table()

    # Build standings table
    html_message += self._build_standings_table()

    return html_message


class LineupEmailer(BoiEmailer):

  def __init__(self, lineup_entries, suicide_pick, pre_trash_talk,
               post_trash_talk, basho_squadron, day, **kwargs):
    self.lineup_entries = lineup_entries
    self.suicide_pick = suicide_pick
    self.pre_trash_talk = pre_trash_talk
    self.post_trash_talk = post_trash_talk
    self.basho_squadron = basho_squadron
    self.day = day
    super(LineupEmailer, self).__init__(**kwargs)

  def _build_lineup_list(self):
    lineup_list = [
      le.rikishi.shikona_for_basho(self.basho_squadron.basho_id)
      for le in self.lineup_entries]
    return '\n'.join(lineup_list)

  def _build_email_subject(self):
    return "{name}'s rikishi selections for Day {day}".format(
      name=self.basho_squadron.oyakata.short_name,
      day=self.day,
    )

  def _build_email_message(self):
    line = u'----------------------------------------'
    message = u'{pre_trash_talk}\n\n{line}\n\n'.format(
      pre_trash_talk=self.pre_trash_talk,
      line=line,
    )

    message += self._build_lineup_list() + '\n\n'

    if self.suicide_pick:
      message += 'Suicide pick: {shikona}\n\n'.format(
        shikona=self.suicide_pick.shikona,
      )

    message += u'{line}\n\n{post_trash_talk}'.format(
      line=line,
      post_trash_talk=self.post_trash_talk,
    )

    return message


class WaiversResultsEmailer(BoiEmailer):

  def __init__(self, waivers_results, basho_id, **kwargs):
    self.waivers_results = waivers_results
    self.basho_id = basho_id
    super(WaiversResultsEmailer, self).__init__(**kwargs)

  def _build_email_subject(self):
    return 'BoI - {basho_full_name} waivers results'.format(
      basho_full_name=get_basho_full_name(self.basho_id)
    )

  def _build_email_message(self):
    message = ''
    for waiver_result in self.waivers_results:
      # A NoneType object in the waivers_results signifies the end
      # of a division, so create a newline and continue
      if not waiver_result:
        message += '\n'
        continue

      message += (u'{oyakata} drops {waived_rikishi}, '
                  u'adds {added_rikishi}\n').format(
        oyakata=waiver_result.get_oyakata_shikona(),
        waived_rikishi=waiver_result.get_waived_rikishi_shikona(),
        added_rikishi=waiver_result.get_added_rikishi_shikona(),
      )

    return message
