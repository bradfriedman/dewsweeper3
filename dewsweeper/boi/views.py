# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from collections import defaultdict
import logging

from django.db import transaction
from django.db.models import Q
from django.http import HttpResponse, HttpResponseForbidden, \
  HttpResponseRedirect, HttpResponseServerError
from django.shortcuts import get_object_or_404, redirect, render, reverse
from django.views import generic

from .boi_config import CRON_HEADER
from .boi_config import NUM_PLAYERS, TOTAL_DAYS
from .boi_config import KACHI_BRACKET_BOUND, KUWAHIRA_BRACKET_BOUND
from .boi_config import LAST_DAY_OF_REG_SEASON
from .boi_config import LINEUP_SIZE
from .boi_config import MONKEY_SHIKONA
from .boi_config import SQUADRON_SIZE
from .boi_config import SUICIDE_LAST_STRIKE, SUICIDE_NO_SELECTION_STR
from .boi_config import SUICIDE_WIN_BY_FUSEN_STR

from .draft import DraftRegistrar

from .emailers import LineupEmailer

from .exceptions import BoiException

from .forms import DraftRegistrationForm
from .forms import MonkeyListForm
from .forms import RegisterPlayersForm
from .forms import ScheduleRegistrationForm
from .forms import SquadronManagementForm
from .forms import WaiversForm

from .models import BashoSquadron
from .models import Bout
from .models import LineupEntry
from .models import Oyakata
from .models import OzumoBanzukeEntry
from .models import OzumoBout
from .models import Rikishi
from .models import SquadronMember
from .models import SuicidePoolEntry

from .monkey import MonkeyLineupSubmitter
from .monkey import MonkeyListGenerator

from .scheduling import ScheduleRegistrar

from .scoring import DewsweeperProcessor
from .scoring import SquadronSorter

from .utils import get_basho_full_name
from .utils import get_current_basho_id

from .view_utils import check_for_dewsweeper_readiness
from .view_utils import cmp_squadrons_suicide
from .view_utils import get_basho_id_from_request
from .view_utils import get_day_from_request
from .view_utils import get_current_day
from .view_utils import get_opponent
from .view_utils import get_ozumo_banzuke
from .view_utils import get_previous_basho_squadrons
from .view_utils import get_rikishi_profile
from .view_utils import is_basho_active
from .view_utils import is_suicide_pool_active

from .waivers import WaiversProcessor


class IndexView(generic.TemplateView):
  template_name = 'boi/index.html'

  def get_context_data(self, **kwargs):
    context = super(IndexView, self).get_context_data(**kwargs)
    context['basho_full_name'] = get_basho_full_name(get_current_basho_id())
    return context


class BanzukeView(generic.ListView):
  template_name = 'boi/banzuke.html'
  context_object_name = 'squadron_rows'

  def get_queryset(self):
    """Return all oyakata."""
    results = []
    basho_id = get_current_basho_id()
    while len(results) != NUM_PLAYERS / 2 and basho_id >= 450:
      squadrons = BashoSquadron.objects.filter(
        basho_id=basho_id
      ).order_by('rank')
      results = [(squadrons[i], squadrons[i + 1])
                 for i in xrange(0, len(squadrons), 2)]
      basho_id -= 1

    return results


class ProfileView(generic.DetailView):
  template_name = 'boi/profile.html'
  context_object_name = 'profile'
  model = Oyakata


class SquadronManagementIndexView(generic.ListView):
  template_name = 'boi/mgmt_index.html'
  context_object_name = 'squadrons'

  def get_queryset(self):
    """Return all squadrons for the current basho."""
    return BashoSquadron.objects.filter(
      basho_id=get_current_basho_id()
    ).order_by('shikona')


def squadron_management_view(request, oyakata_id):
  basho_id = get_current_basho_id()
  day = get_current_day()

  oyakata = get_object_or_404(Oyakata, id=oyakata_id)

  basho_squadron = BashoSquadron.objects.filter(
    basho_id=basho_id
  ).filter(
    oyakata=oyakata
  ).get()

  if day > TOTAL_DAYS:
    return render(request, 'boi/mgmt.html', {
      'basho_squadron': basho_squadron,
      'form': None,
      'has_lineup': True,
    })

  squadron_members = basho_squadron.squadron_members
  has_lineup = (len(squadron_members) == SQUADRON_SIZE)

  opponent = get_opponent(oyakata, basho_id, day)

  if opponent:
    # See if a lineup has already been submitted
    opponent_info = LineupEntry.objects.filter(
      basho_id=basho_id,
      day=day,
      oyakata=opponent,
    ).order_by('order')

    # If opponent has no lineup, use entire squadron roster
    if not opponent_info:
      opponent_info = SquadronMember.objects.filter(
        basho_id=basho_id,
        oyakata=opponent,
        is_active=True,
      ).order_by('selection_order')
  else:
    opponent_info = None

  prev_suicide_picks = SuicidePoolEntry.objects.filter(
    basho_id=basho_id,
    oyakata=oyakata,
    day__lt=day,
  ).values('rikishi_id')
  suicide_pool_active = is_suicide_pool_active(basho_id)

  form = SquadronManagementForm(request.POST or None,
                                rikishi=squadron_members,
                                prev_suicide_picks=prev_suicide_picks,
                                suicide_pool_active=suicide_pool_active,
                                day=get_current_day())

  if form.is_valid():
    with transaction.atomic():
      selected_day = form.cleaned_data.get('active_day')

      # First, delete any pre-existing lineup entries
      LineupEntry.objects.filter(
        oyakata=oyakata,
        basho_id=basho_id,
        day=selected_day,
      ).delete()

      selected_lineup = [Rikishi.objects.get(id=r)
                         for r
                         in form.cleaned_data.get('starting_rikishi', [])]

      new_lineup_entries = []
      for n, rikishi in enumerate(selected_lineup):
        new_lineup_entries.append(LineupEntry.objects.create(
          oyakata=oyakata,
          rikishi=rikishi,
          basho_id=basho_id,
          day=selected_day,
          order=n + 1
        ))

      suicide_pick = form.cleaned_data.get('suicide_pick')

      if suicide_pick:
        # First, map the OzumoBanzukeEntry to a Rikishi
        suicide_rikishi = suicide_pick.rikishi

        # Then, remove old suicide pick, if any
        SuicidePoolEntry.objects.filter(
          oyakata=oyakata,
          basho_id=basho_id,
          day=selected_day,
        ).delete()

        # Finally, create new SuicidePoolEntry
        SuicidePoolEntry.objects.create(
          oyakata=oyakata,
          rikishi=suicide_rikishi,
          basho_id=basho_id,
          day=selected_day,
        )

      lineup_emailer = LineupEmailer(
        new_lineup_entries,
        suicide_pick,
        form.cleaned_data.get('pre_trash_talk'),
        form.cleaned_data.get('post_trash_talk'),
        basho_squadron,
        selected_day,
      )
      lineup_emailer.send_email()

      return HttpResponseRedirect(
        reverse('boi:matchups') + '?b={0}&d={1}'.format(
          basho_id, selected_day))

  return render(request, 'boi/mgmt.html', {
    'basho_squadron': basho_squadron,
    'form': form,
    'has_lineup': has_lineup,
    'opponent': opponent,
    'opponent_info': opponent_info,
    'suicide_pool_active': suicide_pool_active,
  })


def monkey_automated_management_view(request):
  # First, make sure this is run by a cron job
  if request.META.get(CRON_HEADER) is None:
    logging.error(
      'Someone attempted to run the Monkey outside of a cron job')
    return HttpResponseForbidden(
      'Cannot run the Monkey outside of a cron job')

  # Make sure basho is active
  basho_id = get_basho_id_from_request(request)
  if not is_basho_active(basho_id):
    msg = 'Monkey exiting because {0} is inactive'.format(
      get_basho_full_name(basho_id))
    logging.info(msg)
    return HttpResponse(msg)

  day = get_day_from_request(request, basho_id=basho_id)
  send_email = bool(request.GET.get('send_email', True))

  # Make sure Monkey has a BashoSquadron
  basho_squadron = BashoSquadron.objects.filter(
    basho_id=basho_id,
    shikona=MONKEY_SHIKONA,
  ).first()
  if not basho_squadron:
    msg = 'Monkey exiting because no BashoSquadron exists for {0}'.format(
      get_basho_full_name(basho_id))
    logging.info(msg)
    return HttpResponse(msg)

  # Make sure Monkey has a squadron to choose from
  squadron_members_count = SquadronMember.objects.filter(
    basho_id=basho_id,
    oyakata=basho_squadron.oyakata,
    is_active=True,
  ).count()
  if squadron_members_count != SQUADRON_SIZE:
    msg = ('Monkey exiting because of incorrect squadron size '
           '({0}, expected {1}) for {2}').format(
      squadron_members_count,
      SQUADRON_SIZE,
      get_basho_full_name(basho_id))
    logging.info(msg)
    return HttpResponse(msg)

  # Make sure Monkey doesn't already have a lineup in
  preexisting_lineup_entry_count = LineupEntry.objects.filter(
    oyakata=basho_squadron.oyakata,
    basho_id=basho_id,
    day=day,
  ).count()
  if preexisting_lineup_entry_count >= LINEUP_SIZE:
    msg = ('Monkey exiting because lineup already exists '
           'for {0}, Day {1}').format(
      get_basho_full_name(basho_id), day)
    logging.info(msg)
    return HttpResponse(msg)

  # Check if we need to generate a suicide pick
  include_suicide = (is_suicide_pool_active(basho_id) and
                     basho_squadron.suicide_still_alive)

  monkey_lineup_submitter = MonkeyLineupSubmitter(
    basho_id, day, include_suicide, send_email)
  monkey_lineup_entries = monkey_lineup_submitter.submit_lineup()

  msg = 'Monkey successfully submitted for {0} Day {1}'.format(
    get_basho_full_name(basho_id), day)
  logging.info(msg)
  msg2 = 'Monkey Lineup:\n{0}'.format(
    '\n'.join([le.rikishi.shikona_for_basho(basho_id)
               for le in monkey_lineup_entries]))
  logging.info(msg2)
  return HttpResponse(u'{0}\n{1}'.format(msg, msg2))


class ResultsIndexView(generic.TemplateView):
  template_name = 'boi/resultsindex.html'
  context_object_name = 'context'

  def get_context_data(self, **kwargs):
    context = super(ResultsIndexView, self).get_context_data(**kwargs)

    basho_id = get_basho_id_from_request(self.request)

    current_day = get_current_day(basho_id)
    context['basho_id'] = basho_id
    context['day'] = current_day
    context['day_range'] = range(1, current_day)

    return context


class ResultsView(generic.TemplateView):
  template_name = 'boi/results.html'
  context_object_name = 'context'

  def get_context_data(self, **kwargs):
    context = super(ResultsView, self).get_context_data(**kwargs)

    basho_id = get_basho_id_from_request(self.request)
    day = get_day_from_request(self.request)

    context['results'] = Bout.objects.filter(
      basho_id=basho_id
    ).filter(
      day=day
    ).order_by(
      'order'
    )

    ozumo_results = OzumoBout.objects.filter(
      basho_id=basho_id
    ).filter(
      day=day
    )

    context['ozumo_results'] = {
      r.winning_rikishi.shikona_for_basho(basho_id): r.value
      for r in ozumo_results
    }

    return context


class MatchupsView(generic.TemplateView):
  template_name = 'boi/matchups.html'
  context_object_name = 'context'

  def get_context_data(self, **kwargs):
    context = super(MatchupsView, self).get_context_data(**kwargs)

    basho_id = get_basho_id_from_request(self.request)
    day = get_day_from_request(self.request, basho_id=basho_id)

    context['matchups'] = Bout.objects.filter(
      basho_id=basho_id
    ).filter(
      day=day
    ).order_by(
      'order'
    )

    context['day'] = day

    return context


class StandingsView(generic.TemplateView):
  template_name = 'boi/standings.html'

  def get_context_data(self, **kwargs):
    context = super(StandingsView, self).get_context_data(**kwargs)

    basho_id = get_basho_id_from_request(self.request)

    squadrons = list(BashoSquadron.objects.filter(
      basho_id=basho_id
    ))
    squadron_sorter = SquadronSorter()
    sorted_squadrons = squadron_sorter.sort(squadrons)

    # Calculate global stats
    global_points = sum([s.total_points for s in squadrons])
    global_bp = sum([s.points_possible for s in squadrons])
    global_coach_rating = (float(global_points) / float(global_bp)
                           if global_bp > 0 else 0.)
    avg_points = (float(global_points) / float(len(squadrons))
                  if squadrons else 0.)
    avg_bp = (float(global_bp) / float(len(squadrons))
              if squadrons else 0.)

    context['standings'] = sorted_squadrons
    context['avg_points'] = avg_points
    context['avg_bp'] = avg_bp
    context['global_coach_rating'] = global_coach_rating
    context['KACHI_BRACKET_BOUND'] = KACHI_BRACKET_BOUND
    context['KUWAHIRA_BRACKET_BOUND'] = KUWAHIRA_BRACKET_BOUND

    return context


class SuicideStandingsView(generic.TemplateView):
  template_name = 'boi/suicidestandings.html'

  def get_context_data(self, **kwargs):
    context = super(SuicideStandingsView, self).get_context_data(**kwargs)

    basho_id = get_basho_id_from_request(self.request)
    current_day = get_current_day(basho_id)

    squadrons = list(BashoSquadron.objects.filter(
      basho_id=basho_id
    ))
    squadrons.sort(cmp_squadrons_suicide)

    squadron_to_results = defaultdict(list)
    squadron_to_labels = defaultdict(list)
    for s in squadrons:
      entries = s.suicide_picks
      num_strikes = 0
      for d in range(1, current_day):
        if num_strikes >= SUICIDE_LAST_STRIKE:
          squadron_to_results[s].append(100)
          squadron_to_labels[s].append(None)
        elif d not in entries:
          squadron_to_results[s].append(-1)
          squadron_to_labels[s].append(SUICIDE_NO_SELECTION_STR)
          num_strikes += 1
        elif entries[d].did_win < 0:
          squadron_to_results[s].append(-1)

          # If the rikishi won by fusen (which counts as a loss
          # in suicide), make a special label explaining it.
          # Note that -2 from did_win denotes win by fusen.
          label = (SUICIDE_WIN_BY_FUSEN_STR
                   if entries[d].did_win == -2 else 'Lost.')
          squadron_to_labels[s].append(label)

          num_strikes += 1
        elif entries[d].did_win == 1:
          squadron_to_results[s].append(1)
          squadron_to_labels[s].append('Won.')
        else:
          squadron_to_results[s].append(0)
          squadron_to_labels[s].append(None)

    context['basho_squadrons'] = squadrons
    context['day_range'] = range(1, TOTAL_DAYS + 1)
    context['current_day_range'] = range(get_current_day(basho_id) - 1)
    context['squadron_to_results'] = dict(squadron_to_results)
    context['squadron_to_labels'] = dict(squadron_to_labels)

    return context


class UtilsView(generic.TemplateView):
  template_name = 'boi/utils.html'


class WinLossMatrixView(generic.TemplateView):
  template_name = 'boi/winlossmatrix.html'

  def get_context_data(self, **kwargs):
    context = super(WinLossMatrixView, self).get_context_data(**kwargs)

    basho_id = get_basho_id_from_request(self.request)

    squadrons = BashoSquadron.objects.filter(
      basho_id=basho_id
    )

    results_dict = defaultdict(dict)

    # For each squadron, get a list of bout results
    for s in squadrons:
      bouts = Bout.objects.filter(
        Q(oyakata1=s.oyakata) | Q(oyakata2=s.oyakata),
        basho_id=basho_id,
        day__lte=LAST_DAY_OF_REG_SEASON,
      )

      # Check each bout to see if this squadron won
      for b in bouts:
        if b.oyakata1 == s.oyakata:
          our_score = b.oyakata1_score
          their_score = b.oyakata2_score
          their_oyakata = b.oyakata2
        else:
          our_score = b.oyakata2_score
          their_score = b.oyakata1_score
          their_oyakata = b.oyakata1

        # Assign appropriate value to results dict based on bout result.
        # If bout hasn't yet happened, return a sentinel value to
        # indicate that status.
        if our_score is None or their_score is None:
          results_dict[s.oyakata_id][their_oyakata.id] = -999
        elif our_score > their_score:
          results_dict[s.oyakata_id][their_oyakata.id] = 1
        elif their_score > our_score:
          results_dict[s.oyakata_id][their_oyakata.id] = -1
        else:
          results_dict[s.oyakata_id][their_oyakata.id] = 0

    context['squadrons'] = squadrons
    context['results_dict'] = dict(results_dict)

    return context


def record_banzuke_view(request):
  basho_id = get_basho_id_from_request(request)
  error_msg = None

  # If submitted, process the new OzumoBanzukeEntry objects
  if request.method == 'POST':
    shikona = {k: v for k, v in request.POST.dict().items()
               if k.endswith('shikona')}
    ranks = {k: v for k, v in request.POST.dict().items()
             if k.endswith('rank')}
    sumoref_ids = {k: v for k, v in request.POST.dict().items()
                   if k.endswith('sumoref_id')}

    num_rikishi = int(request.POST.get('num_rikishi', 0))

    if len(shikona) != len(ranks) or len(shikona) != len(sumoref_ids):
      return HttpResponse('''
                <h1>Input not of equal lengths</h1>
                len(shikona) = {0}, len(ranks) = {1}, len(sumoref_ids) = {2}
                '''.format(len(shikona), len(ranks), len(sumoref_ids)),
                          status=500)

    if len(shikona) != num_rikishi:
      return HttpResponse('''
                <h1>Could not extract correct number of rikishi</h1>
                len(shikona) = {0}, num_rikishi = {1}<br />
                {2}
                '''.format(len(shikona), num_rikishi, str(request.POST.dict())),
                          status=500)

    try:
      with transaction.atomic():
        # Delete any old entries
        OzumoBanzukeEntry.objects.filter(
          basho_id=basho_id
        ).delete()

        for i in xrange(1, len(shikona) + 1):
          cur_shikona = shikona.get('r%s_shikona' % i)
          cur_rank = ranks.get('r%s_rank' % i)
          cur_sumoref_id = sumoref_ids.get('r%s_sumoref_id' % i)

          if not cur_shikona:
            raise BoiException('No shikona found '
                               'with id {0}'.format(
              'r%s_shikona' % i))

          if not cur_rank:
            raise BoiException('No rank found with id {0}'.format(
              'r%s_rank' % i))

          if not cur_sumoref_id:
            raise BoiException('No sumoref_id found '
                               'with id {0}'.format(
              'r%s_sumoref_id' % i))

          rikishi_obj = Rikishi.objects.filter(
            web_id=cur_sumoref_id).first()
          if not rikishi_obj:
            rikishi_profile = get_rikishi_profile(cur_sumoref_id)
            rikishi_obj = Rikishi.objects.create(
              real_name=rikishi_profile.get('Real Name'),
              dob=rikishi_profile.get('Birth Date'),
              web_id=cur_sumoref_id,
            )

          OzumoBanzukeEntry.objects.create(
            rikishi=rikishi_obj,
            shikona=cur_shikona,
            rank=cur_rank,
            basho_id=basho_id
          )
    except BoiException as e:
      return HttpResponseServerError(str(e))

  # Otherwise, display the ozumo banzuke for registration
  # First, scrape ozumo banzuke data from SumoRef
  rikishi = get_ozumo_banzuke(basho_id)

  return render(request, 'boi/recordbanzuke.html', {
    'rikishi': rikishi,
    'error_msg': error_msg,
  })


def register_players_view(request):
  basho_id = get_basho_id_from_request(request)
  old_basho_squadrons = get_previous_basho_squadrons(basho_id - 1)

  form = RegisterPlayersForm(request.POST or None,
                             old_basho_squadrons=old_basho_squadrons)

  if form.is_valid():
    # First, delete any pre-existing basho squadrons
    BashoSquadron.objects.filter(
      basho_id=basho_id,
    ).delete()

    all_oyakata = Oyakata.objects.all()

    # Create the new basho squadrons
    for o in all_oyakata:
      for field in form.visible_fields():
        if field.label == o.real_name:
          BashoSquadron.objects.create(
            oyakata=o,
            shikona=field.value(),
            rank=o.rank,
            basho_id=basho_id,
          )
          break

    # Create DraftDivisionMember objects
    # create_divisions(basho_id)

    return redirect('boi:utils')

  return render(request, 'boi/registerplayers.html', {
    'form': form,
    'basho_id': basho_id,
    'basho_full_name': get_basho_full_name(basho_id),
  })


def monkey_list_view(request):
  basho_id = get_basho_id_from_request(request)

  form = MonkeyListForm(request.POST or None)
  status = None
  results = None

  if form.is_valid():
    partial_draft_results = form.cleaned_data['partial_draft_results']
    monkey_list_generator = MonkeyListGenerator()
    results = monkey_list_generator.generate_list(partial_draft_results)

  return render(request, 'boi/monkeylist.html', {
    'form': form,
    'status': status,
    'results': results
  })


def draft_registration_view(request):
  basho_id = get_basho_id_from_request(request)

  form = DraftRegistrationForm(request.POST or None)
  status = None

  if form.is_valid():
    draft_results = form.cleaned_data['draft_results']
    draft_registrar = DraftRegistrar(basho_id)
    draft_registrar.register_draft(draft_results)

    return redirect('boi:utils')

  return render(request, 'boi/draftregistration.html', {
    'form': form,
    'status': status,
  })


def waivers_view(request):
  basho_id = get_basho_id_from_request(request)

  form = WaiversForm(request.POST or None)
  status = None

  if form.is_valid():
    waivers = form.cleaned_data['waivers']
    send_email = form.cleaned_data.get('send_email', True)

    waivers_processor = WaiversProcessor(basho_id, send_email)
    waivers_processor.process_waivers(waivers)

    return redirect('boi:utils')

  return render(request, 'boi/waivers.html', {
    'form': form,
    'status': status,
  })


def schedule_registration_view(request):
  basho_id = get_current_basho_id()
  basho_full_name = get_basho_full_name(basho_id)

  form = ScheduleRegistrationForm(request.POST or None)
  if form.is_valid():
    schedule_registrar = ScheduleRegistrar(basho_id)
    schedule_registrar.register_schedule(
      form.cleaned_data['schedule'])
    form = ScheduleRegistrationForm(None)
    return render(request, 'boi/scheduleregistration.html', {
      'basho_full_name': basho_full_name,
      'basho_id': basho_id,
      'form': form,
      'results': schedule_registrar.create_bouts_html(),
      'status': None,
    })

  return render(request, 'boi/scheduleregistration.html', {
    'basho_full_name': basho_full_name,
    'basho_id': basho_id,
    'form': form,
    'results': None,
    'status': None,
  })


def dewsweeper_view(request):
  basho_id = get_basho_id_from_request(request)
  day = get_day_from_request(request, basho_id=basho_id)

  # If we're submitting, sweep the dew!
  if request.method == 'POST':
    if not request.POST.get('confirm', False):
      return render(request, 'boi/dewsweeper.html', {
        'error_msg': 'ERROR: Set confirm=True before submitting',
      })

    send_email = bool(request.POST.get('send_email', True))

    # Make all updates/additions to objects based on Bout results
    dewsweeper_processor = DewsweeperProcessor(
      basho_id, day, send_email)
    dewsweeper_processor.sweep_dew()

    return render(request, 'boi/dewsweeperconfirm.html', {
      'day': day,
    })

  error_msgs = check_for_dewsweeper_readiness(basho_id, day)
  if error_msgs:
    return render(request, 'boi/dewsweeper.html', {
      'error_msg': '\n'.join(error_msgs),
    })

  return render(request, 'boi/dewsweeper.html', {
    'basho_id': basho_id,
    'day': day,
  })


def dewsweeper_automated_view(request):
  # First, make sure this is run by a cron job
  if request.META.get(CRON_HEADER) is None:
    logging.error(
      'Someone attempted to run Dewsweeper outside of a cron job')
    return HttpResponseForbidden(
      'Cannot run Dewsweeper outside of a cron job')

  basho_id = get_basho_id_from_request(request)
  if not is_basho_active(basho_id):
    msg = 'Dewsweeper exiting because {0} is inactive'.format(
      get_basho_full_name(basho_id))
    logging.info(msg)
    return HttpResponse(msg)

  day = get_day_from_request(request, basho_id=basho_id)
  error_msgs = check_for_dewsweeper_readiness(basho_id, day)
  if error_msgs:
    msg = 'Dewsweeper encountered one or more errors: {0}'.format(
      '\n'.join(error_msgs))
    logging.error(msg)
    return HttpResponse(msg)

  # Looks good, let's run Dewsweeper!
  send_email = bool(request.GET.get('send_email', True))

  # Make all updates/additions to objects based on Bout results
  dewsweeper_processor = DewsweeperProcessor(
    basho_id, day, send_email)
  dewsweeper_processor.sweep_dew()

  msg = 'Dewsweeper successfully run for {0} Day {1}'.format(
    get_basho_full_name(basho_id), day)
  logging.info(msg)
  return HttpResponse(msg)
