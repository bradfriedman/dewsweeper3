# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-12-29 21:18
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('boi', '0012_auto_20171229_1203'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='suicidepoolentry',
            unique_together=set([('oyakata', 'basho_id', 'day')]),
        ),
    ]
