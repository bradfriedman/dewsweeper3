# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-12-19 01:38
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('boi', '0010_auto_20171218_1638'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kimarite',
            name='name',
            field=models.CharField(max_length=20, unique=True),
        ),
        migrations.AlterField(
            model_name='oyakata',
            name='short_name',
            field=models.CharField(max_length=25, unique=True),
        ),
        migrations.AlterField(
            model_name='rikishi',
            name='real_name',
            field=models.CharField(max_length=50, unique=True),
        ),
        migrations.AlterUniqueTogether(
            name='bout',
            unique_together=set([('oyakata1', 'oyakata2', 'basho_id', 'day')]),
        ),
        migrations.AlterUniqueTogether(
            name='ozumobanzukeentry',
            unique_together=set([('rikishi', 'basho_id')]),
        ),
    ]
