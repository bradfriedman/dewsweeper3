# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-12 22:23
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('boi', '0005_auto_20170911_1337'),
    ]

    operations = [
        migrations.AlterField(
            model_name='squadronmember',
            name='rikishi',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='boi.Rikishi'),
        ),
    ]
