# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-03-10 22:02
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('boi', '0016_switch_shikona_to_utf8mb4_columns'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='bashosquadron',
            options={},
        ),
        migrations.AlterModelOptions(
            name='suicidepoolentry',
            options={'verbose_name': 'suicide pick'},
        ),
    ]
