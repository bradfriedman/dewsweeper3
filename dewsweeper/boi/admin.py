# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime

from django.contrib import admin

from .boi_config import BASHO_NAMES

from .models import BashoSquadron, Oyakata
from .models import Rikishi
from .models import Bout, LineupEntry
from .models import Kimarite, OzumoBout
from .models import OzumoBanzukeEntry
from .models import SquadronMember
from .models import SuicidePoolEntry

from .utils import convert_rank_to_short_string
from .utils import get_basho_full_name
from .utils import get_basho_ids_in_month
from .utils import get_basho_ids_in_year

rank_lambda = lambda obj: convert_rank_to_short_string(obj.rank)
rank_lambda.short_description = 'Rank'

basho_name_lambda = lambda obj: get_basho_full_name(obj.basho_id)
basho_name_lambda.short_description = 'Basho'

oyakata_shikona_lambda = lambda obj: obj.oyakata.shikona_for_basho(
  obj.basho_id)
oyakata_shikona_lambda.short_description = 'Oyakata'

rikishi_shikona_lambda = lambda obj: obj.rikishi.shikona_for_basho(
  obj.basho_id)
rikishi_shikona_lambda.short_description = 'Rikishi'


class BashoYearFilter(admin.SimpleListFilter):
  title = 'year'

  parameter_name = 'year'

  def lookups(self, request, model_admin):
    return (
      (year, str(year))
      for year in range(2003, datetime.datetime.now().year + 1)
    )

  def queryset(self, request, queryset):
    year = self.value()
    if year is None or not year.isdigit():
      return queryset
    return queryset.filter(
      basho_id__in=get_basho_ids_in_year(int(year)),
    )


class BashoMonthFilter(admin.SimpleListFilter):
  title = 'tournament'

  parameter_name = 'tournament'

  def lookups(self, request, model_admin):
    return ((n, BASHO_NAMES[n]) for n in range(len(BASHO_NAMES)))

  def queryset(self, request, queryset):
    month = self.value()
    if month is None or not month.isdigit():
      return queryset
    return queryset.filter(
      basho_id__in=get_basho_ids_in_month(int(month)),
    )


@admin.register(Bout)
class BoutAdmin(admin.ModelAdmin):
  o1_shikona_lambda = lambda b: b.oyakata1.shikona_for_basho(b.basho_id)
  o1_shikona_lambda.short_description = 'Oyakata 1'

  o2_shikona_lambda = lambda b: b.oyakata2.shikona_for_basho(b.basho_id)
  o2_shikona_lambda.short_description = 'Oyakata 2'

  ordering = ['-basho_id', '-day', 'order']
  list_display = [o1_shikona_lambda, 'oyakata1_score', o2_shikona_lambda,
                  'oyakata2_score', basho_name_lambda, 'day', 'order']
  list_filter = (BashoMonthFilter, BashoYearFilter,)


@admin.register(BashoSquadron)
class BashoSquadronAdmin(admin.ModelAdmin):
  ordering = ['-basho_id', 'place', 'shikona']
  list_display = ['shikona', basho_name_lambda, rank_lambda, 'place',
                  'suicide_place', 'suicide_num_strikes', 'suicide_elim_day']
  list_filter = (BashoMonthFilter, BashoYearFilter,)


@admin.register(Kimarite)
class KimariteAdmin(admin.ModelAdmin):
  ordering = ['id']
  list_display = ['name', 'id']


@admin.register(LineupEntry)
class LineupEntryAdmin(admin.ModelAdmin):
  ordering = ['-basho_id', '-day', 'oyakata', 'order']
  list_display = [rikishi_shikona_lambda, oyakata_shikona_lambda,
                  basho_name_lambda, 'day', 'order']
  list_filter = ('oyakata', BashoMonthFilter, BashoYearFilter,)


@admin.register(Oyakata)
class OyakataAdmin(admin.ModelAdmin):
  ordering = ['first_name']
  list_display = ['real_name', rank_lambda, 'is_kadoban',
                  'is_repromotion_eligible', 'is_tsunatori',
                  'is_jun_tsunatori']


@admin.register(OzumoBanzukeEntry)
class OzumoBanzukeEntryAdmin(admin.ModelAdmin):
  ordering = ['-basho_id', 'rank', 'shikona']
  list_display = ['shikona', rank_lambda, basho_name_lambda, ]
  list_filter = (BashoMonthFilter, BashoYearFilter,)


@admin.register(OzumoBout)
class OzumoBoutAdmin(admin.ModelAdmin):
  winning_shikona_lambda = lambda ob: ob.winning_rikishi.shikona_for_basho(
    ob.basho_id)
  winning_shikona_lambda.short_description = 'Winner'

  losing_shikona_lambda = lambda ob: ob.losing_rikishi.shikona_for_basho(
    ob.basho_id)
  losing_shikona_lambda.short_description = 'Loser'

  winning_rank_lambda = lambda ob: convert_rank_to_short_string(
    ob.winning_rikishi_rank)
  winning_rank_lambda.short_description = 'Rank'

  losing_rank_lambda = lambda ob: convert_rank_to_short_string(
    ob.losing_rikishi_rank)
  losing_rank_lambda.short_description = 'Rank'

  ordering = ['-basho_id', '-day', 'winning_rikishi_rank',
              'losing_rikishi_rank']
  list_display = [winning_shikona_lambda, winning_rank_lambda,
                  losing_shikona_lambda, losing_rank_lambda,
                  'kimarite', 'value', basho_name_lambda, 'day', ]
  list_filter = (BashoMonthFilter, BashoYearFilter, 'day', 'value')


@admin.register(Rikishi)
class RikishiAdmin(admin.ModelAdmin):
  most_recent_shikona_lambda = lambda r: r.most_recent_shikona
  most_recent_shikona_lambda.short_description = 'Shikona'

  list_display = [most_recent_shikona_lambda, 'real_name', 'dob', 'web_id']


@admin.register(SquadronMember)
class SquadronMemberAdmin(admin.ModelAdmin):
  ordering = ['-basho_id', 'oyakata', '-is_active', 'selection_order']
  list_display = [oyakata_shikona_lambda, rikishi_shikona_lambda,
                  basho_name_lambda, 'selection_order', 'is_active']
  list_filter = ('oyakata', BashoMonthFilter, BashoYearFilter,)


@admin.register(SuicidePoolEntry)
class SuicidePoolEntryAdmin(admin.ModelAdmin):
  ordering = ['-basho_id', '-day', 'oyakata']
  list_display = [oyakata_shikona_lambda, rikishi_shikona_lambda,
                  basho_name_lambda, 'day']
  list_filter = ('oyakata', BashoMonthFilter, BashoYearFilter,)
