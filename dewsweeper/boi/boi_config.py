TOTAL_DAYS = 15
DAY_ONES = [12, 14]
DAY_TWOS = [13, 15]

WINNING_STR = u'{0} no kachi desu! ({1}-{2})'
DAY_ONE_WINNING_STR = u'{0} wa {1} ni katte imasu ({2}-{3})'
HIKIWAKE_STR = u'Hikiwake desu! ({0}-{1})'
TIEGAME_STR = u'Taigeemu desu! ({0}-{1})'
TIEBREAKER_STR = u'Playoff tiebreaker invoked.'

BASHO_NAMES = ['Hatsu', 'Haru', 'Natsu', 'Nagoya', 'Aki', 'Kyushu']
BASHO_MONTHS = ['01', '03', '05', '07', '09', '11']

BASE_PTS = 2
CHANKO_BOUNDS = [-2, -1, 0, 1, 5, 9, 13]

NUM_PLAYERS = 12

# Make sure these two numbers multiply up to NUM_PLAYERS
DRAFT_DIVISIONS = 3
DRAFT_DIVISION_SIZE = 4

LINEUP_SIZE = 4
SQUADRON_SIZE = 6

LAST_DAY_OF_REG_SEASON = 11

KACHI_BRACKET_BOUND = 4
KUWAHIRA_BRACKET_BOUND = 8

SUICIDE_LAST_STRIKE = 2
SUICIDE_NO_SELECTION_STR = 'No selection for this day.'
SUICIDE_WIN_BY_FUSEN_STR = ('Won by fusensho, but those count as losses in '
                            'suicide.')

SUMOREF_ROOT_URL = 'http://sumodb.sumogames.de/'
SUMOREF_BANZUKE_URL = SUMOREF_ROOT_URL + 'Banzuke.aspx'
SUMOREF_RESULTS_URL = SUMOREF_ROOT_URL + 'Results.aspx'
SUMOREF_RIKISHI_URL = SUMOREF_ROOT_URL + 'Rikishi.aspx'

EMAIL_FROM_ADDRESS = 'boi.dewsweeper@dewsweeper3.appspotmail.com'
EMAIL_TO_ADDRESS = 'yokozuna@googlegroups.com'

PLACE_TO_RANK_CHANGE = {
  1: -4,
  2: -3,
  3: -3,
  4: -2,
  5: -1,
  6: 0,
  7: 0,
  8: 1,
  9: 2,
  10: 3,
  11: 3,
  12: 4,
}
RANK_FLOOR = 10

MONKEY_SHIKONA = 'Makino the Magic Monkey'

CRON_HEADER = 'HTTP_X_APPENGINE_CRON'

# Skipped Haru 2011, Natsu 2020, Nagoya 2023, and Aki 2023 so far
SKIPPED_BASHO_IDS = [546, 600, 618, 619]

BASHO_ID = 620
