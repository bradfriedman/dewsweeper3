from django.db import transaction

from .boi_config import DRAFT_DIVISIONS
from .boi_config import DRAFT_DIVISION_SIZE
from .boi_config import SQUADRON_SIZE

from .exceptions import BoiDraftException

from .models import Oyakata
from .models import OzumoBanzukeEntry
from .models import SquadronMember

from .utils import fuzzy_oyakata_lookup
from .utils import get_basho_full_name


class DraftRegistrar(object):

  def __init__(self, basho_id):
    self.basho_id = basho_id

  @transaction.atomic
  def register_draft(self, draft, force=False):
    registered_squadron_members = []
    lines = [line for line in draft.split('\n')
             if line and not line.isspace()]

    # Check if squadron members already exist
    existing_squadrons = SquadronMember.objects.filter(
      basho_id=self.basho_id
    )
    if existing_squadrons and not force:
      raise BoiDraftException(
        'Squadron members already exist for {basho_name}. Use '
        'force=True to overwrite'.format(
          get_basho_full_name(self.basho_id)))

    # First, remove all existing squadron members
    SquadronMember.objects.filter(
      basho_id=self.basho_id,
    ).delete()

    # Then, read the draft results and create squadron members
    for division in range(DRAFT_DIVISIONS):
      oyakata_line_num = division * (SQUADRON_SIZE + 1)
      oyakata_line = lines[oyakata_line_num]
      oyakata_tokens = [o.strip() for o in oyakata_line.split('\t')]
      if len(oyakata_tokens) != DRAFT_DIVISION_SIZE:
        raise BoiDraftException(
          'Incorrect number of oyakata in line {line}.'.format(
            line=oyakata_line,
          ))

      oyakata_objs = [self._get_oyakata(o) for o in oyakata_tokens]

      # Read each line of selections
      for rikishi_row_num in range(SQUADRON_SIZE):
        rikishi_line_num = oyakata_line_num + rikishi_row_num + 1
        rikishi_line = lines[rikishi_line_num]
        rikishi_tokens = [r.strip() for r in rikishi_line.split('\t')]
        if len(rikishi_tokens) != DRAFT_DIVISION_SIZE:
          raise BoiDraftException(
            'Incorrect number of rikishi in line {line}.'.format(
              line=rikishi_line,
            ))

        for n, shikona in enumerate(rikishi_tokens):
          banzuke_entry_obj = OzumoBanzukeEntry.objects.filter(
            basho_id=self.basho_id,
            shikona=shikona,
          ).first()
          if not banzuke_entry_obj:
            raise BoiDraftException(
              'No OzumoBanzukeEntry for shikona {shikona}'.format(
                shikona=shikona))
          new_squadron_member = SquadronMember.objects.create(
            oyakata=oyakata_objs[n],
            rikishi=banzuke_entry_obj.rikishi,
            basho_id=self.basho_id,
            selection_order=rikishi_row_num + 1,
            is_active=True,
          )
          registered_squadron_members.append(new_squadron_member)

    return registered_squadron_members

  def _get_oyakata(self, oyakata):
    short_name = fuzzy_oyakata_lookup(oyakata)
    if not short_name:
      raise BoiDraftException(
        'Could not fuzzy parse oyakata name {0}'.format(oyakata))
    return Oyakata.objects.get(short_name=short_name)
