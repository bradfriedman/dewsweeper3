from django.db import transaction
from django.utils.encoding import smart_text

from .boi_config import LAST_DAY_OF_REG_SEASON
from .boi_config import NUM_PLAYERS

from .exceptions import BoiSchedulingException

from .models import BashoSquadron
from .models import Bout

from .utils import get_basho_full_name


class ScheduleRegistrar(object):

  def __init__(self, basho_id):
    self.basho_id = basho_id
    self.registered_bouts = []

  @transaction.atomic
  def register_schedule(self, schedule, force=False):
    lines = [line for line in schedule.split('\n')
             if line and not line.isspace()]
    cur_line_index = 0

    # Check if bouts already exist here
    existing_bouts = Bout.objects.filter(
      basho_id=self.basho_id
    )
    if existing_bouts and not force:
      raise BoiSchedulingException(
        'Bouts already exist for {basho_name}. Use force=True to '
        'overwrite'.format(
          basho_name=get_basho_full_name(self.basho_id)))

    for day in range(1, LAST_DAY_OF_REG_SEASON + 1):
      # Skip to line that days "Day [day]"
      while str(day) not in lines[cur_line_index]:
        cur_line_index += 1
        self._check_out_of_bounds(cur_line_index, len(lines))

      # Skip past the "Day [day]" line
      cur_line_index += 1

      # Read in the bouts for this day
      for bout_order in range(1, (NUM_PLAYERS / 2) + 1):
        self._check_out_of_bounds(cur_line_index, len(lines))
        oyakata1, oyakata2 = self._extract_oyakata_from_schedule_line(
          lines[cur_line_index])
        self.registered_bouts.append(Bout.objects.create(
          oyakata1=oyakata1,
          oyakata2=oyakata2,
          basho_id=self.basho_id,
          day=day,
          order=bout_order,
        ))
        # print "{0} faces {1} on day {2}".format(oyakata1, oyakata2, day)
        cur_line_index += 1

    return self.registered_bouts

  def create_bouts_html(self):
    return smart_text(u'\n'.join(
      [self.create_single_bout_string(b) for b in self.registered_bouts])
    )

  def create_single_bout_string(self, b):
    return smart_text(u'{o1} faces {o2} on Day {day}'.format(
      o1=b.oyakata1.shikona_for_basho(self.basho_id),
      o2=b.oyakata2.shikona_for_basho(self.basho_id),
      day=b.day,
    ))

  def _extract_oyakata_from_schedule_line(self, line):
    shikona1, shikona2 = self._extract_shikona_from_schedule_line(line)

    basho_squadron1 = BashoSquadron.objects.filter(
      basho_id=self.basho_id,
      shikona=shikona1,
    ).first()
    basho_squadron2 = BashoSquadron.objects.filter(
      basho_id=self.basho_id,
      shikona=shikona2,
    ).first()

    if not basho_squadron1:
      raise BoiSchedulingException(
        'Could not find basho squadron with shikona {shikona}.'.format(
          shikona=shikona1
        ))

    if not basho_squadron2:
      raise BoiSchedulingException(
        'Could not find basho squadron with shikona {shikona}.'.format(
          shikona=shikona2
        ))

    return [basho_squadron1.oyakata, basho_squadron2.oyakata]

  def _extract_shikona_from_schedule_line(self, line):
    separators = ['vs.', 'vs', 'versus']
    for separator in separators:
      if separator in line:
        results = [shikona.strip()
                   for shikona in line.split(separator, 1)]
        if len(results) != 2:
          raise BoiSchedulingException(
            "Didn't get two shikona from line {0} ({1})".format(
              line, results))
        return results
    raise BoiSchedulingException(
      'Could not extract shikona from line {0}'.format(line))

  def _check_out_of_bounds(self, cur_line, len_lines):
    if cur_line >= len_lines:
      raise BoiSchedulingException('EOL reached in schedule input.')
