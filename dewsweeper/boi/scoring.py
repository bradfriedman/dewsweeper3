from django.db import transaction

from .boi_config import BASE_PTS, LINEUP_SIZE
from .boi_config import DAY_ONES, DAY_TWOS
from .boi_config import LAST_DAY_OF_REG_SEASON
from .boi_config import NUM_PLAYERS
from .boi_config import PLACE_TO_RANK_CHANGE
from .boi_config import RANK_FLOOR
from .boi_config import TOTAL_DAYS

from .emailers import DewsweeperEmailer

from .exceptions import BoiScoringException

from .models import BashoSquadron
from .models import Bout
from .models import OzumoBout

from .scrapers import OzumoResultsScraper


def score_lineup(lineup_entries, squadron_members, results_by_rikishi,
                 basho_id):
  squadron_rikishi_results = []
  points = 0
  chanko = 0
  best_possible = 0
  for lineup_entry in lineup_entries:
    rikishi = lineup_entry.rikishi.shikona_for_basho(basho_id)
    if rikishi in results_by_rikishi:
      rikishi_score = results_by_rikishi[rikishi][0]
    else:
      rikishi_score = 0
    points += rikishi_score
    chanko += max(0, rikishi_score - BASE_PTS)

  # Calculate best possible
  for squadron_member in squadron_members:
    rikishi = squadron_member.rikishi.shikona_for_basho(basho_id)
    if rikishi in results_by_rikishi:
      squadron_rikishi_results.append(results_by_rikishi[rikishi][0])
    else:
      squadron_rikishi_results.append(0)
  squadron_rikishi_results.sort(reverse=True)
  for r in squadron_rikishi_results[:LINEUP_SIZE]:
    best_possible += r

  return (points, chanko, best_possible)


class DewsweeperProcessor(object):

  def __init__(self, basho_id, current_day, send_email):
    self.basho_id = basho_id
    self.current_day = current_day
    self.send_email = send_email
    self.bouts = Bout.objects.filter(
      basho_id=basho_id,
      day=current_day,
    )

  def sweep_dew(self):
    results, results_by_rikishi = self._get_ozumo_results()
    self._process_ozumo_results(results, results_by_rikishi)

  @transaction.atomic
  def _process_ozumo_results(self, results, results_by_rikishi):
    # Delete any pre-existing OzumoBouts
    OzumoBout.objects.filter(
      basho_id=self.basho_id,
      day=self.current_day,
    ).delete()

    # Record OzumoBouts
    for r in results:
      OzumoBout.objects.create(
        winning_rikishi=r['winning_rikishi'],
        winning_rikishi_rank=r['winning_rikishi_rank'],
        losing_rikishi=r['losing_rikishi'],
        losing_rikishi_rank=r['losing_rikishi_rank'],
        kimarite=r['kimarite'],
        value=r['value'],
        basho_id=self.basho_id,
        day=self.current_day,
      )

    # Update Bouts
    for b in self.bouts:
      b.oyakata1_score, b.oyakata1_chanko, b.oyakata1_bp = (
        score_lineup(b.oyakata1_effective_lineup, b.oyakata1_squadron,
                     results_by_rikishi, self.basho_id)
      )
      b.oyakata2_score, b.oyakata2_chanko, b.oyakata2_bp = (
        score_lineup(b.oyakata2_effective_lineup, b.oyakata2_squadron,
                     results_by_rikishi, self.basho_id)
      )
      b.save()

    # Collect BashoSquadrons
    basho_squadrons = BashoSquadron.objects.filter(
      basho_id=self.basho_id,
    )
    squadron_sorter = SquadronSorter()
    sorted_squadrons = squadron_sorter.sort(basho_squadrons)

    # If it's the last day of the regular season, set up first
    # round of playoffs
    if self.current_day == LAST_DAY_OF_REG_SEASON:
      self._schedule_first_round_of_playoffs(sorted_squadrons)
    # ...or if it's the end of a two-day playoff bout, set up
    # next round of playoffs
    elif (self.current_day in DAY_TWOS and
          self.current_day + 1 <= TOTAL_DAYS):
      self._schedule_deep_round_of_playoffs()
    # ... or if it's the last day of the basho, update BashoSquadrons with
    # with places, then update the banzuke with new ranks and/or flags
    # (is_tsunatori, etc.).
    elif self.current_day == TOTAL_DAYS:
      # Update place for each BashoSquadron
      for n, b in enumerate(self.bouts):
        winning_bs = b.winning_basho_squadron
        losing_bs = b.losing_basho_squadron
        winning_bs.place = (n * 2) + 1
        losing_bs.place = (n * 2) + 2
        winning_bs.save()
        losing_bs.save()
      self._update_banzuke()

    if self.send_email:
      email_result = self._send_results_email(
        results_by_rikishi, sorted_squadrons)

  def _get_ozumo_results(self):
    results_scraper = OzumoResultsScraper(self.basho_id, self.current_day)
    return (results_scraper.scrape(),
            results_scraper.get_bout_results_by_rikishi())

  def _schedule_first_round_of_playoffs(self, sorted_squadrons):
    '''Schedule the first round of playoffs after the regular season.

    Args:
      sorted_squadrons: A QuerySet of BashoSquadrons in standings order.
    '''
    # Create first match of division for current_day+1
    # AND current_day+2 (if current_day+1 is a Day One,
    # which it always will be under the current format)
    days_to_schedule = [self.current_day + 1]
    if self.current_day + 1 in DAY_ONES:
      days_to_schedule.append(self.current_day + 2)

    for division in range(3):
      for day in days_to_schedule:
        # The 1 vs. 4 matchup
        Bout.objects.create(
          oyakata1=sorted_squadrons[division * 4].oyakata,
          oyakata2=sorted_squadrons[(division * 4) + 3].oyakata,
          basho_id=self.basho_id,
          day=day,
          order=(division * 2) + 1,
        )
        # The 2 vs. 3 matchup
        Bout.objects.create(
          oyakata1=sorted_squadrons[(division * 4) + 1].oyakata,
          oyakata2=sorted_squadrons[(division * 4) + 2].oyakata,
          basho_id=self.basho_id,
          day=day,
          order=(division * 2) + 2,
        )

  def _schedule_deep_round_of_playoffs(self):
    '''Schedule a deep round of playoffs.'''
    for i in range(3):
      division_bout1 = self.bouts[i * 2]
      division_bout2 = self.bouts[(i * 2) + 1]
      new_bout1_oyakata1 = new_bout1_oyakata2 = None
      new_bout2_oyakata1 = new_bout2_oyakata2 = None

      # If the 1-seed wins, it'll be oyakata1 for the first bout
      # Also, the 4-seed becomes oyakata2 for the second bout
      if division_bout1.winner > 0:
        new_bout1_oyakata1 = division_bout1.oyakata1
        new_bout2_oyakata2 = division_bout1.oyakata2
        top_seed_won = True
      # Otherwise, the 1-seed becomes oyakata1 for second bout
      # And the 4-seed becomes oyakata2 for the first bout
      else:
        new_bout2_oyakata1 = division_bout1.oyakata1
        new_bout1_oyakata2 = division_bout1.oyakata2
        top_seed_won = False

      # Place the 2- and 3-seeds
      if division_bout2.winner > 0:
        if top_seed_won:
          assert new_bout1_oyakata2 is None
          new_bout1_oyakata2 = division_bout2.oyakata1
          assert new_bout2_oyakata1 is None
          new_bout2_oyakata1 = division_bout2.oyakata2
        else:
          assert new_bout1_oyakata1 is None
          new_bout1_oyakata1 = division_bout2.oyakata1
          assert new_bout2_oyakata2 is None
          new_bout2_oyakata2 = division_bout2.oyakata2
      else:
        if top_seed_won:
          assert new_bout2_oyakata1 is None
          new_bout2_oyakata1 = division_bout2.oyakata1
          assert new_bout1_oyakata2 is None
          new_bout1_oyakata2 = division_bout2.oyakata2
        else:
          assert new_bout2_oyakata2 is None
          new_bout2_oyakata2 = division_bout2.oyakata1
          assert new_bout1_oyakata1 is None
          new_bout1_oyakata1 = division_bout2.oyakata2

      # Create first match of division for current_day+1
      # AND current_day+2 (if current_day+1 is a Day One,
      # which it always will be under the current format)
      days_to_schedule = [self.current_day + 1]
      if self.current_day + 1 in DAY_ONES:
        days_to_schedule.append(self.current_day + 2)

      for day in days_to_schedule:
        Bout.objects.create(
          oyakata1=new_bout1_oyakata1,
          oyakata2=new_bout1_oyakata2,
          basho_id=self.basho_id,
          day=day,
          order=(i * 2) + 1,
        )
        Bout.objects.create(
          oyakata1=new_bout2_oyakata1,
          oyakata2=new_bout2_oyakata2,
          basho_id=self.basho_id,
          day=day,
          order=(i * 2) + 2,
        )

  def _update_basho_squadrons_with_final_basho_results(self):
    '''
    Update the place field of BashoSquadron objects at the end of the basho.
    '''
    for i in range(NUM_PLAYERS / 2):
      winning_place = (i * 2) + 1
      losing_place = winning_place + 1

      # Determine who won and who lost
      if self.bouts[i].winner > 0:
        winning_oyakata = self.bouts[i].oyakata1
        losing_oyakata = self.bouts[i].oyakata2
      else:
        winning_oyakata = self.bouts[i].oyakata2
        losing_oyakata = self.bouts[i].oyakata1

      # Update the BashoSquadron objects with their basho placements
      winning_bs = BashoSquadron.objects.get(
        oyakata=winning_oyakata,
        basho_id=self.basho_id,
      )
      winning_bs.place = winning_place
      winning_bs.save()

      losing_bs = BashoSquadron.objects.get(
        oyakata=losing_oyakata,
        basho_id=self.basho_id,
      )
      losing_bs.place = losing_place
      losing_bs.save()

  def _calculate_new_rank(self, old_rank, rank_change):
    return max(-1, min(RANK_FLOOR, old_rank + rank_change))

  def _promote_oyakata_to_yokozuna(self, o):
    o.rank = -3
    o.is_tsunatori = False
    o.is_kadoban = False
    o.is_repromotion_eligible = False
    o.save()
    return

  def _update_oyakata_rank_and_flags(self, oyakata, place):
    # Once a yokozuna, always a yokozuna
    if oyakata.rank == -3:
      return

    rank_change = PLACE_TO_RANK_CHANGE[place]

    # OZEKI CHECKS
    if oyakata.rank == -2:
      # See if we're promoting to yokozuna!
      prev_prev_bs = BashoSquadron.objects.filter(
        oyakata=oyakata,
        basho_id=self.basho_id - 2,
        rank=-2,
      ).first()
      prev_bs = BashoSquadron.objects.filter(
        oyakata=oyakata,
        basho_id=self.basho_id - 1,
        rank=-2,
      ).first()
      # If oyakata is tsunatori and 1st place => promote!
      if place == 1 and oyakata.is_tsunatori:
        self._promote_oyakata_to_yokozuna(oyakata)
        return
      # If oyakata got first, got a kachi in the previous basho, and got first
      # in the basho before that, all at the rank of ozeki, promote!
      if place == 1:
        if (prev_prev_bs and prev_bs and
            prev_bs.place <= 4 and prev_prev_bs.place == 1):
          self._promote_oyakata_to_yokozuna(oyakata)
          return
      # If oyakata got second or better, check the jun-yusho-jun criterion for
      # promotion
      if place <= 2:
        # If previous two basho placements and this placement add up to 5 or
        # less, promote!
        # Meaning: if an oyakata got (yusho, jun-yusho, jun-yusho), in any
        # order, while at the rank of ozeki, they can be promoted!
        if (prev_prev_bs and prev_bs and
            prev_prev_bs.place + prev_bs.place + place <= 5):
          self._promote_oyakata_to_yokozuna(oyakata)
          return
      # If an ozeki and kadoban, see if we're demoting to sekiwake.
      # Also check if we're removing kadoban from the ozeki.
      # Demotion occurs if rank change would be negative.
      # Kadoban removal occurs if rank change would be positive
      if oyakata.is_kadoban:
        if rank_change > 0:
          oyakata.rank = -1
          oyakata.is_tsunatori = False
          oyakata.is_kadoban = False
          oyakata.is_repromotion_eligible = True
          oyakata.save()
          return
        if rank_change < 0:
          oyakata.is_kadoban = False

      # If the ozeki gets 3rd or lower, remove all tsunatori status
      if place >= 3:
        oyakata.is_tsunatori = False

      # Check if we need to assign kadoban to this oyakata
      if rank_change > 0 and not oyakata.is_kadoban:
        oyakata.is_kadoban = True

      # If an ozeki wins a yusho but isn't tsunatori, make him tsunatori
      if place == 1 and not oyakata.is_tsunatori:
        oyakata.is_tsunatori = True

      oyakata.save()
      return

    # SEKIWAKE CHECKS
    if oyakata.rank == -1:
      # If sekiwake yushos or jun-yushos, promote to ozeki!
      # Also if repromotion-eligible and gets 3rd or better
      if place <= 2 or (place <= 3 and oyakata.is_repromotion_eligible):
        oyakata.rank = -2
        oyakata.is_kadoban = False
        oyakata.is_repromotion_eligible = False
        oyakata.is_tsunatori = False

        oyakata.save()
        return

    # All others - apply rank change, but make sure not to go above
    # sekiwake or go below the rank floor
    oyakata.rank = self._calculate_new_rank(oyakata.rank, rank_change)
    oyakata.is_kadoban = False
    oyakata.is_repromotion_eligible = False
    oyakata.is_tsunatori = False

    oyakata.save()

  def _update_banzuke(self):
    '''
    Update Oyakata objects with new ranks and flags.
    '''
    # Get all BashoSquadrons for this basho
    # NOTE: All BashoSquadrons must already have been updated with
    #       the place field filled in.
    basho_squadrons = BashoSquadron.objects.filter(
      basho_id=self.basho_id,
      place__isnull=False,
    )

    # If we don't have the right number of BashoSquadrons, abort now
    if basho_squadrons.count() != NUM_PLAYERS:
      raise BoiScoringException(
        "Couldn't update Oyakata objects because there were {0} "
        "BashoSquadrons with the place field for basho id {1} "
        "(expecting {2}).".format(
          basho_squadrons.count(), self.basho_id, NUM_PLAYERS))

    for bs in basho_squadrons:
      self._update_oyakata_rank_and_flags(bs.oyakata, bs.place)

  def _send_results_email(self, results_by_rikishi, sorted_squadrons):
    '''Send Dewsweeper results email.

    Args:
      results_by_rikishi: A dict mapping rikishi shikona to
        a tuple of (value, kimarite).
      sorted_squdrons: A list of BashoSquadrons sorted by standings order.

     Returns:
       The value of EmailMessage.send(), which is the number of successfully
         delivered messages (should only be 0 or 1).
    '''
    dewsweeper_emailer = DewsweeperEmailer(
      self.bouts, results_by_rikishi, sorted_squadrons,
      self.basho_id, self.current_day)
    dewsweeper_emailer.send_email()


class SquadronSorter(object):

  def __init__(self):
    self.all_tied_squadrons = []
    self.placed_squadrons = set()

  def sort(self, squadrons):
    results = []
    key = lambda s: s.standings_points
    sorted_squadrons = sorted(squadrons, key=key, reverse=True)
    keyed_squadrons = [(s, s.standings_points) for s in sorted_squadrons]
    joined_squadrons = self._combine_keyed_squadrons(keyed_squadrons)

    for join in joined_squadrons:
      results.extend(self._break_ties(join))

    return results

  def _break_ties(self, tied_squadrons):
    '''
    Break ties between 2 or more BashoSquadrons that have the same number of
    points and chanko.

    Args:
      tied_squadrons: A list of BashoSquadrons

    Returns:
      A list of the BashoSquadrons from highest place to lowest place
    '''
    self.all_tied_squadrons = list(tied_squadrons)
    self.placed_squadrons = set()
    return self._break_ties_fresh_start()

  def _break_ties_fresh_start(self):
    unplaced_squadrons = [s for s in self.all_tied_squadrons
                          if s not in self.placed_squadrons]
    return self._break_ties_entry_point(unplaced_squadrons)

  def _break_ties_entry_point(self, tied_squadrons):
    return self._break_ties_by_h2h(tied_squadrons)

  def _break_ties_by_h2h(self, tied_squadrons):
    # Base case
    if len(tied_squadrons) <= 1:
      self.placed_squadrons.update(tied_squadrons)
      return tied_squadrons

    h2h_record_points = {}

    # Determine head-to-head records
    for squadron in tied_squadrons:
      cur_h2h_record_points = 0
      for other_squadron in tied_squadrons:
        if squadron == other_squadron:
          continue
        h2h_result = squadron.result_against(other_squadron)
        if h2h_result:
          cur_h2h_record_points += h2h_result

      h2h_record_points[squadron] = cur_h2h_record_points

    # Check result
    key = lambda s: h2h_record_points[s]
    sorted_squadrons = sorted(tied_squadrons, key=key, reverse=True)
    keyed_squadrons = [(s, key(s)) for s in sorted_squadrons]

    return self._generate_step_results(
      sorted_squadrons, keyed_squadrons,
      self._break_ties_by_points)

  def _break_ties_by_points(self, tied_squadrons):
    # Base case
    if len(tied_squadrons) <= 1:
      self.placed_squadrons.update(tied_squadrons)
      return tied_squadrons

    # Check result
    key = lambda s: s.total_points
    sorted_squadrons = sorted(tied_squadrons, key=key, reverse=True)
    keyed_squadrons = [(s, key(s)) for s in sorted_squadrons]

    return self._generate_step_results(
      sorted_squadrons, keyed_squadrons,
      self._break_ties_by_chanko)

  def _break_ties_by_chanko(self, tied_squadrons):
    # Base case
    if len(tied_squadrons) <= 1:
      self.placed_squadrons.update(tied_squadrons)
      return tied_squadrons

    key = lambda s: s.total_chanko
    sorted_squadrons = sorted(tied_squadrons, key=key, reverse=True)
    keyed_squadrons = [(s, key(s)) for s in sorted_squadrons]

    return self._generate_step_results(
      sorted_squadrons, keyed_squadrons,
      self._break_ties_by_points_against)

  def _break_ties_by_points_against(self, tied_squadrons):
    # Base case
    if len(tied_squadrons) <= 1:
      self.placed_squadrons.update(tied_squadrons)
      return tied_squadrons

    key = lambda s: s.points_against
    sorted_squadrons = sorted(tied_squadrons, key=key, reverse=True)
    keyed_squadrons = [(s, key(s)) for s in sorted_squadrons]

    return self._generate_step_results(
      sorted_squadrons, keyed_squadrons,
      self._break_ties_by_rank)

  def _break_ties_by_rank(self, tied_squadrons):
    # Base case
    if len(tied_squadrons) <= 1:
      self.placed_squadrons.update(tied_squadrons)
      return tied_squadrons

    # Check result
    key = lambda s: s.rank
    sorted_squadrons = sorted(tied_squadrons, key=key, reverse=False)
    keyed_squadrons = [(s, key(s)) for s in sorted_squadrons]

    return self._generate_step_results(
      sorted_squadrons, keyed_squadrons,
      self._break_ties_noop)

  def _break_ties_noop(self, tied_squadrons):
    self.placed_squadrons.update(tied_squadrons)
    return tied_squadrons

  def _combine_keyed_squadrons(self, keyed_squadrons):
    '''
    Return a list of lists of squadrons, where each sublist's members are
    tied with each other based on the keys of each squadron.

    Example return value: [[o1], [o2, o3], [o4], [o5, o6, o7]]

    Args:
      keyed_squadrons: A list of tuples of (BashoSquadron, key)

    Returns:
      A list of lists of BashoSquadrons, where each sublist's members are
      tied with each other.
    '''
    if not keyed_squadrons:
      return keyed_squadrons

    results = []
    last_key = keyed_squadrons[0][1]
    sublist = []

    for s in keyed_squadrons:
      cur_key = s[1]
      if cur_key != last_key:
        results.append(sublist)
        sublist = []
        last_key = cur_key
      sublist.append(s[0])

    # One final append to the result after we complete the list
    results.append(sublist)

    return results

  def _generate_step_results(self, sorted_squadrons, keyed_squadrons,
                             next_step):
    results = []

    # Join by ties
    joined_squadrons = self._combine_keyed_squadrons(
      keyed_squadrons)

    # The first join determines the next course of action
    # If one team emerges on top, restart the process for all
    # OTHER squadrons.
    if len(joined_squadrons[0]) == 1:
      results.extend(joined_squadrons[0])
      self.placed_squadrons.update(joined_squadrons[0])
      results.extend(self._break_ties_fresh_start())
    elif (len(joined_squadrons[0]) == 2 and
          len(sorted_squadrons) >= 3):
      results.extend(self._break_ties_entry_point(joined_squadrons[0]))
      self.placed_squadrons.update(joined_squadrons[0])
      results.extend(self._break_ties_fresh_start())
    else:
      results.extend(next_step(joined_squadrons[0]))
      self.placed_squadrons.update(joined_squadrons[0])
      results.extend(self._break_ties_fresh_start())

    return results
