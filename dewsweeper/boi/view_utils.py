from django.db.models import Max

from .boi_config import BASHO_ID, TOTAL_DAYS
from .boi_config import DRAFT_DIVISIONS
from .boi_config import NUM_PLAYERS
from .boi_config import SQUADRON_SIZE

from .models import BashoSquadron
from .models import Bout
from .models import DraftDivisionMember
from .models import OzumoBanzukeEntry
from .models import SquadronMember

from .scrapers import BanzukeScraper, RikishiProfileScraper

from .utils import get_basho_full_name
from .utils import get_current_basho_id


def get_current_day(basho_id=None):
  basho_id = basho_id or get_current_basho_id()

  # See if there are any scored bouts for this basho
  scored_bouts = Bout.objects.filter(
    basho_id=basho_id
  ).exclude(
    oyakata1_score__isnull=True
  )

  # If there are, then return the max scored day plus one, which is the
  # active day.
  if scored_bouts.count() > 0:
    return scored_bouts.aggregate(Max('day'))['day__max'] + 1

  # If not, check if there are any scheduled bouts for Day 1. If so, then the
  # current day is Day 1. If not, then the basho hasn't been scheduled yet.
  scheduled_bouts = Bout.objects.filter(
    basho_id=basho_id
  ).filter(
    day=1
  )
  return 1 if scheduled_bouts.count() > 0 else 0


def is_basho_active(basho_id=None):
  basho_id = basho_id or get_current_basho_id()
  current_day = get_current_day(basho_id)
  return 1 <= current_day <= TOTAL_DAYS


def create_divisions(basho_id):
  '''
  Create DraftDivisonMember objects for a basho, clearing any existing
  objects first. Currently, draft divisions are computed by "counting off"
  from the top of the banzuke to the bottom, and left to right.

  Args:
      basho_id: The basho for which to create divisions

  Returns:
      A list of the new DraftDivisionMember objects
  '''
  # Delete any pre-existing draft division member objects
  DraftDivisionMember.objects.filter(
    basho_id=basho_id,
  ).delete()

  results = []

  # Create the new draft division member objects
  new_basho_squadrons = BashoSquadron.objects.filter(
    basho_id=basho_id,
  ).order_by('rank')
  division_counter = 0
  for bs in new_basho_squadrons:
    results.append(DraftDivisionMember.objects.create(
      oyakata=bs.oyakata,
      division=division_counter + 1,
      basho_id=basho_id,
    ))
    division_counter = (division_counter + 1) % DRAFT_DIVISIONS

  return results


def get_day_from_request(request, basho_id=None, query_param='d'):
  return int(request.GET.get(query_param, get_current_day(basho_id)))


def get_basho_id_from_request(request, query_param='b'):
  return int(request.GET.get(query_param, get_current_basho_id()))


def is_suicide_pool_active(basho_id=None):
  basho_id = basho_id or get_current_basho_id()

  # If the basho has ended, return False immediately
  current_day = get_current_day(basho_id)
  if current_day > TOTAL_DAYS:
    return False

  basho_squadrons = BashoSquadron.objects.filter(
    basho_id=basho_id
  )

  num_alive = 0
  for bs in basho_squadrons:
    if bs.suicide_still_alive:
      num_alive += 1
    if num_alive > 1:
      return True

  return False


def cmp_squadrons_suicide(a, b):
  basho_id = a.basho_id
  current_day = get_current_day(basho_id)
  a_num_strikes = a.num_suicide_strikes + max(
    0, current_day - 1 - a.num_suicide_entries)
  b_num_strikes = b.num_suicide_strikes + max(
    0, current_day - 1 - b.num_suicide_entries)
  # 1. Number of strikes
  if a_num_strikes < b_num_strikes:
    return -1
  elif b_num_strikes < a_num_strikes:
    return 1
  else:
    # 2. Earlier strikes are worse
    a_picks = a.suicide_picks
    b_picks = b.suicide_picks
    for d in range(1, current_day):
      if a_picks.get(d, -3) < 0 and b_picks.get(d, -3) > 0:
        return 1
      elif b_picks.get(d, -3) < 0 and a_picks.get(d, -3) > 0:
        return -1

    # 3. Return alphabetical order of shikona
    return cmp(a.shikona, b.shikona)


def get_ozumo_banzuke(basho_id):
  banzuke_scraper = BanzukeScraper(basho_id)
  return banzuke_scraper.scrape()


def get_ozumo_results(basho_id, day):
  results_scraper = OzumoResultsScraper(basho_id, day)
  return (results_scraper.scrape(),
          results_scraper.get_bout_results_by_rikishi())


def get_rikishi_profile(rikishi_id):
  rikishi_profile_scraper = RikishiProfileScraper(rikishi_id)
  return rikishi_profile_scraper.scrape()


def get_previous_basho_squadrons(starting_basho_id):
  basho_squadrons = BashoSquadron.objects.none()
  basho_id = starting_basho_id
  while len(basho_squadrons) < 1:
    basho_squadrons = BashoSquadron.objects.filter(basho_id=basho_id)
    basho_id -= 1
    if basho_id < 450:
      break
  return basho_squadrons


def get_opponent(oyakata, basho_id, day):
  # Try oyakata1
  bout = Bout.objects.filter(
    basho_id=basho_id,
    day=day,
    oyakata1=oyakata,
  ).first()

  if bout:
    return bout.oyakata2
  else:
    # Try oyakata2
    bout = Bout.objects.filter(
      basho_id=basho_id,
      day=day,
      oyakata2=oyakata,
    ).first()

    if bout:
      return bout.oyakata1
    else:
      return None


def check_for_dewsweeper_readiness(basho_id, day):
  error_msgs = []

  if not is_basho_active(basho_id):
    error_msgs.append('The basho is not active.')

  ozumo_banzuke_entries = OzumoBanzukeEntry.objects.filter(
    basho_id=basho_id,
  )

  if ozumo_banzuke_entries.count() < 20:
    error_msgs.append('WARNING: {0} ozumo banzuke entries for basho id '
                      '{1} ({2})'.format(
      ozumo_banzuke_entries.count(), basho_id,
      get_basho_full_name(basho_id)))

  basho_squadrons = BashoSquadron.objects.filter(
    basho_id=basho_id,
  )

  if basho_squadrons.count() != NUM_PLAYERS:
    error_msgs.append('WARNING: {0} basho squadrons for '
                      '{1} players'.format(basho_squadrons.count(),
                                           NUM_PLAYERS))

  for basho_squadron in basho_squadrons:
    squadron_members = SquadronMember.objects.filter(
      oyakata=basho_squadron.oyakata,
      basho_id=basho_id,
      is_active=True,
    )
    if squadron_members.count() != SQUADRON_SIZE:
      error_msgs.append(u'WARNING: {0} has {1} squadron members'.format(
        basho_squadron.shikona,
        squadron_members.count()))

  bouts = Bout.objects.filter(
    basho_id=basho_id,
    day=day,
  )

  if bouts.count() != NUM_PLAYERS / 2:
    error_msgs.append('WARNING: {0} bouts scheduled for basho ID {1} '
                      'and day {2}'.format(bouts.count(),
                                           basho_id,
                                           day))

  return error_msgs
