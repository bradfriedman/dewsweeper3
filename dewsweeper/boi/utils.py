from .boi_config import BASHO_ID
from .boi_config import BASHO_MONTHS
from .boi_config import BASHO_NAMES
from .boi_config import BASE_PTS, CHANKO_BOUNDS
from .boi_config import SKIPPED_BASHO_IDS


def _adjust_basho_id_for_skipped_basho(basho_id):
  # If basho is after a skipped basho, add one to account for the skipped basho
  for skipped_basho_id in SKIPPED_BASHO_IDS:
    if basho_id >= skipped_basho_id:
      basho_id += 1
  return basho_id


def get_current_basho_id():
  return BASHO_ID


def get_basho_name(basho_id):
  basho_id = _adjust_basho_id_for_skipped_basho(basho_id)
  return BASHO_NAMES[(basho_id + 1) % len(BASHO_NAMES)]


def get_basho_year(basho_id):
  basho_id = _adjust_basho_id_for_skipped_basho(basho_id)
  return ((basho_id + 1) / 6) + 1920


def get_basho_month(basho_id):
  basho_id = _adjust_basho_id_for_skipped_basho(basho_id)
  return BASHO_MONTHS[(basho_id + 1) % len(BASHO_NAMES)]


def convert_basho_id_to_sumoref_id(basho_id):
  return '%s%s' % (get_basho_year(basho_id), get_basho_month(basho_id))


def get_basho_ids_in_month(month):
  '''
  NOTE: Month should be an integer between 0-5 inclusive
        (0 - Hatsu, 1 - Haru, 2 - Natsu, 3 - Nagoya, 4 - Aki, 5 - Kyushu)
  '''
  # Basho ids before the 2011 year with the skipped Haru 2011 (id 546) basho
  results = range(527 + month, 545, 6)

  # Basho ids in 2011
  if month == 0:
    results.append(545)
  elif month != 1:
    results.append(545 + month - 1)

  # Basho ids between 2012 and 2019
  results.extend(range(550 + month, 598, 6))

  # Basho ids in 2020
  if month < 2:
    results.append(598 + month)
  elif month != 3:
    results.append(598 + month - 1)

  results.extend(range(603 + month, 2000, 6))

  return results


def get_basho_ids_in_year(year):
  if year < 2011:
    first_basho_id = 479 + (6 * (year - 2000))
  # Special case: 2011 was basho ids 545-549 (Haru 2011 was skipped)
  elif year == 2011:
    return range(545, 550)
  elif year < 2020:
    first_basho_id = 550 + (6 * (year - 2012))
  # Special case: 2020 was basho ids 598-602 (Natsu 2020 was skipped)
  elif year == 2020:
    return range(598, 603)
  else:
    first_basho_id = 603 + (6 * (year - 2021))

  # Return the first basho of the year through the last basho of the year,
  # which is 5 basho later.
  # (range is exclusive of the upper bound, so we add 6, not just 5)
  return range(first_basho_id, first_basho_id + 6)


def get_basho_full_name(basho_id):
  return '{0} {1}'.format(get_basho_name(basho_id), get_basho_year(basho_id))


def convert_rank_to_full_string(rank):
  if rank == -3:
    return 'Yokozuna'
  elif rank == -2:
    return 'Ozeki'
  elif rank == -1:
    return 'Sekiwake'
  elif rank == 0:
    return 'Komusubi'
  elif rank <= 19:
    return 'Maegashira-{0}'.format(rank)
  elif 1001 <= rank < 2000:
    return 'Juryo-{0}'.format(str(rank - 1000))
  elif 2001 <= rank < 3000:
    return 'Makushita-{0}'.format(str(rank - 2000))
  elif 3001 <= rank < 4000:
    return 'Sandanme-{0}'.format(str(rank - 3000))
  elif 4001 <= rank < 5000:
    return 'Jonidan-{0}'.format(str(rank - 4000))
  elif 5001 <= rank < 6000:
    return 'Jonokuchi-{0}'.format(str(rank - 5000))
  else:
    return 'Other'


def convert_rank_to_short_string(rank):
  if rank == -3:
    return 'Y'
  elif rank == -2:
    return 'O'
  elif rank == -1:
    return 'S'
  elif rank == 0:
    return 'K'
  elif rank <= 19:
    return 'M{0}'.format(rank)
  elif 1001 <= rank < 2000:
    return 'J{0}'.format(str(rank - 1000))
  elif 2001 <= rank < 3000:
    return 'Ms{0}'.format(str(rank - 2000))
  elif 3001 <= rank < 4000:
    return 'Sd{0}'.format(str(rank - 3000))
  elif 4001 <= rank < 5000:
    return 'Jd{0}'.format(str(rank - 4000))
  elif 5001 <= rank < 6000:
    return 'Jk{0}'.format(str(rank - 5000))
  else:
    return '?'


def convert_rank_string_to_rank(rank):
  if rank == 'Y' or rank == 'YO':
    return -3
  elif rank == 'O':
    return -2
  elif rank == 'S':
    return -1
  elif rank == 'K':
    return 0
  elif rank.startswith('M') and rank[1].isdigit():
    return int(rank[1:])
  elif rank.startswith('J'):
    return 1000 + int(rank[1:])
  elif rank.startswith('Ms'):
    return 2000 + int(rank[2:])
  elif rank.startswith('Sd'):
    return 3000 + int(rank[2:])
  elif rank.startswith('Jd'):
    return 4000 + int(rank[2:])
  elif rank.startswith('Jk'):
    return 5000 + int(rank[2:])
  else:
    raise Exception('Invalid rank %s', rank)


def calculate_score(winning_rank, losing_rank, kimarite=None):
  def _chanko_bound(r):
    for n, b in enumerate(CHANKO_BOUNDS):
      if r < b:
        return n
    return len(CHANKO_BOUNDS)

  # For fusensho, no chanko is awarded
  if kimarite == 'fusen':
    return BASE_PTS

  return (BASE_PTS +
          max(0, _chanko_bound(winning_rank) - _chanko_bound(losing_rank)))


def fuzzy_oyakata_lookup(oyakata):
  oyakata = oyakata.strip().lower()

  # Brad
  if oyakata in ['brad', 'friedman']:
    return 'Brad'
  # Erik
  elif oyakata in ['erik', 'williamson'] or 'dishy' in oyakata:
    return 'Erik'
  # Rob
  elif oyakata in ['rob', 'berger']:
    return 'Rob'
  # Kratville
  elif oyakata in ['eryck', 'kratville'] or oyakata.startswith('krat'):
    return 'Kratville'
  # Stan
  elif oyakata in ['stan', 'will', 'morton', 'willstan']:
    return 'Stan'
  # Tron
  elif oyakata in ['tron', 'stanton'] or oyakata.startswith('matt s'):
    return 'Tron'
  # Dave Ritt
  elif oyakata in ['dave', 'ritt'] or oyakata.endswith('ritt'):
    return 'Dave'
  # Textor
  elif oyakata in ['cindi', 'textor'] or oyakata.startswith('tex'):
    return 'Textor'
  # Ben
  elif oyakata == 'ben' or oyakata.endswith('elias'):
    return 'Ben'
  # Monkey
  elif oyakata in ['monkey', 'makino']:
    return 'The Monkey'
  # Matt Rich
  elif oyakata in ['matt rich', 'rich'] or oyakata.startswith('matt r'):
    return 'Matt Rich'
  # Sarah
  elif oyakata in ['sarah', 'unger', 'sunger', 'smunger']:
    return 'Sunger'
  # Pete
  elif oyakata.startswith('pete'):
    return 'Pete'
  else:
    return None
