from django.db import transaction

from .emailers import WaiversResultsEmailer

from .exceptions import BoiWaiversException

from .models import BashoSquadron
from .models import Oyakata
from .models import OzumoBanzukeEntry
from .models import SquadronMember

from .utils import fuzzy_oyakata_lookup


class WaiversResult(object):

  def __init__(self, basho_squadron_obj, waived_squadron_member,
               added_squadron_member):
    self.basho_squadron_obj = basho_squadron_obj
    self.waived_squadron_member = waived_squadron_member
    self.added_squadron_member = added_squadron_member

  def get_oyakata_shikona(self):
    return self.basho_squadron_obj.shikona

  def get_waived_rikishi_shikona(self):
    return self.waived_squadron_member.rikishi.shikona_for_basho(
      self.basho_squadron_obj.basho_id
    )

  def get_added_rikishi_shikona(self):
    return self.added_squadron_member.rikishi.shikona_for_basho(
      self.basho_squadron_obj.basho_id
    )


class WaiversProcessor(object):

  def __init__(self, basho_id, send_email):
    self.basho_id = basho_id
    self.send_email = send_email

  @transaction.atomic
  def process_waivers(self, waivers):
    processed_waivers = []
    lines = waivers.strip().split('\n')

    # Read the waivers results and create/update squadron members
    for waivers_line in lines:
      if not waivers_line or waivers_line.isspace():
        processed_waivers.append(None)
        continue

      tokens = [t.strip() for t in waivers_line.split(',')]
      if len(tokens) != 3:
        raise BoiWaiversException(
          'Invalid waivers line: {0}'.format(waivers_line))

      # Get BashoSquadron object for this waivers result
      basho_squadron_obj = self._get_basho_squadron(tokens[0])
      waived_squadron_member = self._get_waived_squadron_member(
        tokens[1], basho_squadron_obj)
      added_rikishi = self._get_added_rikishi(tokens[2])

      # Update waived SquadronMember with is_active=False
      waived_squadron_member.is_active = False
      waived_squadron_member.save()

      # Add a new SquadronMember for the added rikishi
      new_squadron_member = SquadronMember.objects.create(
        oyakata=basho_squadron_obj.oyakata,
        rikishi=added_rikishi,
        basho_id=self.basho_id,
        selection_order=basho_squadron_obj.num_squadron_selections + 1,
        is_active=True,
      )

      processed_waivers.append(WaiversResult(
        basho_squadron_obj,
        waived_squadron_member,
        new_squadron_member,
      ))

    if self.send_email:
      waivers_results_emailer = WaiversResultsEmailer(
        processed_waivers, self.basho_id)
      waivers_results_emailer.send_email()

    return processed_waivers

  def _get_basho_squadron(self, token):
    # First, try to fuzzy lookup the oyakata by short name (e.g., 'Erik')
    short_name = fuzzy_oyakata_lookup(token)
    if short_name:
      oyakata_obj = Oyakata.objects.get(short_name=short_name)
      return BashoSquadron.objects.get(
        basho_id=self.basho_id,
        oyakata=oyakata_obj,
      )

    # Then, try to match the shikona directly
    basho_squadron_obj = BashoSquadron.objects.filter(
      basho_id=self.basho_id,
      shikona=token,
    ).first()
    if basho_squadron_obj:
      return basho_squadron_obj

    # Otherwise, just give up and throw an exception
    raise BoiWaiversException(
      "Couldn't find an oyakata for {0}".format(token))

  def _get_waived_squadron_member(self, token, basho_squadron):
    ozumo_banzuke_entry = OzumoBanzukeEntry.objects.filter(
      basho_id=self.basho_id,
      shikona=token,
    ).first()
    if not ozumo_banzuke_entry:
      raise BoiWaiversException(
        '{0} not found in this basho'.format(token))

    squadron_member_obj = SquadronMember.objects.filter(
      basho_id=self.basho_id,
      oyakata=basho_squadron.oyakata,
      rikishi=ozumo_banzuke_entry.rikishi,
      is_active=True,
    ).first()
    if not squadron_member_obj:
      raise BoiWaiversException(
        "{0} not a member of {1}'s squadron.".format(
          token, basho_squadron.shikona))

    return squadron_member_obj

  def _get_added_rikishi(self, token):
    ozumo_banzuke_entry = OzumoBanzukeEntry.objects.filter(
      basho_id=self.basho_id,
      shikona=token,
    ).first()
    if not ozumo_banzuke_entry:
      raise BoiWaiversException(
        '{0} not found in this basho'.format(token))

    return ozumo_banzuke_entry.rikishi
