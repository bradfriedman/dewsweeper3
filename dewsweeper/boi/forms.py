from django import core
from django import forms

from .boi_config import LINEUP_SIZE, TOTAL_DAYS
from .models import OzumoBanzukeEntry
from .utils import get_current_basho_id


class StartingRikishiField(forms.MultipleChoiceField):
  def validate(self, value):
    if len(value) != LINEUP_SIZE:
      raise core.exceptions.ValidationError(
        'Please select %s rikishi.' % LINEUP_SIZE)

    super(StartingRikishiField, self).validate(value)


class ActiveDayField(forms.TypedChoiceField):
  def validate(self, value):
    super(ActiveDayField, self).validate(value)
    if int(value) < 1 or int(value) > TOTAL_DAYS:
      raise core.exceptions.ValidationError(
        'Please select a valid day. (%s, %s)' % (value, type(value))
      )


class SquadronManagementForm(forms.Form):
  pre_trash_talk = forms.CharField(widget=forms.Textarea,
                                   required=False)
  starting_rikishi = StartingRikishiField(
    widget=forms.CheckboxSelectMultiple,
  )
  suicide_pick = forms.ModelChoiceField(queryset=None, empty_label=None,
                                        required=False)
  post_trash_talk = forms.CharField(widget=forms.Textarea,
                                    required=False)
  active_day = ActiveDayField(coerce=int, empty_value=0)

  def __init__(self, *args, **kwargs):
    rikishi = kwargs.pop('rikishi')
    prev_suicide_picks = kwargs.pop('prev_suicide_picks', [])
    suicide_pool_active = kwargs.pop('suicide_pool_active', False)
    day = kwargs.pop('day')
    basho_id = get_current_basho_id()
    self.base_fields['starting_rikishi'].empty_label = None
    super(SquadronManagementForm, self).__init__(*args, **kwargs)

    self.fields['starting_rikishi'].choices = [
      (int(r.rikishi.id), r.rikishi.shikona_for_basho(basho_id))
      for r in rikishi
    ]

    self.fields['suicide_pick'].queryset = OzumoBanzukeEntry.objects.filter(
      basho_id=basho_id
    ).exclude(
      rikishi_id__in=prev_suicide_picks
    ).order_by('rank')

    if not suicide_pool_active:
      self.fields['suicide_pick'].disabled = True

    self.fields['active_day'].choices = [
      (d, d) for d in xrange(max(day, 1), TOTAL_DAYS + 1)
    ]

    self.fields['pre_trash_talk'].widget.attrs['rows'] = 10
    self.fields['pre_trash_talk'].widget.attrs['cols'] = 80
    self.fields['post_trash_talk'].widget.attrs['rows'] = 10
    self.fields['post_trash_talk'].widget.attrs['cols'] = 80


class RegisterPlayersForm(forms.Form):

  def __init__(self, *args, **kwargs):
    old_basho_squadrons = kwargs.pop('old_basho_squadrons', [])

    super(RegisterPlayersForm, self).__init__(*args, **kwargs)

    for i, squadron in enumerate(old_basho_squadrons):
      field_name = 'player{i}'.format(i=i)
      self.fields[field_name] = forms.CharField(
        label=squadron.oyakata.real_name,
        initial=squadron.shikona,
        min_length=1,
        max_length=100,
      )
      self.fields[field_name].widget.attrs['size'] = 50


class MonkeyListForm(forms.Form):
  partial_draft_results = forms.CharField(
    widget=forms.Textarea, required=True)

  def __init__(self, *args, **kwargs):
    super(MonkeyListForm, self).__init__(*args, **kwargs)

    self.fields['partial_draft_results'].widget.attrs['rows'] = 25
    self.fields['partial_draft_results'].widget.attrs['cols'] = 80


class DraftRegistrationForm(forms.Form):
  draft_results = forms.CharField(widget=forms.Textarea, required=True)

  def __init__(self, *args, **kwargs):
    super(DraftRegistrationForm, self).__init__(*args, **kwargs)

    self.fields['draft_results'].widget.attrs['rows'] = 25
    self.fields['draft_results'].widget.attrs['cols'] = 80


class ScheduleRegistrationForm(forms.Form):
  schedule = forms.CharField(widget=forms.Textarea, required=True)

  def __init__(self, *args, **kwargs):
    super(ScheduleRegistrationForm, self).__init__(*args, **kwargs)

    self.fields['schedule'].widget.attrs['rows'] = 40
    self.fields['schedule'].widget.attrs['cols'] = 80


class WaiversForm(forms.Form):
  waivers = forms.CharField(widget=forms.Textarea, required=True)

  def __init__(self, *args, **kwargs):
    super(WaiversForm, self).__init__(*args, **kwargs)

    self.fields['waivers'].widget.attrs['rows'] = 20
    self.fields['waivers'].widget.attrs['cols'] = 60
