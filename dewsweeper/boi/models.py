# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.staticfiles.templatetags.staticfiles import static
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.db.models import F, Max, Sum
from django.utils.encoding import python_2_unicode_compatible
from django.utils.functional import cached_property

from .boi_config import (DAY_ONE_WINNING_STR, DAY_ONES, DAY_TWOS, HIKIWAKE_STR,
                         TIEGAME_STR, TIEBREAKER_STR, WINNING_STR)
from .boi_config import LINEUP_SIZE
from .boi_config import LAST_DAY_OF_REG_SEASON
from .boi_config import SUICIDE_LAST_STRIKE

from .exceptions import BoiNoDayOneScoreException

from .utils import convert_rank_to_full_string, convert_rank_to_short_string
from .utils import get_basho_full_name
from .utils import get_current_basho_id


@python_2_unicode_compatible
class Kimarite(models.Model):
  class Meta:
    verbose_name_plural = "kimarite"

  name = models.CharField(max_length=20, unique=True)

  def __str__(self):
    return self.name


@python_2_unicode_compatible
class Rikishi(models.Model):
  class Meta:
    verbose_name_plural = "rikishi"

  real_name = models.CharField(max_length=50, unique=True)
  dob = models.DateField()
  web_id = models.CharField(max_length=20)

  @cached_property
  def most_recent_shikona(self):
    obj_result = self.ozumobanzukeentry_set.order_by('-basho_id').first()

    if obj_result:
      return obj_result.shikona
    else:
      return self.real_name

  def shikona_for_basho(self, basho_id):
    obj_result = self.ozumobanzukeentry_set.filter(
      basho_id=basho_id
    ).first()

    if obj_result:
      return obj_result.shikona
    else:
      return self.real_name

  def rank_for_basho(self, basho_id):
    return self.ozumobanzukeentry_set.get(
      basho_id=basho_id
    ).rank

  def __str__(self):
    return self.most_recent_shikona or self.real_name


@python_2_unicode_compatible
class Oyakata(models.Model):
  class Meta:
    verbose_name_plural = "oyakata"

  first_name = models.CharField(max_length=25)
  last_name = models.CharField(max_length=25)
  short_name = models.CharField(max_length=25, unique=True)
  rank = models.SmallIntegerField()
  style = models.CharField(max_length=25)
  quote = models.CharField(max_length=300)
  email = models.EmailField(default='')
  image_path = models.CharField(max_length=50)
  is_kadoban = models.BooleanField(default=False)
  is_repromotion_eligible = models.BooleanField(default=False)
  is_tsunatori = models.BooleanField(default=False)
  is_jun_tsunatori = models.BooleanField(default=False)

  @property
  def real_name(self):
    return '{0} {1}'.format(self.first_name, self.last_name)

  @property
  def rank_full_str(self):
    return convert_rank_to_full_string(self.rank)

  @property
  def rank_short_str(self):
    return convert_rank_to_short_string(self.rank)

  @property
  def image_url(self):
    return static(self.image_path)

  @cached_property
  def most_recent_shikona(self):
    return self.basho_squadron_set.order_by(
      '-basho_id'
    ).first().shikona

  @cached_property
  def shikona_for_current_basho(self):
    basho_id = get_current_basho_id()
    basho_squadron = self.basho_squadron_set.filter(
      basho_id=basho_id
    )
    if basho_squadron:
      return basho_squadron.first().shikona
    else:
      return ''

  @cached_property
  def shikona_history(self):
    shikona_set = [s['shikona'] for s in
                   self.basho_squadron_set.order_by(
                     'basho_id'
                   ).values('shikona')]
    result = []
    last_shikona = None
    for s in shikona_set:
      if s != last_shikona:
        result.append(s)
        last_shikona = s
    return result

  def shikona_for_basho(self, basho_id):
    return self.basho_squadron_set.get(
      basho_id=basho_id
    ).shikona

  def rank_for_basho(self, basho_id):
    return self.basho_squadron_set.get(
      basho_id=basho_id
    ).rank

  def basho_finishes_at_place(self, place):
    return self.basho_squadron_set.filter(
      place=place,
    ).count()

  @cached_property
  def yusho(self):
    return self.basho_finishes_at_place(1)

  @cached_property
  def jun_yusho(self):
    return self.basho_finishes_at_place(2)

  @cached_property
  def lifetime_wins(self):
    return Bout.objects.filter(
      oyakata1=self,
      day__lte=LAST_DAY_OF_REG_SEASON,
      oyakata1_score__gt=F('oyakata2_score'),
    ).count() + Bout.objects.filter(
      oyakata2=self,
      day__lte=LAST_DAY_OF_REG_SEASON,
      oyakata2_score__gt=F('oyakata1_score')
    ).count()

  @cached_property
  def lifetime_losses(self):
    return Bout.objects.filter(
      oyakata1=self,
      day__lte=LAST_DAY_OF_REG_SEASON,
      oyakata1_score__lt=F('oyakata2_score'),
    ).count() + Bout.objects.filter(
      oyakata2=self,
      day__lte=LAST_DAY_OF_REG_SEASON,
      oyakata2_score__lt=F('oyakata1_score')
    ).count()

  @cached_property
  def lifetime_draws(self):
    return Bout.objects.filter(
      oyakata1=self,
      day__lte=LAST_DAY_OF_REG_SEASON,
      oyakata1_score=F('oyakata2_score'),
    ).count() + Bout.objects.filter(
      oyakata2=self,
      day__lte=LAST_DAY_OF_REG_SEASON,
      oyakata2_score=F('oyakata1_score')
    ).count()

  @cached_property
  def lifetime_bouts(self):
    return self.lifetime_wins + self.lifetime_losses + self.lifetime_draws

  @cached_property
  def lifetime_record_string(self):
    return '{wins}-{losses}-{draws}'.format(
      wins=self.lifetime_wins,
      losses=self.lifetime_losses,
      draws=self.lifetime_draws,
    )

  @cached_property
  def lifetime_winning_percentage(self):
    return ((float(self.lifetime_wins) + (0.5 * self.lifetime_draws)) /
            float(self.lifetime_bouts))

  def __str__(self):
    return self.real_name


class Bout(models.Model):
  class Meta:
    unique_together = (('oyakata1', 'oyakata2', 'basho_id', 'day'),)

  oyakata1 = models.ForeignKey(Oyakata, on_delete=models.PROTECT,
                               related_name='o1_result',
                               verbose_name='oyakata 1')
  oyakata1_score = models.PositiveSmallIntegerField(
    verbose_name="oyakata 1's score", null=True, blank=True)
  oyakata1_chanko = models.PositiveSmallIntegerField(
    verbose_name="oyakata 1's chanko points", null=True, blank=True)
  oyakata1_bp = models.PositiveSmallIntegerField(
    verbose_name="oyakata 1's best possible score", null=True, blank=True)
  oyakata2 = models.ForeignKey(Oyakata, on_delete=models.PROTECT,
                               related_name='o2_result',
                               verbose_name='oyakata 2')
  oyakata2_score = models.PositiveSmallIntegerField(
    verbose_name="oyakata 2's score", null=True, blank=True)
  oyakata2_chanko = models.PositiveSmallIntegerField(
    verbose_name="oyakata 2's chanko points", null=True, blank=True)
  oyakata2_bp = models.PositiveSmallIntegerField(
    verbose_name="oyakata 2's best possible score", null=True, blank=True)
  basho_id = models.PositiveSmallIntegerField()
  day = models.PositiveSmallIntegerField()
  order = models.PositiveSmallIntegerField()

  @property
  def is_day_two(self):
    return self.day in DAY_TWOS

  @property
  def oyakata1_result(self):
    return {
      'oyakata': self.oyakata1,
      'score': self.oyakata1_score,
      'chanko': self.oyakata1_chanko,
      'bp': self.oyakata1_bp,
      'day_one_score': self.oyakata1_day_one_score if self.day in DAY_TWOS
      else 0
    }

  @property
  def oyakata2_result(self):
    return {
      'oyakata': self.oyakata2,
      'score': self.oyakata2_score,
      'chanko': self.oyakata2_chanko,
      'bp': self.oyakata2_bp,
      'day_one_score': self.oyakata2_day_one_score if self.day in DAY_TWOS
      else 0
    }

  def _oyakata_shikona(self, oyakata):
    return oyakata.basho_squadron_set.filter(
      basho_id=self.basho_id
    ).get().shikona

  @cached_property
  def oyakata1_shikona(self):
    return self._oyakata_shikona(self.oyakata1)

  @cached_property
  def oyakata2_shikona(self):
    return self._oyakata_shikona(self.oyakata2)

  @cached_property
  def oyakata1_rank_short_str(self):
    bs = BashoSquadron.objects.get(
      oyakata=self.oyakata1,
      basho_id=self.basho_id,
    )
    return convert_rank_to_short_string(bs.rank)

  @cached_property
  def oyakata2_rank_short_str(self):
    bs = BashoSquadron.objects.get(
      oyakata=self.oyakata2,
      basho_id=self.basho_id,
    )
    return convert_rank_to_short_string(bs.rank)

  def _oyakata_basho_record(self, oyakata):
    return {
      'wins': oyakata.basho_squadron_set.filter(
        basho_id=self.basho_id
      ).get().wins,
      'losses': oyakata.basho_squadron_set.filter(
        basho_id=self.basho_id
      ).get().losses,
      'draws': oyakata.basho_squadron_set.filter(
        basho_id=self.basho_id
      ).get().draws,
    }

  @property
  def oyakata1_basho_record(self):
    return self._oyakata_basho_record(self.oyakata1)

  @property
  def oyakata2_basho_record(self):
    return self._oyakata_basho_record(self.oyakata2)

  @property
  def oyakata1_day_one_score(self):
    day_one_bout = Bout.objects.filter(
      oyakata1=self.oyakata1,
      oyakata2=self.oyakata2,
      basho_id=self.basho_id,
      day=self.day - 1,
      oyakata1_score__isnull=False,
      oyakata2_score__isnull=False,
    ).first()
    if not day_one_bout:
      raise BoiNoDayOneScoreException(
        'No day one score for {o1} vs. {o2}'.format(
          o1=self.oyakata1, o2=self.oyakata2))
    else:
      return day_one_bout.oyakata1_score

  @property
  def oyakata2_day_one_score(self):
    day_one_bout = Bout.objects.filter(
      oyakata1=self.oyakata1,
      oyakata2=self.oyakata2,
      basho_id=self.basho_id,
      day=self.day - 1,
      oyakata1_score__isnull=False,
      oyakata2_score__isnull=False,
    ).first()
    if not day_one_bout:
      raise BoiNoDayOneScoreException(
        'No day one score for {o1} vs. {o2}'.format(
          o1=self.oyakata1, o2=self.oyakata2))
    else:
      return day_one_bout.oyakata2_score

  @property
  def winning_oyakata_day_one_score(self):
    winner = self.winner
    if winner > 0:
      return self.oyakata1_day_one_score
    elif winner < 0:
      return self.oyakata2_day_one_score
    else:
      return None

  @property
  def losing_oyakata_day_one_score(self):
    winner = self.winner
    if winner > 0:
      return self.oyakata2_day_one_score
    elif winner < 0:
      return self.oyakata1_day_one_score
    else:
      return None

  @cached_property
  def oyakata1_total_score(self):
    o1_score = self.oyakata1_score
    if self.day in DAY_TWOS:
      o1_score += self.oyakata1_day_one_score
    return o1_score

  @cached_property
  def oyakata2_total_score(self):
    o2_score = self.oyakata2_score
    if self.day in DAY_TWOS:
      o2_score += self.oyakata2_day_one_score
    return o2_score

  @property
  def winner(self):
    '''Returns 1 if oyakata1 won, -1 if oyakata2 won, and 0 if a draw.'''
    second_day = self.day in DAY_TWOS
    o1_score = self.oyakata1_score
    o2_score = self.oyakata2_score
    if second_day:
      o1_score += self.oyakata1_day_one_score
      o2_score += self.oyakata2_day_one_score

      # Tiebreaker on second day
      return 1 if o1_score >= o2_score else -1

    if o1_score > o2_score:
      return 1
    elif o1_score < o2_score:
      return -1
    else:
      return 0

  @property
  def winning_oyakata(self):
    winner = self.winner
    if winner > 0:
      return self.oyakata1
    elif winner < 0:
      return self.oyakata2
    else:
      return None

  @property
  def losing_oyakata(self):
    winner = self.winner
    if winner > 0:
      return self.oyakata2
    elif winner < 0:
      return self.oyakata1
    else:
      return None

  @property
  def winning_result(self):
    if self.winner >= 0:
      return self.oyakata1_result
    else:
      return self.oyakata2_result

  @property
  def losing_result(self):
    if self.winner >= 0:
      return self.oyakata2_result
    else:
      return self.oyakata1_result

  @property
  def oyakata1_basho_squadron(self):
    return BashoSquadron.objects.get(
      basho_id=self.basho_id,
      oyakata=self.oyakata1,
    )

  @property
  def oyakata2_basho_squadron(self):
    return BashoSquadron.objects.get(
      basho_id=self.basho_id,
      oyakata=self.oyakata2,
    )

  @property
  def winning_basho_squadron(self):
    if self.winner >= 0:
      return self.oyakata1_basho_squadron
    else:
      return self.oyakata2_basho_squadron

  @property
  def losing_basho_squadron(self):
    if self.winner >= 0:
      return self.oyakata2_basho_squadron
    else:
      return self.oyakata1_basho_squadron

  def _oyakata_lineup(self, oyakata):
    return LineupEntry.objects.filter(
      basho_id=self.basho_id
    ).filter(
      day=self.day
    ).filter(
      oyakata=oyakata
    ).order_by('order')

  @property
  def oyakata1_lineup(self):
    return self._oyakata_lineup(self.oyakata1)

  @property
  def oyakata2_lineup(self):
    return self._oyakata_lineup(self.oyakata2)

  def _oyakata_effective_lineup(self, oyakata):
    cur_day = self.day
    for cur_day in range(self.day, 0, -1):
      cur_lineup = LineupEntry.objects.filter(
        basho_id=self.basho_id,
        day=cur_day,
        oyakata=oyakata,
      ).order_by('order')
      if cur_lineup:
        return cur_lineup

    # No lineups set this basho, so return first draft selections
    return SquadronMember.objects.filter(
      basho_id=self.basho_id,
      oyakata=oyakata,
      is_active=True,
    ).order_by('selection_order')[:LINEUP_SIZE]

  @property
  def oyakata1_effective_lineup(self):
    return self._oyakata_effective_lineup(self.oyakata1)

  @property
  def oyakata2_effective_lineup(self):
    return self._oyakata_effective_lineup(self.oyakata2)

  def _oyakata_squadron(self, oyakata):
    return SquadronMember.objects.filter(
      basho_id=self.basho_id,
      oyakata=oyakata,
      is_active=True,
    ).order_by('selection_order')

  @property
  def oyakata1_squadron(self):
    return self._oyakata_squadron(self.oyakata1)

  @property
  def oyakata2_squadron(self):
    return self._oyakata_squadron(self.oyakata2)

  @property
  def winning_lineup(self):
    if self.winner >= 0:
      return self.oyakata1_effective_lineup
    else:
      return self.oyakata2_effective_lineup

  @property
  def losing_lineup(self):
    if self.winner >= 0:
      return self.oyakata2_effective_lineup
    else:
      return self.oyakata1_effective_lineup

  def get_rikishi_value(self, rikishi):
    try:
      result = OzumoBout.objects.filter(
        basho_id=self.basho_id
      ).filter(
        day=self.day
      ).filter(
        winning_rikishi=rikishi
      ).get()
      return result.value
    except ObjectDoesNotExist:
      # No winning entry for rikishi, so assume a loss and return 0 pts
      return 0

  @property
  def is_playoff_tiebreaker(self):
    return (self.is_day_two and
            self.oyakata1_total_score == self.oyakata2_total_score)

  @property
  def result_string(self):
    if (self.oyakata1_total_score > self.oyakata2_total_score or
        (self.oyakata1_total_score >= self.oyakata2_total_score and
         self.day in DAY_TWOS)):
      if self.day in DAY_ONES:
        return DAY_ONE_WINNING_STR.format(
          self.oyakata1.shikona_for_basho(self.basho_id),
          self.oyakata2.shikona_for_basho(self.basho_id),
          self.oyakata1_total_score,
          self.oyakata2_total_score,
        )
      else:
        result = WINNING_STR.format(
          self.oyakata1.shikona_for_basho(self.basho_id),
          self.oyakata1_total_score,
          self.oyakata2_total_score
        )
        if self.is_playoff_tiebreaker:
          result += '\n' + TIEBREAKER_STR
        return result
    elif (self.oyakata2_total_score > self.oyakata1_total_score or
          (self.oyakata2_total_score >= self.oyakata1_total_score and
           self.day in DAY_TWOS)):
      if self.day in DAY_ONES:
        return DAY_ONE_WINNING_STR.format(
          self.oyakata2.shikona_for_basho(self.basho_id),
          self.oyakata1.shikona_for_basho(self.basho_id),
          self.oyakata2_total_score,
          self.oyakata1_total_score,
        )
      else:
        result = WINNING_STR.format(
          self.oyakata2.shikona_for_basho(self.basho_id),
          self.oyakata2_total_score,
          self.oyakata1_total_score,
        )
        if self.is_playoff_tiebreaker:
          result += '\n' + TIEBREAKER_STR
        return result
    else:
      # Tie game or hikiwake
      if self.day in DAY_ONES:
        return TIEGAME_STR.format(
          self.oyakata1_total_score,
          self.oyakata2_total_score,
        )
      else:
        return HIKIWAKE_STR.format(
          self.oyakata1_total_score,
          self.oyakata2_total_score,
        )

  def __str__(self):
    return unicode('{o1} vs. {o2} - {basho} Day {day}'.format(
      o1=self.oyakata1.shikona_for_basho(self.basho_id),
      o2=self.oyakata2.shikona_for_basho(self.basho_id),
      basho=get_basho_full_name(self.basho_id),
      day=self.day,
    )).encode('utf-8')


class LineupEntry(models.Model):
  class Meta:
    verbose_name_plural = 'lineup entries'

  oyakata = models.ForeignKey(Oyakata, on_delete=models.PROTECT)
  rikishi = models.ForeignKey(Rikishi, on_delete=models.PROTECT)
  basho_id = models.PositiveSmallIntegerField()
  day = models.PositiveSmallIntegerField()
  order = models.PositiveSmallIntegerField()


@python_2_unicode_compatible
class SuicidePoolEntry(models.Model):
  class Meta:
    verbose_name = 'suicide pick'

  oyakata = models.ForeignKey(Oyakata, on_delete=models.PROTECT)
  rikishi = models.ForeignKey(Rikishi, on_delete=models.PROTECT)
  basho_id = models.PositiveSmallIntegerField()
  day = models.PositiveSmallIntegerField()

  @cached_property
  def did_win(self):
    # Check to see if there's an ozumo bout with the rikishi as the winner
    winning_bout = OzumoBout.objects.filter(
      basho_id=self.basho_id,
      day=self.day,
      winning_rikishi=self.rikishi,
    )
    if winning_bout:
      # Wins by fusen count as losses in suicide pool
      return -2 if winning_bout.first().kimarite == 'fusen' else 1

    # If no winning bout, check for losing bout
    losing_bout = OzumoBout.objects.filter(
      basho_id=self.basho_id,
      day=self.day,
      losing_rikishi=self.rikishi,
    )

    # If a losing bout, return a loss code, else return no bout code
    return -1 if losing_bout else 0

  def __str__(self):
    return '{short_name} picks {rikishi}, {basho} Day {day}'.format(
      short_name=self.oyakata.short_name,
      rikishi=self.rikishi.shikona_for_basho(self.basho_id),
      basho=get_basho_full_name(self.basho_id),
      day=self.day,
    )


@python_2_unicode_compatible
class BashoSquadron(models.Model):
  class Meta:
    unique_together = (('oyakata', 'basho_id'),)

  oyakata = models.ForeignKey(Oyakata, on_delete=models.PROTECT,
                              related_name='basho_squadron_set')
  basho_id = models.PositiveSmallIntegerField()
  shikona = models.CharField(max_length=100)
  rank = models.SmallIntegerField()
  place = models.PositiveSmallIntegerField(null=True, blank=True)
  suicide_place = models.PositiveSmallIntegerField(null=True, blank=True)
  suicide_num_strikes = models.PositiveSmallIntegerField(null=True,
                                                         blank=True)
  suicide_elim_day = models.PositiveSmallIntegerField(null=True, blank=True)

  @property
  def wins(self):
    return Bout.objects.filter(
      basho_id=self.basho_id
    ).filter(
      oyakata1=self.oyakata
    ).filter(
      day__lte=LAST_DAY_OF_REG_SEASON
    ).filter(
      oyakata1_score__gt=F('oyakata2_score')
    ).count() + Bout.objects.filter(
      basho_id=self.basho_id
    ).filter(
      oyakata2=self.oyakata
    ).filter(
      day__lte=LAST_DAY_OF_REG_SEASON
    ).filter(
      oyakata2_score__gt=F('oyakata1_score')
    ).count()

  @property
  def losses(self):
    return Bout.objects.filter(
      basho_id=self.basho_id
    ).filter(
      oyakata1=self.oyakata
    ).filter(
      day__lte=LAST_DAY_OF_REG_SEASON
    ).filter(
      oyakata1_score__lt=F('oyakata2_score')
    ).count() + Bout.objects.filter(
      basho_id=self.basho_id
    ).filter(
      oyakata2=self.oyakata
    ).filter(
      day__lte=LAST_DAY_OF_REG_SEASON
    ).filter(
      oyakata2_score__lt=F('oyakata1_score')
    ).count()

  @property
  def draws(self):
    return Bout.objects.filter(
      basho_id=self.basho_id
    ).filter(
      oyakata1=self.oyakata
    ).filter(
      day__lte=LAST_DAY_OF_REG_SEASON
    ).filter(
      oyakata1_score=F('oyakata2_score')
    ).count() + Bout.objects.filter(
      basho_id=self.basho_id
    ).filter(
      oyakata2=self.oyakata
    ).filter(
      day__lte=LAST_DAY_OF_REG_SEASON
    ).filter(
      oyakata2_score=F('oyakata1_score')
    ).count()

  @property
  def record_string(self):
    return '{wins}-{losses}-{draws}'.format(
      wins=self.wins,
      losses=self.losses,
      draws=self.draws,
    )

  @cached_property
  def total_points(self):
    return (Bout.objects.filter(
      basho_id=self.basho_id
    ).filter(
      oyakata1=self.oyakata
    ).aggregate(p=Sum('oyakata1_score'))['p'] or 0) + (Bout.objects.filter(
      basho_id=self.basho_id
    ).filter(
      oyakata2=self.oyakata
    ).aggregate(p=Sum('oyakata2_score'))['p'] or 0)

  @cached_property
  def total_chanko(self):
    return (Bout.objects.filter(
      basho_id=self.basho_id
    ).filter(
      oyakata1=self.oyakata
    ).aggregate(p=Sum('oyakata1_chanko'))['p'] or 0) + (Bout.objects.filter(
      basho_id=self.basho_id
    ).filter(
      oyakata2=self.oyakata
    ).aggregate(p=Sum('oyakata2_chanko'))['p'] or 0)

  @cached_property
  def points_against(self):
    return (Bout.objects.filter(
      basho_id=self.basho_id
    ).filter(
      oyakata1=self.oyakata
    ).aggregate(p=Sum('oyakata2_score'))['p'] or 0) + (Bout.objects.filter(
      basho_id=self.basho_id
    ).filter(
      oyakata2=self.oyakata
    ).aggregate(p=Sum('oyakata1_score'))['p'] or 0)

  @property
  def dominance_factor(self):
    if self.points_against == 0:
      return 999 if self.total_points > 0 else 0.
    return float(self.total_points) / float(self.points_against)

  @cached_property
  def points_possible(self):
    return (Bout.objects.filter(
      basho_id=self.basho_id
    ).filter(
      oyakata1=self.oyakata
    ).aggregate(pp=Sum('oyakata1_bp'))['pp'] or 0) + (Bout.objects.filter(
      basho_id=self.basho_id
    ).filter(
      oyakata2=self.oyakata
    ).aggregate(pp=Sum('oyakata2_bp'))['pp'] or 0)

  @property
  def coach_rating(self):
    if self.points_possible == 0:
      return 0.
    return float(self.total_points) / float(self.points_possible)

  @cached_property
  def standings_points(self):
    return (self.wins * 2) + self.draws

  @cached_property
  def suicide_picks(self):
    results = {}

    suicide_entries = SuicidePoolEntry.objects.filter(
      basho_id=self.basho_id,
      oyakata=self.oyakata
    )

    for se in suicide_entries:
      results[se.day] = se

    return results

  @cached_property
  def num_suicide_strikes(self):
    num_strikes = 0

    suicide_entries = SuicidePoolEntry.objects.filter(
      basho_id=self.basho_id,
      oyakata=self.oyakata
    )

    for se in suicide_entries:
      if se.did_win < 0:
        num_strikes += 1

    return num_strikes

  @cached_property
  def num_suicide_entries(self):
    return SuicidePoolEntry.objects.filter(
      basho_id=self.basho_id,
      oyakata=self.oyakata
    ).count()

  @cached_property
  def suicide_still_alive(self, ):
    current_day = get_current_day(self.basho_id)
    num_strikes = self.num_suicide_strikes + max(
      0, current_day - 1 - self.num_suicide_entries)
    return num_strikes < SUICIDE_LAST_STRIKE

  @property
  def rank_full_str(self):
    return convert_rank_to_full_string(self.rank)

  @property
  def rank_short_str(self):
    return convert_rank_to_short_string(self.rank)

  @property
  def squadron_members(self):
    return SquadronMember.objects.filter(
      oyakata=self.oyakata,
      basho_id=self.basho_id,
      is_active=True,
    ).order_by('selection_order')

  @property
  def num_squadron_selections(self):
    return SquadronMember.objects.filter(
      oyakata=self.oyakata,
      basho_id=self.basho_id,
    ).count()

  def result_against(self, other):
    '''
    Returns the result of this BashoSquadron against another.

    Args:
      other: The other BashoSquadron to compare against

    Returns:
      0 if this BashoSquadron lost, 2 if it won, or 1 for a draw.
      None if no bout exists between these two BashoSquadrons.
    '''
    as_o1 = Bout.objects.filter(
      basho_id=self.basho_id,
      oyakata1=self.oyakata,
      oyakata2=other.oyakata,
      day__lte=LAST_DAY_OF_REG_SEASON,
    ).first()
    as_o2 = Bout.objects.filter(
      basho_id=self.basho_id,
      oyakata1=other.oyakata,
      oyakata2=self.oyakata,
      day__lte=LAST_DAY_OF_REG_SEASON,
    ).first()

    if not as_o1 and not as_o2:
      return None

    if as_o1:
      if as_o1.oyakata1_score > as_o1.oyakata2_score:
        return 2
      elif as_o1.oyakata1_score < as_o1.oyakata2_score:
        return 0
      else:
        return 1
    else:
      if as_o2.oyakata2_score > as_o2.oyakata1_score:
        return 2
      elif as_o2.oyakata2_score < as_o2.oyakata1_score:
        return 0
      else:
        return 1

  def __str__(self):
    return u'{0} ({1})'.format(self.shikona,
                               get_basho_full_name(self.basho_id))


@python_2_unicode_compatible
class SquadronMember(models.Model):
  oyakata = models.ForeignKey(Oyakata, on_delete=models.PROTECT)
  rikishi = models.ForeignKey(Rikishi, on_delete=models.PROTECT)
  basho_id = models.PositiveSmallIntegerField()
  selection_order = models.PositiveSmallIntegerField()
  is_active = models.BooleanField(default=True)

  def __str__(self):
    return u'{0} ({1}, {2}){3}'.format(
      self.rikishi.shikona_for_basho(self.basho_id),
      self.oyakata.shikona_for_basho(self.basho_id),
      get_basho_full_name(self.basho_id),
      '' if self.is_active else ' [INACTIVE]'
    )


class DraftDivisionMember(models.Model):
  oyakata = models.ForeignKey(Oyakata, on_delete=models.PROTECT)
  division = models.PositiveSmallIntegerField()
  basho_id = models.PositiveSmallIntegerField()


@python_2_unicode_compatible
class OzumoBanzukeEntry(models.Model):
  class Meta:
    verbose_name_plural = 'ozumo banzuke entries'
    unique_together = (('rikishi', 'basho_id'),)

  rikishi = models.ForeignKey(Rikishi, on_delete=models.PROTECT)
  shikona = models.CharField(max_length=20)
  basho_id = models.PositiveSmallIntegerField()
  rank = models.SmallIntegerField()

  @property
  def rank_full_str(self):
    return convert_rank_to_full_string(self.rank)

  @property
  def rank_short_str(self):
    return convert_rank_to_short_string(self.rank)

  def __str__(self):
    return self.rikishi.shikona_for_basho(self.basho_id)


class OzumoBout(models.Model):
  winning_rikishi = models.ForeignKey(Rikishi, on_delete=models.PROTECT,
                                      related_name='winning_rikishi')
  winning_rikishi_rank = models.SmallIntegerField()
  losing_rikishi = models.ForeignKey(Rikishi, on_delete=models.PROTECT,
                                     related_name='losing_rikishi')
  losing_rikishi_rank = models.SmallIntegerField()
  kimarite = models.ForeignKey(Kimarite, on_delete=models.PROTECT)
  value = models.PositiveSmallIntegerField()
  basho_id = models.PositiveSmallIntegerField()
  day = models.PositiveSmallIntegerField()


def _get_lineup(oyakata, basho_id, day, force=False):
  '''
  Helper method to collect lineups.
  '''
  results = []
  if force:
    # Start at current day, go back to day 1, looking for a set lineup
    for d in xrange(day, 0, -1):
      results = LineupEntry.objects.filter(
        basho_id=basho_id
      ).filter(
        day=d
      ).filter(
        oyakata=oyakata
      ).order_by(
        'order'
      )

      if len(results) > 0:
        return [r.rikishi.shikona_for_basho(basho_id) for r in results]

    # If no lineups have been set this basho, return first four rikishi
    results = SquadronMember.objects.filter(
      basho_id=basho_id
    ).filter(
      oyakata=oyakata
    ).filter(
      is_active=True
    ).order_by(
      'selection_order'
    )[:LINEUP_SIZE]
  else:
    results = LineupEntry.objects.filter(
      basho_id=basho_id
    ).filter(
      day=day
    ).filter(
      oyakata=oyakata
    ).order_by(
      'order'
    )

  return [r.rikishi.shikona_for_basho(basho_id) for r in results]


def get_current_day(basho_id=None):
  basho_id = basho_id or get_current_basho_id()

  # See if there are any scored bouts for this basho
  scored_bouts = Bout.objects.filter(
    basho_id=basho_id
  ).exclude(
    oyakata1_score__isnull=True
  )

  # If there are, then return the max scored day plus one, which is the
  # active day.
  if scored_bouts.count() > 0:
    return scored_bouts.aggregate(Max('day'))['day__max'] + 1

  # If not, check if there are any scheduled bouts for Day 1. If so, then the
  # current day is Day 1. If not, then the basho hasn't been scheduled yet.
  scheduled_bouts = Bout.objects.filter(
    basho_id=basho_id
  ).filter(
    day=1
  )
  return 1 if scheduled_bouts.count() > 0 else 0
